################################################################################
#
# cpio to archive target filesystem
#
################################################################################

ifeq ($(BR2_ROOTFS_DEVICE_CREATION_STATIC),y)

define ROOTFS_CPIO_ADD_INIT
	if [ ! -e $(TARGET_DIR)/init ]; then \
		ln -sf sbin/init $(TARGET_DIR)/init; \
	fi
endef

else
# devtmpfs does not get automounted when initramfs is used.
# Add a pre-init script to mount it before running init
# patch/cpio/init을 복사 하기 위해서 코드 수정
ifneq ($(BR2_ROOTFS_CUSTOM_PROCESSING),"")
define ROOTFS_CPIO_ADD_INIT
	@$(call MSG_INFO,"Call $(BR2_ROOTFS_CUSTOM_PROCESSING)")
	$(BR2_ROOTFS_CUSTOM_PROCESSING) -r $(TOPDIR)/output/target
endef
else
define ROOTFS_CPIO_ADD_INIT
	@$(call MSG_WARN,"Skip post processing for building target rootfs")
endef
endif

PACKAGES_PERMISSIONS_TABLE += /dev/console c 622 0 0 5 1 - - -$(sep)

endif # BR2_ROOTFS_DEVICE_CREATION_STATIC

ROOTFS_CPIO_PRE_GEN_HOOKS += ROOTFS_CPIO_ADD_INIT

ifneq ($(BR2_TARGET_ROOTFS_CPIO_CUSTOM_INITRAMFS_LIST),"")
# SUNXI및 AMLOGIC의 경우, RAMDISK를 생성하기 위해서 필요한 최소한의 구성요소만을 정의하기 위해서 사용
define ROOTFS_CPIO_CMD
	cd $(TARGET_DIR) && cat $(BR2_TARGET_ROOTFS_CPIO_CUSTOM_INITRAMFS_LIST) | cpio --quiet -o -H newc > $@
endef
else
define ROOTFS_CPIO_CMD
	cd $(TARGET_DIR) && find . | cpio --quiet -o -H newc > $@
endef
endif # BR2_TARGET_ROOTFS_CPIO_CUSTOM_INITRAMFS_LIST



$(BINARIES_DIR)/rootfs.cpio.uboot: $(BINARIES_DIR)/rootfs.cpio host-uboot-tools
	$(MKIMAGE) -A $(MKIMAGE_ARCH) -T ramdisk \
		-C none -d $<$(ROOTFS_CPIO_COMPRESS_EXT) $@

ifeq ($(BR2_TARGET_ROOTFS_CPIO_UIMAGE),y)
ROOTFS_CPIO_POST_TARGETS += $(BINARIES_DIR)/rootfs.cpio.uboot
endif




ifneq ($(BR2_TARGET_ROOTFS_CPIO_CUSTOM_INITRAMFS_IMAGENAME),"")
ifeq ($(BR2_ARM_SOC_VENDOR_SUNXI),y)
ROOTFS_CPIO_POST_TARGETS += $(BINARIES_DIR)/rootfs.cpio.sunxi.img
else ifeq ($(BR2_ARM_SOC_VENDOR_AMLOGIC),y)
ROOTFS_CPIO_POST_TARGETS += $(BINARIES_DIR)/rootfs.cpio.aml.img
else
endif
else
#ROOTFS_CPIO_CUSTOM_IMAGE_NAME := $(BR2_TARGET_ROOTFS_CPIO_CUSTOM_INITRAMFS_IMAGENAME)
$(error ROOTFS_CPIO_CUSTOM_IMAGE_NAME: You have to define image name)
endif


# boot.img 혹은 recovery.img를 만드는데 사용될 kernel 바이너리 선택
ifeq ($(BR2_ARM_SOC_VENDOR_SUNXI),y)
ifeq ($(BR2_ARM_SOC_SUNXI_A31), y)
ifeq ($(BR2_TARGET_IMGPACK_BOOT_PNAND), y)
BOOTIMG_SRC_KERNEL_IMG = Image
BOOTIMG_SRC_KERNEL_OFFSET=0x8000
BOOTIMG_SRC_RAMDISK_OFFSET=0x2000000
BOOTIMG_SRC_TAG_OFFSET=0x100
BOOTIMG_SRC_PAGE_SIZE=2048
BOOTIMG_SRC_BASE_ADDR = 0x40000000
else
BOOTIMG_SRC_KERNEL_IMG =
BOOTIMG_SRC_KERNEL_OFFSET=0x8000
BOOTIMG_SRC_RAMDISK_OFFSET=0x2000000
BOOTIMG_SRC_TAG_OFFSET=0x100
BOOTIMG_SRC_PAGE_SIZE=2048
BOOTIMG_SRC_BASE_ADDR = 0x40000000
endif 	#<<BR2_TARGET_IMGPACK_BOOT_PNAND
else ifeq ($(BR2_ARM_SOC_SUNXI_V3),y)
ifeq ($(BR2_TARGET_IMGPACK_BOOT_SNOR), y)
BOOTIMG_SRC_KERNEL_IMG = zImage
BOOTIMG_SRC_KERNEL_OFFSET=0x8000
BOOTIMG_SRC_RAMDISK_OFFSET=0x1000000
BOOTIMG_SRC_TAG_OFFSET=0x100
BOOTIMG_SRC_PAGE_SIZE=2048
BOOTIMG_SRC_BASE_ADDR = 0x40000000
else
BOOTIMG_SRC_KERNEL_IMG =
BOOTIMG_SRC_KERNEL_OFFSET=0x8000
BOOTIMG_SRC_RAMDISK_OFFSET=0x2000000
BOOTIMG_SRC_TAG_OFFSET=0x100
BOOTIMG_SRC_PAGE_SIZE=2048
BOOTIMG_SRC_BASE_ADDR = 0x40000000
endif
else
BOOTIMG_SRC_KERNEL_IMG =
BOOTIMG_SRC_KERNEL_OFFSET=0x8000
BOOTIMG_SRC_RAMDISK_OFFSET=0x2000000
BOOTIMG_SRC_TAG_OFFSET=0x100
BOOTIMG_SRC_PAGE_SIZE=2048
BOOTIMG_SRC_BASE_ADDR = 0x40000000
endif
else ifeq ($(BR2_ARM_SOC_VENDOR_AMLOGIC),y)
BOOTIMG_SRC_KERNEL_IMG = uImage
BOOTIMG_SRC_KERNEL_OFFSET=0x8000
BOOTIMG_SRC_RAMDISK_OFFSET=0x2000000
BOOTIMG_SRC_TAG_OFFSET=0x100
BOOTIMG_SRC_PAGE_SIZE=2048
BOOTIMG_SRC_BASE_ADDR = 0x40000000
else
BOOTIMG_SRC_KERNEL_IMG =
BOOTIMG_SRC_KERNEL_OFFSET=0x8000
BOOTIMG_SRC_RAMDISK_OFFSET=0x2000000
BOOTIMG_SRC_TAG_OFFSET=0x100
BOOTIMG_SRC_PAGE_SIZE=2048
BOOTIMG_SRC_BASE_ADDR = 0x40000000
endif


ifneq ($(BOOTIMG_SRC_KERNEL_IMG),"")
$(BINARIES_DIR)/rootfs.cpio.sunxi.img: $(BINARIES_DIR)/rootfs.cpio
	@$(call MSG_INFO,"Generating boot or recovery image: ${BOOTIMG_SRC_KERNEL_IMG}"); \
	echo $(LINUX_DIR)
	$(TOPDIR)/3rd_party/mbrain/tools/imgtools/mkbootimg/mkbootimg --kernel $(LINUX_DIR)/arch/arm/boot/${BOOTIMG_SRC_KERNEL_IMG} --ramdisk $(BINARIES_DIR)/rootfs.cpio.gz --base $(BOOTIMG_SRC_BASE_ADDR) --cmdline '' --pagesize $(BOOTIMG_SRC_PAGE_SIZE) --kernel_offset $(BOOTIMG_SRC_KERNEL_OFFSET) --ramdisk_offset $(BOOTIMG_SRC_RAMDISK_OFFSET) --tags_offset $(BOOTIMG_SRC_TAG_OFFSET) --output $(BINARIES_DIR)/boot.img --board $(BR2_TARGET_GENERIC_HOSTNAME)
else
$(BINARIES_DIR)/rootfs.cpio.sunxi.img: $(BINARIES_DIR)/rootfs.cpio
	@$(call MSG_WARN,"Generating boot or recovery image: using default uImage); \
	echo $(LINUX_DIR)
	$(TOPDIR)/3rd_party/mbrain/tools/imgtools/mkbootimg/mkbootimg --kernel $(LINUX_DIR)/arch/arm/boot/uImage --ramdisk $(BINARIES_DIR)/rootfs.cpio.gz --base $(BOOTIMG_SRC_BASE_ADDR) --cmdline '' --pagesize $(BOOTIMG_SRC_PAGE_SIZE) --kernel_offset $(BOOTIMG_SRC_KERNEL_OFFSET) --ramdisk_offset $(BOOTIMG_SRC_RAMDISK_OFFSET) --tags_offset $(BOOTIMG_SRC_TAG_OFFSET) --output $(BINARIES_DIR)/boot.img --board $(BR2_TARGET_GENERIC_HOSTNAME)
endif

$(BINARIES_DIR)/rootfs.cpio.aml.img: $(BINARIES_DIR)/rootfs.cpio
	@$(call MESSAGE,"Generating boot or recovery image")





$(eval $(call ROOTFS_TARGET,cpio))

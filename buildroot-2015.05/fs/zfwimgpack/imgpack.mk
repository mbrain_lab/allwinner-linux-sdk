################################################################################
#
# Build the squashfs root filesystem image
#
################################################################################

ifeq ($(BR2_TARGET_ROOTFS_EXT2),y)
ROOTFS_IMGPACK_DEPENDENCIES =  host-mke2img
endif

ifeq ($(BR2_TARGET_ROOTFS_SQUASHFS),y)
ROOTFS_IMGPACK_DEPENDENCIES += host-squashfs
endif

ifeq ($(BR2_TARGET_ROOTFS_JFFS2),y)
ROOTFS_IMGPACK_DEPENDENCIES += host-jffs2
endif


define ROOTFS_IMGPACK_CMD
	echo "No job"
endef










#이미지 패킹 파마미터 준비부
rootfs-imgpack-setparam:
#Allwinner사의 packing에 사용될 Packing 옵션 정의
ifeq ($(BR2_ARM_SOC_VENDOR_SUNXI), y)
ifeq ($(BR2_ARM_SOC_SUNXI_A31), y)
ifeq ($(BR2_TARGET_IMGPACK_BOOT_PNAND), y)
PACK_PARAMS = -p linux -B nand
else ifeq ($(BR2_TARGET_IMGPACK_BOOT_SDMMC), y)
	@$(call MSG_WARN,"SDMMC boot media does not support yet for A31.")
PACK_PARAMS =
else
	@$(call MSG_ERROR,"Other boot media does not support on A31 device")
PACK_PARAMS =
endif
else ifeq ($(BR2_ARM_SOC_SUNXI_V3), y)
ifeq ($(BR2_TARGET_IMGPACK_BOOT_SNOR), y)
	@$(call MSG_INFO,"Target boot media select serial NOR flash.")
PACK_PARAMS = -B nor -p linux -d uart1 -a buildroot
else
	@$(call MSG_ERROR,"Other boot media does not support on V3 device")
PACK_PARAMS =
endif
else
	@$(call MSG_ERROR,"Un supported allwinner chip")
PACK_PARAMS =
endif
else ifeq ($(BR2_ARM_SOC_VENDOR_AMLOGIC), y)
	@$(call MSG_WARN,"PACK Option was reserved for AMLOGIC device.")
PACK_PARAMS =
else
	@$(call MSG_ERROR,"Un supported device")
PACK_PARAMS =
endif

ifeq ($(BR2_ARM_SOC_SUNXI_V3), y)
ifeq ($(BR2_TARGET_IMGPACK_FORMFACTOR_V3_CDR), y)
PACK_PARAMS += -b CDR
else ifeq ($(BR2_TARGET_IMGPACK_FORMFACTOR_V3_CDR8M), y)
	@$(call MSG_WARN,"CDR-8M fomfactor does not support yet.")
else
	@$(call MSG_ERROR,"Other fomfactor does not support.")
endif
endif



#이미지 패킹 실행부
rootfs-imgpack-pack:
ifeq ($(BR2_ARM_SOC_VENDOR_SPECIFIC_PROPFERTY), y)
ifneq ($(BR2_TARGET_IMGPACK_UTIL_PATH),"")
ifeq ($(BR2_TARGET_IMGPACK_FUSING_USB), y)
ifeq ($(BR2_ARM_SOC_VENDOR_SUNXI), y)
	@$(call MESSAGE,"Generating SUNXI style usb image...")
	$(BR2_TARGET_IMGPACK_UTIL_PATH) $(PACK_PARAMS) -i $(BINARIES_DIR)
else ifeq ($(BR2_ARM_SOC_VENDOR_AMLOGIC), y)
	@$(call MESSAGE,"Generating AMLOGIC style usb image...")
else
	@$(call MESSAGE,"Unsupport VENDOR type, skip generating usb image...")
endif
else ifeq ($(BR2_TARGET_IMGPACK_FUSING_SDMMC), y)
ifeq ($(BR2_ARM_SOC_VENDOR_SUNXI), y)
ifeq ($(BR2_ARM_SOC_SUNXI_A31), y)
	@$(call MESSAGE,"Does not support SDMMC yet for A31...")
else ifeq ($(BR2_ARM_SOC_SUNXI_V3), y)
	@$(call MESSAGE,"Generating SUNXI style SDMMC image for V3...")
	$(BR2_TARGET_IMGPACK_UTIL_PATH) $(PACK_PARAMS) -i $(BINARIES_DIR)
else
	@$(call MESSAGE,"Invalid processor type...")
endif
else ifeq ($(BR2_ARM_SOC_VENDOR_AMLOGIC), y)
	@$(call MESSAGE,"Does not support SDMMC yet for AMLOGIC...")
else
	@$(call MESSAGE,"Does not support SDMMC yet...")
endif
else
	@$(call MESSAGE,"Invalid Flashing type...")
endif
endif
else
	@$(call MESSAGE,"Skip usb-packing")
endif




.PHONY: rootfs-imgpack-setparam rootfs-imgpack-pack


ROOTFS_IMGPACK_POST_TARGETS += rootfs-imgpack-setparam  rootfs-imgpack-pack



$(eval $(call ROOTFS_TARGET,imgpack))

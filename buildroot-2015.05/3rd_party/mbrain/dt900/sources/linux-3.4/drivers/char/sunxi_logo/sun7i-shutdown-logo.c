
#include <linux/module.h>
#include <linux/init.h>
#include <linux/input.h>
#include <linux/slab.h>
#include<linux/mm.h>   
#include<linux/cdev.h>   
#include<asm/uaccess.h>   

#define LOGO_DEBUG_LEVEL 1

#if (LOGO_DEBUG_LEVEL == 1)
    #define logo_dbg(format,args...)     do {} while (0)
    #define logo_inf(format,args...)     do {} while (0)
    #define logo_err(format,args...)     printk(KERN_ERR "[logo-err] "format,##args)
#elif (LOGO_DEBUG_LEVEL == 2)
    #define logo_dbg(format,args...)     do {} while (0)
    #define logo_inf(format,args...)     printk(KERN_INFO"[logo-inf] "format,##args)
    #define logo_err(format,args...)     printk(KERN_ERR "[logo-err] "format,##args); 
#elif (LOGO_DEBUG_LEVEL == 3)
    #define logo_dbg(format,args...)     printk(KERN_INFO"[logo-dbg] "format,##args)
    #define logo_inf(format,args...)     printk(KERN_INFO"[logo-inf] "format,##args)
    #define logo_err(format,args...)     printk(KERN_ERR "[logo-err] "format,##args)
#endif

#define  LOGO_MAJOR  0
static unsigned long logo_major = LOGO_MAJOR; 
struct logo_dev   
{  
	struct cdev cdev;  
};  

struct class *logo_class;
struct logo_dev   *logo_devp;  

ssize_t logo_read(struct file *filp, char __user *buf, size_t count, loff_t *f_ops)
{
#if 0
    int result;  
    unsigned char *kernel_logo_buf = __va(LCD_CONFIG_MEMBASE_LOGO_CLOSE);
    int cnt_debug = 0;
	unsigned char *tmp_buf = kernel_logo_buf;

    for (cnt_debug = 0; cnt_debug <= 100; cnt_debug++ )	{
		logo_dbg("kernel_logo_buf :0x%x\n",*((volatile unsigned int *)(tmp_buf+cnt_debug*4)));
	}

	if(copy_to_user((void __user *)buf, kernel_logo_buf, SZ_512K)) {
    	logo_err("copy_to_user fail\n");
        return  -EFAULT;
    }
#endif
	return  count;
	
}

static ssize_t logo_write(struct file *filp, const char __user *buf,
size_t size, loff_t *ppos)
{
	unsigned int count = size;
	int ret = 0;
	int buf_value = 0;
	count = size ;
	if (copy_from_user(&buf_value, buf, count)) {
		ret = - EFAULT;
	} else {
		ret = count;
    }
    return ret;
}

static int logo_open(struct inode *inode,struct file *filp)
{
	filp->private_data = logo_devp;
	return 0;  
}

static int logo_release(struct inode *inode, struct file *filp) 
{
	return 0;	  
}

struct file_operations logo_fops = {
	.owner = THIS_MODULE,
	.read  = logo_read,
	.write = logo_write,
    .open  = logo_open,
	.release = logo_release,
};

static void logo_setup_cdev(struct logo_dev *dev, int index)  
{  
    int err,devno = MKDEV(logo_major,index);  
    cdev_init(&dev->cdev,&logo_fops);  
    dev->cdev.owner = THIS_MODULE;  
    err = cdev_add(&dev->cdev,devno,1);  
    if(err) {  
        logo_err(KERN_NOTICE "Error %d adding %d\n",err,index);  
    }  
}  

static int __init shutdown_logo_init(void)
{
	int result;  
	logo_dbg(" logo_init \n");
	
	int cnt_debug = 0;
    dev_t devno = MKDEV(logo_major,0);  
	if(logo_major) {
		logo_err(" logo_init register \n");
		result = register_chrdev_region(devno,1,"shutlogo");  
	}else {  
			result = alloc_chrdev_region(&devno,0,1,"shutlogo");  
			logo_major = MAJOR(devno);  
			logo_dbg(" logo_init alloc_chrdev \n");
	}  
	if(result < 0) {  
		logo_err(" logo_init register failed!");  
		return result;	
	}  
	logo_devp =(struct logo_dev*)kmalloc(sizeof(struct logo_dev),GFP_KERNEL);	
	if(!logo_devp) {  
			result = -ENOMEM;  
			printk(" chrdev no memory fail\n");
			unregister_chrdev_region(devno,1);	
			return result;
	}  
	memset(logo_devp, 0 ,sizeof(struct logo_dev));  
	logo_setup_cdev(logo_devp,0);  
	logo_class = class_create(THIS_MODULE, "shutlogo");
	if(IS_ERR(logo_class)) {
        logo_err("Err: failed in creating class\n");
        return -1; 
	}
	device_create(logo_class, NULL, MKDEV(logo_major, 0),  NULL, "shutlogo");
	
    return 0;  
}

static void __exit shutdown_logo_exit(void)
{
	if(logo_devp)
		cdev_del(&logo_devp->cdev);
	device_destroy(logo_class, MKDEV(logo_major, 0));        
    class_destroy(logo_class);                               

	if(logo_devp)
		kfree(logo_devp);
	unregister_chrdev_region(MKDEV(logo_major,0),1);
}

module_init(shutdown_logo_init);
module_exit(shutdown_logo_exit);
MODULE_DESCRIPTION("shutdown logo driver");
MODULE_AUTHOR("Richard");
MODULE_LICENSE("GPL");


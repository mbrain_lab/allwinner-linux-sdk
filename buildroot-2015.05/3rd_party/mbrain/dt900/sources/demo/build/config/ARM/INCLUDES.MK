# (c) mBrain

ifndef $(INCLUDES_MK)
INCLUDES_MK = 1

# Common Include
COMMON_INC=-I$(DEMO_ROOT_DIR)/inc

# KERNEL Include
KERNEL_INC=-I$(DEMO_SDK_DIR)/output/build/linux-custom
KERNEL_INC+=-I$(DEMO_SDK_DIR)/output/build/linux-custom/include

# User Include
DRV_INC=-I$(DEMO_ROOT_DIR)/libs/drv/inc
OSA_INC=-I$(DEMO_ROOT_DIR)/libs/osa/inc

# Common Library
COMMON_LIBS =


# User Library
USER_LIBS+ =

endif # ifndef $(INCLUDES_MK)

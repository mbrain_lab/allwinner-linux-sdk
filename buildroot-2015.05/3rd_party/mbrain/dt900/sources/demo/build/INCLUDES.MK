# (c) mBrain
ifeq ($(DEMO_PLATFORM), x86)
include $(DEMO_BUILD_DIR)/config/X86/INCLUDES.MK
else ifeq ($(DEMO_PLATFORM), ARM)
include $(DEMO_BUILD_DIR)/config/ARM/INCLUDES.MK
else
$(error : "Invalid DEMO_PLATFORM, $(DEMO_PLATFORM)")
endif

#!/bin/bash

drv_list=("videobuf-core.ko" "videobuf-dma-contig.ko" "cci.ko" "vfe_os.ko" "vfe_subdev.ko" "ov2710_mipi.ko" "vfe_v4l2.ko" "videobuf2-core.ko" "videobuf2-memops.ko" "videobuf2-vmalloc.ko")


for drv in "${drv_list[@]}"; do
	#echo $drv
	file_path=$(find /lib/modules/ -name ${drv})
	if [[ -z "$file_path" ]]; then
		echo "Cannot find device driver[${drv}]"
	else
		echo "Install device driver[${drv}]"
		insmod $file_path
		sleep 0.1
	fi
done

sleep 1

echo "1" > /sys/devices/platform/sunxi_vfe.0/video4linux/video0/device/vfe_attr/vfe_dbg_en
echo "3" > /sys/devices/platform/sunxi_vfe.0/video4linux/video0/device/vfe_attr/vfe_dbg_lv

/**
 * @file   display.h
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.08.02
 * @brief  Capture Task header
 * 
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain & MidmeSoft
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain & MidmeSoft
 * 
 * (c) Copyright mBrain & MidmeSoft(2015).  All Rights Reserved.
 */


/*! [ 1]. File classification */
#ifndef _TASK_DISPLAY_H_
#define _TASK_DISPLAY_H_


/*! [ 2]. File Directive (#include) */
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>

#include <osa.h>
#include <osa_tsk.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/unistd.h>
#include <sys/mman.h>
#include <asm/types.h>
#include <linux/types.h>

#include <fcntl.h>

#include <linux/fb.h>
#include <video/sunxi_display2.h>

#include <linux/videodev2.h>



/*! [ 3]. Global macro and enum*/
typedef enum
{
	DISP_CH0                =0,
	DISP_CH1                  ,
    DISP_CH_MAXCNT
}DISP_CHANNEL;


typedef enum
{
	DISP_OIF_LCD             =0,
	DISP_OIF_HDMI              ,
	DISP_OIF_DVI               ,
	DISP_OIF_VGA               ,
	DISP_OIF_CVBS              ,
    DISP_OIF_MAXCNT
}DISP_OUTPUT_INTERFACE;


typedef enum
{
	DISP_LAYER_0             =0,
	DISP_LAYER_1               ,
	DISP_LAYER_2               ,
	DISP_LAYER_3               ,
    DISP_LAYER_MAXCNT
}DISP_LAYER_INDEX;

#define SCREEN_0 0
#define SCREEN_1 1

#define ZORDER_MAX 11
#define ZORDER_MIN 0


#define CHN_NUM 3
#define LYL_NUM 4
#define HLAY(chn, lyl) (chn*4+lyl)
#define HD2CHN(hlay) (hlay/4)
#define HD2LYL(hlay) (hlay%4)

enum
{
	HWD_STATUS_REQUESTED    = 1,
	HWD_STATUS_NOTUSED		= 2,
    HWD_STATUS_OPENED       = 4
};




/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
/*! [ 5]. Global Function prototype declaration */
extern int TaskDisplay( struct OSA_TskHndl *pPrc, OSA_MsgHndl *pMsg, Uint32 curState );

/*! [ 6]. Global Variable declaration  */




#endif //_TASK_DISPLAY_H_



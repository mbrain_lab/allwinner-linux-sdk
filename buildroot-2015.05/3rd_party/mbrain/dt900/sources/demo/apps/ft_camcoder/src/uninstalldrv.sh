#!/bin/bash

drv_list=("videobuf2-vmalloc.ko"  "videobuf2-memops.ko"  "videobuf2-core.ko" "vfe_v4l2.ko" "ov2710_mipi.ko" "vfe_subdev.ko" "vfe_os.ko" "cci.ko" "videobuf-dma-contig.ko" "videobuf-core.ko")

for drv in "${drv_list[@]}"; do
	#echo $drv
	file_path=$(find /lib/modules/ -name ${drv})
	if [[ -z "$file_path" ]]; then
		echo "Cannot find device driver[${drv}]"
	else
		echo "Uninstall device driver[${drv}]"
		rmmod $file_path
		sleep 0.1
	fi
done

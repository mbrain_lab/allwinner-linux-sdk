/**
 * @file   encoder.h
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.08.02
 * @brief  Capture Task header
 * 
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain & MidmeSoft
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain & MidmeSoft
 * 
 * (c) Copyright mBrain & MidmeSoft(2015).  All Rights Reserved.
 */


/*! [ 1]. File classification */
#ifndef _TASK_ENCODER_H_
#define _TASK_ENCODER_H_


/*! [ 2]. File Directive (#include) */
#include <osa.h>
#include <osa_tsk.h>

#include <fcntl.h>             
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <asm/types.h>         
//#include <linux/sunxi_physmem.h>


/*! [ 3]. Global macro and enum*/
/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
/*! [ 5]. Global Function prototype declaration */
extern int TaskEncoder( struct OSA_TskHndl *pPrc, OSA_MsgHndl *pMsg, Uint32 curState );

/*! [ 6]. Global Variable declaration  */




#endif //_TASK_ENCODER_H_




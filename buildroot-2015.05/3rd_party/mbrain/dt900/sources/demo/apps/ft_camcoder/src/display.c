/**
 * @file   display.c
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.08.02
 * @brief  Display task
 *
 * Detailed description of file.
 *
 * @copyright
 * (c) Copyright MBRAIN & mBrain(2015).  All Rights Reserved.
 */

/*! [ 1]. File Directive (#include) */
#include <osa.h>
#include <osa_debug.h>
#include <osa_buf.h>


#include "display.h"
#include "task.h"
#include "ft_camcoder.h"

#include <linux/ion.h>
#include <linux/ion_sunxi.h>

#include <qdbmp.h>



/*! [ 2]. Local Macro (#define, enum) */
#define DISP_DEVICE			"/dev/disp"
#define FB0_DEVICE          "/dev/fb0"
#define FB1_DEVICE          "/dev/fb1"

#define ION_DEVICE          "/dev/ion"


/*! [ 3]. Type definition or structure declaration (typedef or struct) */
typedef struct {
	char  szName[SZ_DESC_MAX];
	int   phyAddr;
	void* virAddr;
	int   length;
	int   isValid;
} dspBufPool;

typedef struct {
	int                 fd_fb[DISP_LAYER_MAXCNT];
	int                 fb_bufCnt[DISP_LAYER_MAXCNT];
	dspBufPool          fb_bufPool[DISP_LAYER_MAXCNT];
	int                 fb_mode[DISP_LAYER_MAXCNT];
	int                 fb_fmt[DISP_LAYER_MAXCNT];
	int                 fb_seq[DISP_LAYER_MAXCNT];
	int                 if_type[DISP_LAYER_MAXCNT];
	int                 output_std[DISP_LAYER_MAXCNT];

	Uint32              layerId[DISP_LAYER_MAXCNT];
} disp_ch;



typedef struct {
	int                 fd_phymem;
	struct ion_handle_data hndl_ion;
	
	int                 fd_dsp;
	
	OSA_ThrHndl         thrDsp;
	int                 exitThrDsp;

	Uint32              layerStatus[CHN_NUM][LYL_NUM];
	disp_ch             ch[DISP_CH_MAXCNT];
} sys_disp;

/*! [ 4]. Local Function prototype declaration */

/*! [ 5]. Local Variable declaration */
/*! [ 6]. Global Variable declaration */
sys_disp gSysDisp;


#define COLOR_WHITE                     0xffffffff
#define COLOR_YELLOW                    0xffffff00
#define COLOR_GREEN                     0xff00ff00
#define COLOR_CYAN                      0xff00ffff
#define COLOR_MAGENTA                   0xffff00ff
#define COLOR_RED                       0xffff0000
#define COLOR_BLUE                      0xff0000ff
#define COLOR_BLACK                     0x00000000


/*! [ 3]. Type definition or structure declaration (typedef or struct) */
static unsigned int colorbar[8] = 
{
    COLOR_WHITE,
    COLOR_YELLOW,
    COLOR_CYAN,
    COLOR_GREEN,
    COLOR_MAGENTA,
    COLOR_RED,
    COLOR_BLUE,
    COLOR_BLACK
};














int drv_ion_open(int* pFd)
{
	if (pFd==NULL){
		OSA_ERROR("Invalid device handle\n");
		return OSA_EFAIL;
	}

	if(*pFd == 0){
		*pFd = open(ION_DEVICE, O_RDONLY);
	}

	if(*pFd < 0 ) {
		OSA_ERROR("Failed to open ion device - %s",strerror(errno));
		*pFd = 0;
		return -errno;
	}

	OSA_printf("ION drive opend\n");
	return OSA_SOK;
}

void drv_ion_close(int* pFd)
{
	if( pFd != NULL){
		if( *pFd ){
			close(*pFd);
			*pFd = 0;
		}
	}
}

int drv_ion_alloc_buffer(int fd, int nSize, unsigned char** pVir, unsigned long* phyAddr, struct ion_handle_data* pHndl)
{
	int err = 0;
	struct ion_fd_data fd_data;
	struct ion_allocation_data ionAllocData;
	struct ion_custom_data custom_data;
	sunxi_phys_data phys_data;
	void* buffer=NULL;

	if( pHndl==NULL || fd<0 ){
		OSA_ERROR("Invalid parameters\n");
		return OSA_EFAIL;
	}


	ionAllocData.len = nSize;
	ionAllocData.align = 0;
	ionAllocData.heap_id_mask = ION_HEAP_CARVEOUT_MASK;
	ionAllocData.flags = 0;
	err = ioctl(fd, ION_IOC_ALLOC, &ionAllocData);
	if(err) {
		OSA_ERROR("ION_IOC_ALLOC failed.\n");
		return OSA_EFAIL;
	}

	fd_data.handle = ionAllocData.handle;
	pHndl->handle = ionAllocData.handle;
	err = ioctl(fd, ION_IOC_MAP, &fd_data);
	if(err) {
		OSA_ERROR("ION_IOC_MAP failed.\n");
		ioctl(fd, ION_IOC_FREE, pHndl);
		return OSA_EFAIL;
	}

	/* mmap to user space */
	buffer = mmap(NULL, ionAllocData.len, PROT_READ|PROT_WRITE, MAP_SHARED, fd_data.fd, 0);
	if(buffer == MAP_FAILED) {
		OSA_ERROR("Failed to map the allocated memory.");
		ioctl(fd, ION_IOC_FREE, pHndl);
		return OSA_EFAIL;
	}
	memset(buffer, 0, ionAllocData.len);
	*pVir = buffer;

	/* Get physical address */
	custom_data.cmd = ION_IOC_SUNXI_PHYS_ADDR;
	phys_data.handle = ionAllocData.handle;
	custom_data.arg = (unsigned long)&phys_data;
	err = ioctl(fd, ION_IOC_CUSTOM, &custom_data);
	if(err)
	{
		OSA_ERROR("Failed to get physical address.\n");
		return OSA_EFAIL;
	}

	*phyAddr = (unsigned long)phys_data.phys_addr;

	return OSA_SOK;
}

int drv_ion_free_buffer(int fd, struct ion_handle_data* pHndl, void *base, size_t size)
{
	int err = OSA_SOK;

	OSA_printf("ion: Unmapping buffer  base:%p size:%d", base, size);
	if(munmap(base, size)) {
		OSA_ERROR("ion: Failed to unmap memory at %p.", base);
	}

	ioctl(fd, ION_IOC_FREE, pHndl);
	OSA_printf("ion: Allocated buffer base:%p size:%d fd:%d",base, size, fd);

	return err;
}












int drv_dsp_config(int disp_fd, int nScrrenNum,  __DISP_t cmd, disp_layer_config *pinfo)
{
	unsigned long args[4] = {0};
	unsigned int ret = OSA_SOK;

	// OSA_printf("CMD=%d\n",cmd);

	args[0] = nScrrenNum;
	args[1] = (unsigned long)pinfo;
	args[2] = 1;
	ret = ioctl(disp_fd, cmd, args);
	if(OSA_SOK != ret) {
		OSA_ERROR("fail to processing para\n");
		ret = OSA_EFAIL;
	}
	return ret;
}


int drv_dsp_get_layer_param(int disp_fd, int nScrrenNum, disp_layer_config *pinfo)
{
	return drv_dsp_config(disp_fd, nScrrenNum, DISP_LAYER_GET_CONFIG, pinfo);
}

int drv_dsp_set_layer_param(int disp_fd, int nScrrenNum, disp_layer_config *pinfo)
{
	return drv_dsp_config(disp_fd, nScrrenNum, DISP_LAYER_SET_CONFIG, pinfo);
}



int drv_dsp_get_layer(int *pCh, int *pId)	//finish
{
	int ch;
	int id;
	{
		for(id=0; id<LYL_NUM; id++) {
			for (ch=0; ch<CHN_NUM; ch++) {
				if (!(gSysDisp.layerStatus[ch][id] & HWD_STATUS_REQUESTED)) {
					gSysDisp.layerStatus[ch][id] |= HWD_STATUS_REQUESTED;
					goto out;
				}
			}
		}
	}
out:
	if ((ch==CHN_NUM) && (id==LYL_NUM)) {
		OSA_ERROR("all layer used.\n");
		return OSA_EFAIL;
	}
	*pCh = ch;
	*pId = id;
	OSA_printf("requested: ch:%d, id:%d\n", ch, id);
	return HLAY(ch, id);
}



int drv_dsp_request_layer(int disp_fd, int nScrrenNum, int sx, int sy, int w, int h)
{
	int hlay;
	int ch, id;
	disp_layer_config config;

	memset(&config, 0, sizeof(disp_layer_config));
	hlay = drv_dsp_get_layer(&ch,&id);
	if (hlay<0){
		OSA_ERROR("drv_dsp_get_layer() fail\n");
		return OSA_EFAIL;
	}
	config.channel = ch;
	config.layer_id = id;
	config.enable = 0;
	config.info.screen_win.x = sx;
	config.info.screen_win.y = sy;
	config.info.screen_win.width	= w;
	config.info.screen_win.height	= h;
	config.info.mode = LAYER_MODE_BUFFER;
	config.info.alpha_mode = 0;
	config.info.alpha_value = 128;
	config.info.fb.flags = DISP_BF_NORMAL;
	config.info.fb.scan = DISP_SCAN_PROGRESSIVE;
	config.info.fb.color_space = DISP_BT601; //DISP_BT709;
	config.info.zorder = 0;
	OSA_printf("hlay:%d, zorder=%d", hlay, config.info.zorder);
	drv_dsp_set_layer_param(disp_fd, nScrrenNum, &config);

	return hlay;
}

int drv_dsp_layer_open(int disp_fd, int nScrrenNum, unsigned int hlay)
{
	disp_layer_config config;
	memset(&config, 0, sizeof(disp_layer_config));
	config.channel	= HD2CHN(hlay);
	config.layer_id = HD2LYL(hlay);
	drv_dsp_get_layer_param(disp_fd, nScrrenNum, &config);

	config.enable = 1;
	return drv_dsp_set_layer_param(disp_fd, nScrrenNum, &config);
}


int drv_dsp_layer_close(int disp_fd, int nScrrenNum, unsigned int hlay)
{
	disp_layer_config config;
	memset(&config, 0, sizeof(disp_layer_config));
	config.channel	= HD2CHN(hlay);
	config.layer_id = HD2LYL(hlay);
	drv_dsp_get_layer_param(disp_fd, nScrrenNum, &config);

	config.enable = 0;
	return drv_dsp_set_layer_param(disp_fd, nScrrenNum, &config);
}

int drv_dsp_layer_set_src(int disp_fd, int nScrrenNum, unsigned int hlay, _win window, _win crop, int pixFmt)
{
	//unsigned long args[4] = {0};
	disp_layer_config config;

	memset(&config, 0, sizeof(disp_layer_config));
	config.channel  = HD2CHN(hlay);
	config.layer_id = HD2LYL(hlay);
	drv_dsp_get_layer_param(disp_fd, nScrrenNum, &config);

	//OSA_printf("Crop: width,height=%lld,%lld\n",config.info.fb.crop.width,config.info.fb.crop.height);

	config.info.fb.crop.x = 0;
	config.info.fb.crop.y = 0;
	config.info.fb.crop.width  = (crop.nWidth);
	config.info.fb.crop.height = (crop.nHeight);

	//OSA_printf("Orignal: width: %lld, height: %lld\n", config.info.fb.crop.width, config.info.fb.crop.height);

	config.info.fb.crop.x = config.info.fb.crop.x << 32;
	config.info.fb.crop.y = config.info.fb.crop.y << 32;
	config.info.fb.crop.width  = config.info.fb.crop.width << 32;
	config.info.fb.crop.height = config.info.fb.crop.height << 32;
	//OSA_printf("For Kernel: width: 0x%llx, height: %llx ", config.info.fb.crop.width, config.info.fb.crop.height);
    
	config.info.fb.size[0].width  = window.nWidth;
	config.info.fb.size[0].height = window.nHeight; 

	switch(pixFmt) {
		case V4L2_PIX_FMT_YUV420:
			config.info.fb.format = DISP_FORMAT_YUV420_P;
			config.info.fb.size[1].width    = config.info.fb.size[0].width / 2;
			config.info.fb.size[1].height   = config.info.fb.size[0].height / 2;
			config.info.fb.size[2].width    = config.info.fb.size[0].width / 2;
			config.info.fb.size[2].height   = config.info.fb.size[0].height / 2;
			break;
		case V4L2_PIX_FMT_NV12:
			config.info.fb.format = DISP_FORMAT_YUV420_SP_UVUV;
			config.info.fb.size[1].width    = config.info.fb.size[0].width / 2;
			config.info.fb.size[1].height   = config.info.fb.size[0].height / 2;
			config.info.fb.size[2].width    = config.info.fb.size[0].width / 2;
			config.info.fb.size[2].height   = config.info.fb.size[0].height / 2;
			break;
		case V4L2_PIX_FMT_NV21:
			config.info.fb.format = DISP_FORMAT_YUV420_SP_VUVU;
			config.info.fb.size[1].width    = config.info.fb.size[0].width / 2;
			config.info.fb.size[1].height   = config.info.fb.size[0].height / 2;
			config.info.fb.size[2].width    = config.info.fb.size[0].width / 2;
			config.info.fb.size[2].height   = config.info.fb.size[0].height / 2;
			break;
		case V4L2_PIX_FMT_YUV444:
			config.info.fb.format = DISP_FORMAT_YUV444_P;
			config.info.fb.size[1].width    = config.info.fb.size[0].width;
			config.info.fb.size[1].height   = config.info.fb.size[0].height;
			config.info.fb.size[2].width    = config.info.fb.size[0].width;
			config.info.fb.size[2].height   = config.info.fb.size[0].height;
			break;
		case V4L2_PIX_FMT_YUV422P:
			config.info.fb.format = DISP_FORMAT_YUV422_P;
			config.info.fb.size[1].width    = config.info.fb.size[0].width / 2;
			config.info.fb.size[1].height   = config.info.fb.size[0].height;
			config.info.fb.size[2].width    = config.info.fb.size[0].width / 2;
			config.info.fb.size[2].height   = config.info.fb.size[0].height;
			break;
		case V4L2_PIX_FMT_YUYV:
			config.info.fb.format = DISP_FORMAT_YUV422_SP_UVUV;
			config.info.fb.size[1].width    = config.info.fb.size[0].width / 2;
			config.info.fb.size[1].height   = config.info.fb.size[0].height;
			config.info.fb.size[2].width    = config.info.fb.size[0].width / 2;
			config.info.fb.size[2].height   = config.info.fb.size[0].height;
			break;
		case V4L2_PIX_FMT_VYUY:
			config.info.fb.format = DISP_FORMAT_YUV422_SP_VUVU;
			config.info.fb.size[1].width    = config.info.fb.size[0].width / 2;
			config.info.fb.size[1].height   = config.info.fb.size[0].height;
			config.info.fb.size[2].width    = config.info.fb.size[0].width / 2;
			config.info.fb.size[2].height   = config.info.fb.size[0].height;
			break;
		case V4L2_PIX_FMT_YUV411P:
			config.info.fb.format = DISP_FORMAT_YUV411_P;
			config.info.fb.size[1].width    = config.info.fb.size[0].width / 4;
			config.info.fb.size[1].height   = config.info.fb.size[0].height;
			config.info.fb.size[2].width    = config.info.fb.size[0].width / 4;
			config.info.fb.size[2].height   = config.info.fb.size[0].height;
			break;
		default:
			config.info.fb.format = DISP_FORMAT_ARGB_8888;
			config.info.fb.size[1].width    = config.info.fb.size[0].width;
			config.info.fb.size[1].height   = config.info.fb.size[0].height;
			config.info.fb.size[2].width    = config.info.fb.size[0].width;
			config.info.fb.size[2].height   = config.info.fb.size[0].height;
			break;
	}
	//OSA_printf("set fb.format %d\n",config.info.fb.format);
	return drv_dsp_set_layer_param(disp_fd, nScrrenNum, &config);
}


int drv_dsp_layer_render(int disp_fd, int nScrrenNum, unsigned int hlay, int w, int h, unsigned long phyAddrY, unsigned long phyAddrC, int pixFmt)
{
	int ret;
	disp_layer_config config;
	
	memset(&config, 0, sizeof(disp_layer_config));
	config.channel	= HD2CHN(hlay);
	config.layer_id = HD2LYL(hlay);
	drv_dsp_get_layer_param(disp_fd, nScrrenNum, &config);

	//OSA_printf("hlay=%d, w/h=%d/%d pixFmt=%d physical(Y/C)=0x%08x,0x%08x\n",hlay,w,h,pixFmt,(unsigned long)(phyAddrY),(unsigned long)(phyAddrY));

	config.info.fb.addr[0] = phyAddrY;
	switch(pixFmt){
		case V4L2_PIX_FMT_YUV422P:
		case V4L2_PIX_FMT_YUYV:
		case V4L2_PIX_FMT_YVYU:
		case V4L2_PIX_FMT_UYVY:
		case V4L2_PIX_FMT_VYUY:
			config.info.fb.addr[1] = phyAddrC;
			config.info.fb.addr[2] = phyAddrC;
			break;
		case V4L2_PIX_FMT_YVU420:
		case V4L2_PIX_FMT_YUV420:
			config.info.fb.addr[1] = phyAddrC; //your C address,modify this
			config.info.fb.addr[2] = phyAddrC;
			break;
		case V4L2_PIX_FMT_NV16:
			OSA_printf("Not support format yet...\n");
			break;
		case V4L2_PIX_FMT_NV21:
		case V4L2_PIX_FMT_NV12:
			config.info.fb.addr[1] = phyAddrC; //your C address,modify this
			//config.info.fb.addr[2] = phyAddrC;
			break; 
		case V4L2_PIX_FMT_RGB32:
		case V4L2_PIX_FMT_BGR32:
			break;
		case V4L2_PIX_FMT_HM12:
			OSA_printf("Not support format yet...\n");
			break;
		default:
			OSA_printf("gDisp_Format is not found!\n");
			break;
	}
	//OSA_printf("config.info.fb.addr[0] = 0x%08x\n",(unsigned int)config.info.fb.addr[0]);
	//OSA_printf("config.info.fb.addr[1] = 0x%08x\n",(unsigned int)config.info.fb.addr[1]);
	//OSA_printf("config.info.fb.addr[2] = 0x%08x\n",(unsigned int)config.info.fb.addr[2]);
	//OSA_printf("config.info.fb.addr[3] = 0x%08x\n",(unsigned int)config.info.fb.addr[3]);
	ret = drv_dsp_set_layer_param(disp_fd, nScrrenNum, &config);

	#if 0
	if(!(mLayerStatus[config.channel][config.layer_id] & HWD_STATUS_OPENED)) {
		LOG2("(%s %d) %d\n", __FUNCTION__, __LINE__, hlay);
		hwd_layer_open(hlay); //to avoid that the first frame is not initialized 
		mLayerStatus[config.channel][config.layer_id] |= HWD_STATUS_OPENED;
	}
	#endif
	return ret;
}



int drv_dsp_lcd_on(int isOn)
{
	int ret = OSA_SOK;
	unsigned int disp_param[4]={0,};
	
	disp_param[0] = 0;
	disp_param[1] = isOn==TRUE ? DISP_OUTPUT_TYPE_LCD : DISP_OUTPUT_TYPE_NONE;
	ret = ioctl(gSysDisp.fd_dsp, DISP_DEVICE_SWITCH, (void*)disp_param);
	if (ret < 0) {
		OSA_ERROR("Failed to turn on lcd\n");
		return ret;
	}
	return ret;
}


int drv_dsp_stop(void)
{
	return OSA_SOK;
}

int drv_dsp_start(void)
{
	return OSA_SOK;
}




void drv_dsp_scale_test(void)
{
	int ret = 0;
	unsigned int *pbuf_vir = NULL;
	unsigned int nfbuf_size = 0;
	unsigned long pbuf_phy = 0;
	char path_img[512] = {0,};
	/* Load bmp file */
	unsigned char	r, g, b;
	UINT	img_width, img_height;
	UINT	x, y;
	BMP*	bmp=NULL;


	snprintf(path_img, 512, "./sample_1080p.bmp");
	//snprintf(path_img, 512, "./sample_480x272.bmp");

	/* Read an image file */
	bmp = BMP_ReadFile(path_img);
	if (bmp==NULL){
		OSA_ERROR("Cannot open image:%s\n",path_img);
		return;
	}

	/* Get image's dimensions */
	img_width = BMP_GetWidth( bmp );
	img_height = BMP_GetHeight( bmp );
	OSA_printf("Load(%s): width=%d height=%d\n",path_img, (int)img_width, (int)img_height);


	nfbuf_size = img_width*img_height*4;
	ret = drv_ion_alloc_buffer(gSysDisp.fd_phymem, nfbuf_size, (unsigned char*)&pbuf_vir, &pbuf_phy, &gSysDisp.hndl_ion);
	if (ret){
		OSA_ERROR("ION physical memory allocation failed.\n");
		return;
	}
	OSA_printf("Allocated memory summary\n");
	OSA_printf("	Virtual address:0x%08x\n",(unsigned int)pbuf_vir);
	OSA_printf("	Physical address:0x%08x\n",pbuf_phy);
	

	/* Iterate through all the image's pixels */
	for ( x = 0 ; x < img_width ; ++x )
	{
		for ( y = 0 ; y < img_height ; ++y )
		{
			/* Get pixel's RGB values */
			BMP_GetPixelRGB( bmp, x, y, &r, &g, &b );
			//pbuffer[y*nWidth+x] = (r<<16)|(g<<8)|b;
			pbuf_vir[y*img_width+x] = (255<<24)|(b<<16)|(g<<8)|r;
		}
	}
	/* Free all memory allocated for the image */
	BMP_Free( bmp );


 	_win video_win;
	video_win.nSX = 0;
	video_win.nSY = 0;
	video_win.nWidth = img_width/4;
	video_win.nHeight = img_height/4; 
	ret = drv_dsp_layer_set_src(gSysDisp.fd_dsp, DISP_CH0,
									gSysDisp.ch[DISP_CH0].layerId[DISP_LAYER_1],
									video_win,video_win,
									V4L2_PIX_FMT_BGR32);

	if ( ret < 0 ){
		OSA_ERROR("drv_dsp_layer_set_src() failed\n");
	}
	else 
	{
		ret =  drv_dsp_layer_render(gSysDisp.fd_dsp, DISP_CH0,
							gSysDisp.ch[DISP_CH0].layerId[DISP_LAYER_1],
							img_width, img_height, 
							pbuf_phy, pbuf_phy, V4L2_PIX_FMT_BGR32);
		if ( ret < 0 ){
			OSA_ERROR("drv_dsp_layer_render() failed\n");
		}

	}
 						

	#if 0
	ret = drv_ion_free_buffer(gSysDisp.fd_phymem, &gSysDisp.hndl_ion, 
								(void*)gSysDisp.ch[DISP_CH0].fb_bufPool[DISP_LAYER_0].virAddr, 
								nfbuf_size);
	if (ret){
		OSA_ERROR("Failed to free ion buffer.\n");
	}
	#endif
}


void fill_bmpimage(const char* szFileName, unsigned int* pbuffer, int nScrW, int nScrH, unsigned char alpha)
{
	int sx=0,sy=0, dx=0,dy=0;
/* Load bmp file */
	unsigned char	r, g, b;
	UINT	img_width, img_height, copy_width, copy_height;
	UINT	x, y;
	BMP*	bmp=NULL;

	/* Read an image file */
	bmp = BMP_ReadFile(szFileName);
	if (bmp==NULL){
		OSA_ERROR("Cannot open image:%s\n",szFileName);
		return;
	}

	/* Get image's dimensions */
	img_width = BMP_GetWidth( bmp );
	img_height = BMP_GetHeight( bmp );

	if (nScrW<(int)img_width){
		sx = (int)(img_width-nScrW)/2;
		dx = 0;
		copy_width = nScrW;
	}else if (nScrW>(int)img_width){
		dx = (int)(nScrW-img_width)/2;
		sx = 0;
		copy_width = img_width;
	}else{
		copy_width = img_width;
	}

	if (nScrH<(int)img_height){
		sy = (int)(img_height-nScrH)/2;
		dy = 0;
		copy_height = nScrH;
	}else if (nScrH>(int)img_height){
		sy = 0;
		dy = (int)(nScrH-img_height)/2;
		copy_height = img_height;
	}else{
		copy_height = img_height;
	}

	#if 0
	OSA_printf("sx=%d,sy=%d dx=%d,dy=%d img(W/H):%d/%d scr(W/H):%d/%d cpy(W/H):%d/%d\n",
	sx,sy,dx,dy,img_width,img_height,nScrW,nScrH,copy_width,copy_height);
	#endif

	/* Iterate through all the image's pixels */
	for ( x = 0 ; x < copy_width ; ++x )
	{
		for ( y = 0 ; y < copy_height ; ++y )
		{
			/* Get pixel's RGB values */
			BMP_GetPixelRGB( bmp, x+sx, y+sy, &r, &g, &b );
			//pbuffer[y*nWidth+x] = (r<<16)|(g<<8)|b;
			pbuffer[(y+dy)*nScrW+x+dx] = (alpha<<24)|(b<<16)|(g<<8)|r;
		}
	}
	/* Free all memory allocated for the image */
	BMP_Free( bmp );
}


void drv_dsp_show_bmp(void)
{
	char path_img[512] = {0,};
	
	memset(path_img,0x00,512);
	snprintf(path_img, 512, "./sample_480x272.bmp");
	fill_bmpimage(path_img,(unsigned int*)(gSysDisp.ch[DISP_CH0].fb_bufPool[DISP_LAYER_1].virAddr),480,272,0);
}



int Display_Init(_disp *cfg)
{
	int ret = 0;
	int ch_idx=0, layer_idx=0;
	unsigned int disp_param[4]={0,};
	unsigned int nfbuf_size = 0;
	disp_layer_config config_video, config_ui;

	


	for (ch_idx=0; ch_idx<DISP_CH_MAXCNT; ch_idx++)
	{
		OSA_printf("DISP Cfg Name=%s\n", cfg[ch_idx].szName);
		OSA_printf("   sx=%d\n",cfg[ch_idx].canvas.nSX);
		OSA_printf("   sy=%d\n",cfg[ch_idx].canvas.nSY);
		OSA_printf("   width=%d\n",cfg[ch_idx].canvas.nWidth);
		OSA_printf("   height=%d\n\n",cfg[ch_idx].canvas.nHeight);
	}	

	memset(&gSysDisp,0x00,sizeof(sys_disp));
	gSysDisp.ch[DISP_CH0].output_std[DISP_LAYER_0] = cfg[DISP_CH0].nOutputStd;
	gSysDisp.ch[DISP_CH0].if_type[DISP_LAYER_0] = cfg[DISP_CH0].nOutputIF;
	gSysDisp.ch[DISP_CH0].fb_bufCnt[DISP_LAYER_0] = 8;
	gSysDisp.ch[DISP_CH0].fb_fmt[DISP_LAYER_0] = cfg[DISP_CH0].inPixFmt;
	//gSysDisp.fb_mode[DISP_LAYER_0] = DISP_MOD_NON_MB_UV_COMBINED;
    //gSysDisp.fb_seq[DISP_LAYER_0] = DISP_SEQ_UVUV;

	gSysDisp.ch[DISP_CH0].fb_bufCnt[DISP_LAYER_1] = 8;
    gSysDisp.ch[DISP_CH0].fb_fmt[DISP_LAYER_1] = DISP_FORMAT_ARGB_8888;
	//gSysDisp.fb_mode[DISP_LAYER_1] = DISP_MOD_INTERLEAVED;
	//gSysDisp.fb_seq[DISP_LAYER_1] = DISP_SEQ_ARGB;

	gSysDisp.exitThrDsp = FALSE;


	/* Open ION Mem device */
	ret = drv_ion_open(&gSysDisp.fd_phymem);
	if (ret){
		goto __error_close_dsp_dev;
	}

	nfbuf_size = cfg[DISP_CH0].canvas.nWidth*cfg[DISP_CH0].canvas.nHeight*gSysDisp.ch[DISP_CH0].fb_bufCnt[DISP_LAYER_1];
	gSysDisp.ch[DISP_CH0].fb_bufPool[DISP_LAYER_1].length = nfbuf_size;
	ret = drv_ion_alloc_buffer(gSysDisp.fd_phymem, nfbuf_size, 
								&gSysDisp.ch[DISP_CH0].fb_bufPool[DISP_LAYER_1].virAddr, 
								&gSysDisp.ch[DISP_CH0].fb_bufPool[DISP_LAYER_1].phyAddr, 
								&gSysDisp.hndl_ion);
	if (ret){
		goto __error_close_dsp_dev;
	}
	OSA_printf("Allocated memory summary\n");
	OSA_printf("	Virtual address:0x%08x\n",(unsigned int)gSysDisp.ch[DISP_CH0].fb_bufPool[DISP_LAYER_1].virAddr);
	OSA_printf("	Physical address:0x%08x\n",gSysDisp.ch[DISP_CH0].fb_bufPool[DISP_LAYER_1].phyAddr);
	memset(gSysDisp.ch[DISP_CH0].fb_bufPool[DISP_LAYER_1].virAddr,0x00,nfbuf_size);




	gSysDisp.fd_dsp = open(DISP_DEVICE, O_RDWR);
	if (gSysDisp.fd_dsp == -1) {
		OSA_ERROR("open %s failed(%s)\n", DISP_DEVICE, strerror(errno));
		goto __error_close_dsp_dev;
	}


	config_video.channel = 1;
	config_video.layer_id = 0;
	drv_dsp_get_layer_param(gSysDisp.fd_dsp, DISP_CH0, &config_video); //get fb para

	config_ui = config_video;

	/* Initiailize Video Layer */
	config_video.channel = 1;         //<< video channel
	config_video.layer_id = 0;
	config_video.info.alpha_mode = 0; //pixel alpha
	config_video.info.alpha_value = 0;
	config_video.info.zorder = ZORDER_MIN;
	//config_video.info.fb.addr[0] = 0;
	config_video.info.fb.addr[0] = gSysDisp.ch[DISP_CH0].fb_bufPool[DISP_LAYER_1].phyAddr+nfbuf_size/2;
	config_video.info.fb.format = DISP_FORMAT_YUV420_P;
	config_video.info.fb.color_space = DISP_BT601;
	config_video.info.screen_win.x      = 0;
	config_video.info.screen_win.y      = 0;
	config_video.info.screen_win.width  = cfg[DISP_CH0].canvas.nWidth;
	config_video.info.screen_win.height = cfg[DISP_CH0].canvas.nHeight;
	config_video.enable = 1;
	drv_dsp_set_layer_param(gSysDisp.fd_dsp, DISP_CH0, &config_video);
	gSysDisp.ch[DISP_CH0].layerId[DISP_LAYER_0] = HLAY(config_video.channel, config_video.layer_id);
	OSA_printf("Layer ID=%d\n",gSysDisp.ch[DISP_CH0].layerId[DISP_LAYER_0]);

	

	
	



	/* Initialize OSD layer */
	config_ui.channel = 2;           //<<gui channel
	config_ui.layer_id = 0;
	config_ui.info.alpha_mode = 0;   //pixel alpha
	config_ui.info.alpha_value = 255;
	config_ui.info.zorder = ZORDER_MAX;
	config_ui.info.fb.addr[0] = (unsigned int)(gSysDisp.ch[DISP_CH0].fb_bufPool[DISP_LAYER_1].phyAddr);
	#if 0
	config_ui.info.fb.size[0].width = cfg[DISP_CH0].canvas.nWidth;
	config_ui.info.fb.size[0].height = cfg[DISP_CH0].canvas.nHeight;
	config_ui.info.fb.size[1].width = config_ui.info.fb.size[0].width;
	config_ui.info.fb.size[1].height = config_ui.info.fb.size[0].height;
	config_ui.info.fb.size[2].width = config_ui.info.fb.size[1].width;
	config_ui.info.fb.size[2].height = config_ui.info.fb.size[1].height;
	#else
	OSA_printf("config_ui.info.fb.size[0] --> width=%d hegit=%d\n",config_ui.info.fb.size[0].width,config_ui.info.fb.size[0].height);
	OSA_printf("config_ui.info.fb.size[1] --> width=%d hegit=%d\n",config_ui.info.fb.size[1].width,config_ui.info.fb.size[1].height);
	OSA_printf("config_ui.info.fb.size[2] --> width=%d hegit=%d\n",config_ui.info.fb.size[2].width,config_ui.info.fb.size[2].height);
	OSA_printf("config_ui.info.fb.crop --> x=0x%llx y=0x%llx, width=0x%llx hegit=0x%llx\n",
	config_ui.info.fb.crop.x,config_ui.info.fb.crop.y,
	config_ui.info.fb.crop.width,config_ui.info.fb.crop.height);
	#endif
	//config_ui.info.fb.crop.width= cfg[DISP_CH0].canvas.nWidth;
	//config_ui.info.fb.crop.height = cfg[DISP_CH0].canvas.nHeight;
	config_ui.info.fb.format = DISP_FORMAT_ARGB_8888;
	config_ui.info.fb.color_space = DISP_BT601;
	config_ui.info.screen_win.x      = 0;
	config_ui.info.screen_win.y      = 0;
	config_ui.info.screen_win.width  = cfg[DISP_CH0].canvas.nWidth;
	config_ui.info.screen_win.height = cfg[DISP_CH0].canvas.nHeight;
	config_ui.enable = 1;
	drv_dsp_set_layer_param(gSysDisp.fd_dsp, DISP_CH0, &config_ui);
	gSysDisp.ch[DISP_CH0].layerId[DISP_LAYER_1] = HLAY(config_ui.channel, config_ui.layer_id);
	OSA_printf("Layer ID=%d\n",gSysDisp.ch[DISP_CH0].layerId[DISP_LAYER_1]);
	

	

	
	
	OSA_printf("Display init: Done\n");
	
	return OSA_SOK;



__error_close_layer_dev:
	for (ch_idx=0; ch_idx<DISP_CH_MAXCNT; ch_idx++)
	{
		for (layer_idx=0; layer_idx<DISP_LAYER_MAXCNT; layer_idx++)
		{
			if( gSysDisp.ch[ch_idx].layerId[layer_idx]>0 )
			{
				disp_layer_config config;
				memset(&config, 0, sizeof(disp_layer_config));

				config.channel	= HD2CHN(gSysDisp.ch[ch_idx].layerId[layer_idx]);
				config.layer_id = HD2LYL(gSysDisp.ch[ch_idx].layerId[layer_idx]);
				drv_dsp_get_layer_param(gSysDisp.fd_dsp, DISP_CH0, &config);

				config.enable = 1;
				drv_dsp_set_layer_param(gSysDisp.fd_dsp, DISP_CH0, &config);
				gSysDisp.ch[ch_idx].layerId[layer_idx] = 0;
		    }
	    }
    }
    


__error_close_dsp_dev:
	/* Close display device driver */
	if(gSysDisp.fd_dsp){
		close(gSysDisp.fd_dsp);
		gSysDisp.fd_dsp = -1;
	}

	/* Close physical memory device driver */
	if(gSysDisp.fd_phymem>0){
		ret = drv_ion_free_buffer(gSysDisp.fd_phymem, &gSysDisp.hndl_ion, 
								(void*)gSysDisp.ch[DISP_CH0].fb_bufPool[DISP_LAYER_0].virAddr, 
								nfbuf_size);
		if (ret){
			OSA_ERROR("Failed to free ion buffer.\n");
		}
	
		close(gSysDisp.fd_phymem);
	}
	return OSA_EFAIL;

}




int Display_deinit(void)
{
	int ret=0;
    OSA_printf("Display deinit: Done\n");
    
    /* Close display device driver */
	if(gSysDisp.fd_dsp){
		close(gSysDisp.fd_dsp);
		gSysDisp.fd_dsp = -1;
	}

	/* Close physical memory device driver */
	if(gSysDisp.fd_phymem>0){
		ret = drv_ion_free_buffer(gSysDisp.fd_phymem, &gSysDisp.hndl_ion, 
									(void*)gSysDisp.ch[DISP_CH0].fb_bufPool[DISP_LAYER_0].virAddr, 
									gSysDisp.ch[DISP_CH0].fb_bufPool[DISP_LAYER_0].length);
		if (ret){
			OSA_ERROR("Failed to free ion buffer.\n");
		}
	
		close(gSysDisp.fd_phymem);
	}
	

	return OSA_SOK;
}






#define WR_FILE 1
int Display_thread_ch0(void* pParam)
{	
	int status = OSA_EFAIL;
	int nCapBufIdx = 0;
	OSA_TskHndl* pTaskHndl = (OSA_TskHndl*)pParam;

	#ifdef WR_FILE
		unsigned int gCurFrameCnt = 1;
		char szFileName[128]={0,};
		FILE *file_fd;
	#endif


	/*< DISPLAY ON */
	drv_dsp_lcd_on(TRUE);
	OSA_printf("DISPLAY LCD on\n\n");

	drv_dsp_start();
	OSA_printf("DISPLAY start\n\n");

	


	while( gSysDisp.exitThrDsp==FALSE )
	{
		int dspBufId=0;
		OSA_BufInfo *pBufInfo=NULL;
		
		if(pTaskHndl->buffer[DISP_CH0]==NULL){
			//OSA_printf("Buffer is Invalid....\n");
		}else{
			if( OSA_bufGetFull(pTaskHndl->buffer[DISP_CH0], &dspBufId, OSA_TIMEOUT_NONE) == OSA_SOK )
			{
				_win video_win;
				pBufInfo = OSA_bufGetBufInfo(pTaskHndl->buffer[DISP_CH0], dspBufId);
				#if 0
				OSA_printf("Preview: (w/h:%d/%d) phyY=0x%08x, phyC=0x%08x tstemp=%lld\n",
					pBufInfo->PrevWidth,pBufInfo->PrevHeight,
					pBufInfo->PrevAddrPhyY,
					pBufInfo->PrevAddrPhyC,
					pBufInfo->timestamp);
				#endif

				#if 0
				#ifdef WR_FILE
					if(gCurFrameCnt%30==0)
					{
						snprintf(szFileName,128,"./dump_%04d.yuv",gCurFrameCnt);
						file_fd = fopen(szFileName,"wb");
						//fwrite(pBufInfo->PrevAddrVirY, pBufInfo->PrevWidth*pBufInfo->PrevHeight*3/2, 1, file_fd);
						fwrite(pBufInfo->addrVirY+1920*1088*3/2, 480*272*3/2, 1, file_fd);
						if(file_fd){
							fclose(file_fd);
						}
					}
					gCurFrameCnt++;
				#endif
				#else
				video_win.nSX = 0;
				video_win.nSY = 0;
				video_win.nWidth = pBufInfo->PrevWidth;
				video_win.nHeight = pBufInfo->PrevHeight;
				status = drv_dsp_layer_set_src(gSysDisp.fd_dsp, DISP_CH0,
												gSysDisp.ch[DISP_CH0].layerId[DISP_LAYER_0],
												video_win,video_win,
												pBufInfo->pixFmt);
												
				drv_dsp_layer_render(gSysDisp.fd_dsp, DISP_CH0,
									gSysDisp.ch[DISP_CH0].layerId[DISP_LAYER_0],
									pBufInfo->PrevWidth, pBufInfo->PrevHeight, 
									pBufInfo->PrevAddrPhyY, pBufInfo->PrevAddrPhyC, pBufInfo->pixFmt);
				#endif

				OSA_bufPutEmpty(pTaskHndl->buffer[DISP_CH0], dspBufId);
			}
		}
		OSA_waitMsecs(1);
	}



	/*< DISPLAY STOP */
	drv_dsp_stop();



	OSA_printf("Display Finish...\n");

	return OSA_SOK;
}






/**
 * @name    SignalTraps()
 * @brief   signal trap function
 * @ingroup Group_FunctionTest
 * @param: None
 *
 * @retval: None
 *
 * Example Usage:
 * @code
 *  SignalTraps();
 * @endcode
 */
int TaskDisplay( struct OSA_TskHndl *pPrc, OSA_MsgHndl *pMsg, Uint32 curState )
{
	int retVal = OSA_SOK;
	int status = OSA_SOK;
	int curStatus = -1;
	Uint16 cmd = NULL;
	Config * pCfg = (Config*)pPrc->pParamCfg;


	/**! Loop operation */
	if (pMsg==NULL)
	{
		//OSA_waitMsecs(1000);
		//OSA_printf("Task[Display] Loop Operation...\n");
	}
	else
	{
		cmd = OSA_msgGetCmd(pMsg);

		switch(cmd)
		{
			case TASK_CMD_READY:
				OSA_printf("[DISPLAY TASK] Get READY command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_READY);
				break;

			case TASK_CMD_INIT:
				OSA_printf("[DISPLAY TASK] Get INIT command\n");

				OSA_printf("Display Buffer Linked at 0x%x\n",pPrc->buffer[0]);

				curStatus = OSA_tskGetState(pPrc);
				
				if( curStatus==TASK_STATUS_READY )
				{
					if( Display_Init(pCfg->display)!=OSA_SOK ){
						OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
						OSA_tskSetState(pPrc,TASK_STATUS_ERROR);
					}else{				
						/** Test display scale */
						drv_dsp_show_bmp();
						//drv_dsp_scale_test();
						OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
						OSA_tskSetState(pPrc,TASK_STATUS_INIT);
					}
				}
				else
				{
					OSA_ERROR("Invalid cur status\n");	
					OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
				}
				break;

			case TASK_CMD_DEINIT:
				OSA_printf("[DISPLAY TASK] Get DEINIT command\n");
				curStatus = OSA_tskGetState(pPrc);

				if( curStatus==TASK_STATUS_STOP )
				{
					Display_deinit();
					OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
					OSA_tskSetState(pPrc,TASK_STATUS_DEINIT);
				}
				else
				{
					OSA_ERROR("Invalid cur status\n");	
					OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
				}
				break;
				
			case TASK_CMD_RUN:
				OSA_printf("[DISPLAY TASK] Get RUN command\n");

				curStatus = OSA_tskGetState(pPrc);
				
				if( curStatus==TASK_STATUS_INIT )
				{
					status = OSA_thrCreate(&gSysDisp.thrDsp, Display_thread_ch0, 0, 1024, pPrc);
				    if(status==OSA_SOK) {
				    	OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
						OSA_tskSetState(pPrc,TASK_STATUS_RUN);
				    }else{
				    	OSA_ERROR("Fail to create display thread for ch0\n");
				    	OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
				    	OSA_tskSetState(pPrc,TASK_STATUS_ERROR);
				    }
			    }else{
			    	OSA_ERROR("Invalid cur status\n");	
			    	OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
			    }
				break;
				
			case TASK_CMD_STOP:
				OSA_printf("[DISPLAY TASK] Get STOP command\n");

				curStatus = OSA_tskGetState(pPrc);

				if( curStatus!=TASK_STATUS_INIT && 
					curStatus!=TASK_STATUS_DEINIT &&
					curStatus!=TASK_STATUS_READY)
				{
					gSysDisp.exitThrDsp=TRUE;

					OSA_thrDelete(&gSysDisp.thrDsp);

					OSA_printf("Display ch0 thread deleted\n");

					OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
					OSA_tskSetState(pPrc,TASK_STATUS_STOP);
				}
				else
				{
					OSA_ERROR("Invalid cur status\n");	
					OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
				}
				break;
				
			case TASK_CMD_RESUME:
				OSA_printf("[DISPLAY TASK] Get RESUME command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_RESUME);
				break;
				
			case TASK_CMD_PAUSE:
				OSA_printf("[DISPLAY TASK] Get PAUSE command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_PAUSE);
				break;
				
			case TASK_CMD_RESET:
				OSA_printf("[DISPLAY TASK] Get RESET command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_RESET);
				break;
				
			default:
				break;
		}
	}

	return retVal;
}





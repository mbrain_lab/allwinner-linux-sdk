/**
 * @file		ft_lcd.h
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date	2015.07.12
 * @brief	File helper APIs
 *
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain
 * 
 * (c) Copyright mBrain(2015).	All Rights Reserved.
 */
/*! [ 1]. File classification */
#ifndef _FT_LCD_H_
#define _FT_LCD_H_

/*! [ 2]. File Directive (#include) */
/*! [ 3]. Global macro and enum */
#define DEV_NAME	"/dev/disp"

#define SCREEN_0 0
#define SCREEN_1 1

#define ZORDER_MAX 11
#define ZORDER_MIN 0


#define CHN_NUM 3
#define LYL_NUM 4
#define HLAY(chn, lyl) (chn*4+lyl)
#define HD2CHN(hlay) (hlay/4)
#define HD2LYL(hlay) (hlay%4)

enum
{
	HWD_STATUS_REQUESTED    = 1,
	HWD_STATUS_NOTUSED		= 2,
    HWD_STATUS_OPENED       = 4
};

/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
/*! [ 5]. Global Function prototype declaration */
/*! [ 6]. Global Variable declaration  */

#endif /* _FT_LCD_H_ */








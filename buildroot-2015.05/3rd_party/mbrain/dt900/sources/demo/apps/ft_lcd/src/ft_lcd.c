/**
 * @file   ft_hdmi.c
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.07.07
 * @brief  HDMI funnction test for Hummingbird A31
 *
 * Detailed description of file.
 *
 * @copyright
 * (c) Copyright MBRAIN & mBrain(2015).  All Rights Reserved.
 */

/*! [ 1]. File Directive (#include) */
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <osa.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/unistd.h>
#include <sys/mman.h>
#include <asm/types.h>
#include <linux/types.h>

#include <fcntl.h>


#include <linux/ion.h>
#include <linux/ion_sunxi.h>

#include <linux/fb.h>
#include <video/sunxi_display2.h>


#include "ft_lcd.h"


#include <qdbmp.h>

/*! [ 2]. Local Macro (#define, enum) */
#define ION_DEVICE "/dev/ion"


#define LCD_WIDTH						480
#define LCD_HEIGHT						272

#define COLOR_WHITE                     0xffffffff
#define COLOR_YELLOW                    0xffffff00
#define COLOR_GREEN                     0xff00ff00
#define COLOR_CYAN                      0xff00ffff
#define COLOR_MAGENTA                   0xffff00ff
#define COLOR_RED                       0xffff0000
#define COLOR_BLUE                      0xff0000ff
#define COLOR_BLACK                     0xff000000


/*! [ 3]. Type definition or structure declaration (typedef or struct) */
static unsigned int colorbar[8] =
{
	COLOR_WHITE,
	COLOR_YELLOW,
	COLOR_CYAN,
	COLOR_GREEN,
	COLOR_MAGENTA,
	COLOR_RED,
	COLOR_BLUE,
	COLOR_BLACK
};



/*! [ 4]. Local Function prototype declaration */
/*! [ 5]. Local Variable declaration */
int           gIsExitMainTask=FALSE;

unsigned int mLayerStatus[CHN_NUM][LYL_NUM];


static void SignalTraps(void);
static void SignalHandler(int sig);

void SignalTraps(void)
{
  OSA_attachSignalHandler(SIGTERM, SignalHandler);
  OSA_attachSignalHandler(SIGINT,  SignalHandler);
}

void SignalHandler(int sig)
{
  if ( (sig==SIGTERM)||(sig==SIGINT) ){
	OSA_printf("gIsExitRecvThr was setted with 1\n");
	gIsExitMainTask=TRUE;
  }

}


int fill_colorbar(unsigned char* pBuffer, int nWidth, int nHeight)
{
	int i=0,j=0;
	unsigned int *p=NULL;

	for (i = 0; i < nHeight; i++)
	{
		p = (unsigned int *)(pBuffer + nWidth* i * 4);
		for (j = 0; j < nWidth; j++)
		{
			int idx = (j << 3) / nWidth;
			*p++ = colorbar[idx];
		}
	}
	return 0;
}


void fill_bmpimage(const char* szFileName, unsigned int* pbuffer, int nScrW, int nScrH)
{
	int sx=0,sy=0, dx=0,dy=0;
/* Load bmp file */
	unsigned char	r, g, b;
	UINT	img_width, img_height, copy_width, copy_height;
	UINT	x, y;
	BMP*	bmp=NULL;

	/* Read an image file */
	bmp = BMP_ReadFile(szFileName);
	if (bmp==NULL){
		OSA_ERROR("Cannot open image:%s\n",szFileName);
		return;
	}

	/* Get image's dimensions */
	img_width = BMP_GetWidth( bmp );
	img_height = BMP_GetHeight( bmp );

	if (nScrW<(int)img_width){
		sx = (int)(img_width-nScrW)/2;
		dx = 0;
		copy_width = nScrW;
	}else if (nScrW>(int)img_width){
		dx = (int)(nScrW-img_width)/2;
		sx = 0;
		copy_width = img_width;
	}else{
		copy_width = img_width;
	}

	if (nScrH<(int)img_height){
		sy = (int)(img_height-nScrH)/2;
		dy = 0;
		copy_height = nScrH;
	}else if (nScrH>(int)img_height){
		sy = 0;
		dy = (int)(nScrH-img_height)/2;
		copy_height = img_height;
	}else{
		copy_height = img_height;
	}

	OSA_printf("sx=%d,sy=%d dx=%d,dy=%d img(W/H):%d/%d scr(W/H):%d/%d cpy(W/H):%d/%d\n",
	sx,sy,dx,dy,img_width,img_height,nScrW,nScrH,copy_width,copy_height);


	/* Iterate through all the image's pixels */
	for ( x = 0 ; x < copy_width ; ++x )
	{
		for ( y = 0 ; y < copy_height ; ++y )
		{
			/* Get pixel's RGB values */
			BMP_GetPixelRGB( bmp, x+sx, y+sy, &r, &g, &b );
			//pbuffer[y*nWidth+x] = (r<<16)|(g<<8)|b;
			pbuffer[(y+dy)*nScrW+x+dx] = (128<<24)|(b<<16)|(g<<8)|r;
		}
	}
	/* Free all memory allocated for the image */
	BMP_Free( bmp );
}




int drv_ion_open(int* pFd)
{
	if (pFd==NULL){
		OSA_ERROR("Invalid device handle\n");
		return OSA_EFAIL;
	}

	if(*pFd == 0){
		*pFd = open(ION_DEVICE, O_RDONLY);
	}

	if(*pFd < 0 ) {
		OSA_ERROR("Failed to open ion device - %s",strerror(errno));
		*pFd = 0;
		return -errno;
	}

	OSA_printf("ION drive opend\n");
	return OSA_SOK;
}

void drv_ion_close(int* pFd)
{
	if( pFd != NULL){
		if( *pFd ){
			close(*pFd);
			*pFd = 0;
		}
	}
}

int drv_ion_alloc_buffer(int fd, int nSize, unsigned char** pVir, unsigned long* phyAddr, struct ion_handle_data* pHndl)
{
	int err = 0;
	struct ion_fd_data fd_data;
	struct ion_allocation_data ionAllocData;
	struct ion_custom_data custom_data;
	sunxi_phys_data phys_data;
	void* buffer=NULL;

	if( pHndl==NULL || fd<0 ){
		OSA_ERROR("Invalid parameters\n");
		return OSA_EFAIL;
	}


	ionAllocData.len = nSize;
	ionAllocData.align = 0;
	ionAllocData.heap_id_mask = ION_HEAP_CARVEOUT_MASK;
	ionAllocData.flags = 0;
	err = ioctl(fd, ION_IOC_ALLOC, &ionAllocData);
	if(err) {
		OSA_ERROR("ION_IOC_ALLOC failed.\n");
		return OSA_EFAIL;
	}

	fd_data.handle = ionAllocData.handle;
	pHndl->handle = ionAllocData.handle;
	err = ioctl(fd, ION_IOC_MAP, &fd_data);
	if(err) {
		OSA_ERROR("ION_IOC_MAP failed.\n");
		ioctl(fd, ION_IOC_FREE, pHndl);
		return OSA_EFAIL;
	}

	/* mmap to user space */
	buffer = mmap(NULL, ionAllocData.len, PROT_READ|PROT_WRITE, MAP_SHARED, fd_data.fd, 0);
	if(buffer == MAP_FAILED) {
		OSA_ERROR("Failed to map the allocated memory.");
		ioctl(fd, ION_IOC_FREE, pHndl);
		return OSA_EFAIL;
	}
	memset(buffer, 0, ionAllocData.len);
	*pVir = buffer;

	/* Get physical address */
	custom_data.cmd = ION_IOC_SUNXI_PHYS_ADDR;
	phys_data.handle = ionAllocData.handle;
	custom_data.arg = (unsigned long)&phys_data;
	err = ioctl(fd, ION_IOC_CUSTOM, &custom_data);
	if(err)
	{
		OSA_ERROR("Failed to get physical address.\n");
		return OSA_EFAIL;
	}

	*phyAddr = (unsigned long)phys_data.phys_addr;

	return OSA_SOK;
}

int drv_ion_free_buffer(int fd, struct ion_handle_data* pHndl, void *base, size_t size)
{
	int err = OSA_SOK;

	OSA_printf("ion: Unmapping buffer  base:%p size:%d", base, size);
	if(munmap(base, size)) {
		OSA_ERROR("ion: Failed to unmap memory at %p.", base);
	}

	ioctl(fd, ION_IOC_FREE, pHndl);
	OSA_printf("ion: Allocated buffer base:%p size:%d fd:%d",base, size, fd);

	return err;
}












int drv_dsp_config(int disp_fd, int nScrrenNum,  __DISP_t cmd, disp_layer_config *pinfo)
{
	unsigned long args[4] = {0};
	unsigned int ret = OSA_SOK;

	OSA_printf("CMD=%d\n",cmd);

	args[0] = nScrrenNum;
	args[1] = (unsigned long)pinfo;
	args[2] = 1;
	ret = ioctl(disp_fd, cmd, args);
	if(OSA_SOK != ret) {
		OSA_ERROR("fail to processing para\n");
		ret = OSA_EFAIL;
	}
	return ret;
}


int drv_dsp_get_layer_param(int disp_fd, int nScrrenNum, disp_layer_config *pinfo)
{
	return drv_dsp_config(disp_fd, nScrrenNum, DISP_LAYER_GET_CONFIG, pinfo);
}

int drv_dsp_set_layer_param(int disp_fd, int nScrrenNum, disp_layer_config *pinfo)
{
	return drv_dsp_config(disp_fd, nScrrenNum, DISP_LAYER_SET_CONFIG, pinfo);
}



int drv_dsp_get_layer(int *pCh, int *pId)	//finish
{
	int ch;
	int id;
	{
		for(id=0; id<LYL_NUM; id++) {
			for (ch=0; ch<CHN_NUM; ch++) {
				if (!(mLayerStatus[ch][id] & HWD_STATUS_REQUESTED)) {
					mLayerStatus[ch][id] |= HWD_STATUS_REQUESTED;
					goto out;
				}
			}
		}
	}
out:
	if ((ch==CHN_NUM) && (id==LYL_NUM)) {
		OSA_ERROR("all layer used.\n");
		return OSA_EFAIL;
	}
	*pCh = ch;
	*pId = id;
	OSA_printf("requested: ch:%d, id:%d\n", ch, id);
	return HLAY(ch, id);
}



int drv_dsp_request_layer(int disp_fd, int nScrrenNum, int sx, int sy, int w, int h)
{
	int hlay;
	int ch, id;
	disp_layer_config config;

	memset(&config, 0, sizeof(disp_layer_config));
	hlay = drv_dsp_get_layer(&ch,&id);
	if (hlay<0){
		OSA_ERROR("drv_dsp_get_layer() fail\n");
		return OSA_EFAIL;
	}
	config.channel = ch;
	config.layer_id = id;
	config.enable = 0;
	config.info.screen_win.x = sx;
	config.info.screen_win.y = sy;
	config.info.screen_win.width	= w;
	config.info.screen_win.height	= h;
	config.info.mode = LAYER_MODE_BUFFER;
	config.info.alpha_mode = 0;
	config.info.alpha_value = 128;
	config.info.fb.flags = DISP_BF_NORMAL;
	config.info.fb.scan = DISP_SCAN_PROGRESSIVE;
	config.info.fb.color_space = DISP_BT601; //DISP_BT709;
	config.info.zorder = 0;
	OSA_printf("hlay:%d, zorder=%d", hlay, config.info.zorder);
	drv_dsp_set_layer_param(disp_fd, nScrrenNum, &config);

	return hlay;
}

int drv_dsp_layer_open(int disp_fd, int nScrrenNum, unsigned int hlay)
{
	disp_layer_config config;
	memset(&config, 0, sizeof(disp_layer_config));
	config.channel	= HD2CHN(hlay);
	config.layer_id = HD2LYL(hlay);
	drv_dsp_get_layer_param(disp_fd, nScrrenNum, &config);

	config.enable = 1;
	return drv_dsp_set_layer_param(disp_fd, nScrrenNum, &config);
}


int drv_dsp_layer_close(int disp_fd, int nScrrenNum, unsigned int hlay)
{
	disp_layer_config config;
	memset(&config, 0, sizeof(disp_layer_config));
	config.channel	= HD2CHN(hlay);
	config.layer_id = HD2LYL(hlay);
	drv_dsp_get_layer_param(disp_fd, nScrrenNum, &config);

	config.enable = 0;
	return drv_dsp_set_layer_param(disp_fd, nScrrenNum, &config);
}


/**
 * @name    main()
 * @brief   main function
 * @ingroup Group_FunctionTest
 * @param <Reserved>
 *
 * @retval <Reserved>
 *
 * Example Usage:
 * @code
 *  1. insmode device drivers
 *     insmod /lib/modules/3.3.0/kernel/drivers/video/sun6i/disp/disp.ko
 *     insmod /lib/modules/3.3.0/kernel/drivers/video/sun6i/lcd/lcd.ko
 *     insmod /lib/modules/3.3.0/kernel/drivers/video/sun6i/hdmi/hdmi.ko
 * @endcode
 */
int main(int argc, char **argv)
{
	int ret;
	int fd_disp=0,fd_ion=0;
	unsigned int layer_id = 0;
	unsigned int screen_width=0,screen_height=0;
	unsigned int disp_param[4]={0,};

	/* Ion device variables */
	struct ion_handle_data hndl_ion_data;

	/* Frame buffer for drawing */
	unsigned char *fbuf_vir = NULL;
	unsigned int nfbuf_size = 0;
	unsigned long pbuf_phy = 0;

	unsigned int loop_cnt=0;
	int test_flag=0;

	/* Image path */
	char path_img[512] = {0,};





	/* Init OSA library */
	OSA_init();

	/* Register signal */
	SignalTraps();


	OSA_printf("[%s] Start\n",__FILE__);



	/* Open ION Mem device */
	ret = drv_ion_open(&fd_ion);
	if (ret){
		goto err;
	}

	nfbuf_size = LCD_WIDTH*LCD_HEIGHT*8;
	ret = drv_ion_alloc_buffer(fd_ion, nfbuf_size, &fbuf_vir, &pbuf_phy, &hndl_ion_data);
	if (ret){
		goto err_free_ionmem;
	}
	OSA_printf("Allocated memory summary\n");
	OSA_printf("    Virtual address:0x%08x\n",(unsigned int)fbuf_vir);
	OSA_printf("    Physical address:0x%08x\n",pbuf_phy);




	/* Initizlize layer status */
	memset(mLayerStatus, 0, sizeof(mLayerStatus));

	fd_disp = open(DEV_NAME, O_RDWR);
	if (fd_disp == -1) {
		OSA_ERROR("open %s failed(%s)\n", DEV_NAME, strerror(errno));
		goto err;
	}

	disp_param[0] = 0;
	disp_param[1] = DISP_OUTPUT_TYPE_LCD; //<< DISP_OUTPUT_TYPE_NONE : off
	ret = ioctl(fd_disp, DISP_DEVICE_SWITCH, (void*)disp_param);
	if (ret < 0) {
		OSA_ERROR("hdmitester: set hdmi output mode failed(%d)\n", ret);
		goto err;
	}





	disp_layer_config config_ui, config_pip;

	config_ui.channel = 1;
	config_ui.layer_id = 0;
	drv_dsp_get_layer_param(fd_disp, SCREEN_0, &config_ui); //get fb para

	config_pip = config_ui;

	config_ui.channel = 1; //ui channel
	config_ui.layer_id = 0;
	config_ui.info.alpha_mode = 0; //pixel alpha
	config_ui.info.alpha_value =128;
	config_ui.info.zorder = ZORDER_MIN;
	config_ui.info.fb.addr[0] = (unsigned int)pbuf_phy;
	config_ui.info.fb.format = DISP_FORMAT_ARGB_8888;
	config_ui.info.fb.color_space = DISP_BT601;
	config_ui.info.screen_win.x      = 0;
	config_ui.info.screen_win.y      = 0;
	config_ui.info.screen_win.width  = LCD_WIDTH;
	config_ui.info.screen_win.height = LCD_HEIGHT;
	config_ui.enable = 1;
	drv_dsp_set_layer_param(fd_disp, SCREEN_0, &config_ui);

	config_pip.channel = 1; //ui channel
	config_pip.layer_id = 1;
	config_pip.info.alpha_mode = 0; //pixel alpha
	config_pip.info.alpha_value =128;
	config_pip.info.zorder = ZORDER_MAX;
	config_pip.info.fb.addr[0] = (unsigned int)(pbuf_phy+LCD_WIDTH*LCD_HEIGHT*4);
	config_pip.info.fb.format = DISP_FORMAT_ARGB_8888;
	config_pip.info.fb.color_space = DISP_BT601;
	config_pip.info.screen_win.x      = 0;
	config_pip.info.screen_win.y      = 0;
	config_pip.info.screen_win.width  = LCD_WIDTH;
	config_pip.info.screen_win.height = LCD_HEIGHT;
	config_pip.enable = 1;
	drv_dsp_set_layer_param(fd_disp, SCREEN_0, &config_pip);

	//layer_id = drv_dsp_request_layer(fd_disp, SCREEN_0, 0, 0, LCD_WIDTH, LCD_HEIGHT);

	//drv_dsp_layer_open(fd_disp, SCREEN_0, layer_id);


	//memset(fbuf_vir, 0xFF, LCD_WIDTH*LCD_HEIGHT);
	fill_colorbar(fbuf_vir,LCD_WIDTH,LCD_HEIGHT);
	snprintf(path_img, 512, "%s/sample_bmp.bmp", get_current_dir_name());
	fill_bmpimage(path_img,(unsigned int*)fbuf_vir,LCD_WIDTH,LCD_HEIGHT);
	memset(path_img,0x00,512);
	snprintf(path_img, 512, "%s/sample_pip.bmp", get_current_dir_name());
	fill_bmpimage(path_img,(unsigned int*)(fbuf_vir+LCD_WIDTH*LCD_HEIGHT*4),LCD_WIDTH,LCD_HEIGHT);


	#if 0
	config.channel = 2;
	config.layer_id = 1;
	drv_dsp_get_layer_param(fd_disp, SCREEN_0, &config); //get fb para

	config.channel = 2; //ui channel
	config.info.alpha_mode = 0; //pixel alpha
	config.info.alpha_value = 0; //pixel alpha
	config.info.zorder = ZORDER_MAX-1;
	config.info.fb.addr[0] = (unsigned int)(pbuf_phy+LCD_WIDTH*LCD_HEIGHT*4);
	config.info.fb.format = DISP_FORMAT_ARGB_8888;
	config.info.fb.color_space = DISP_BT601;
	config.info.screen_win.x      = 0;
	config.info.screen_win.y      = 0;
	config.info.screen_win.width  = 320;
	config.info.screen_win.height = 240;
	drv_dsp_set_layer_param(fd_disp, SCREEN_0, &config);

	config.channel = 2;
	config.enable = 1;
	drv_dsp_set_layer_param(fd_disp, SCREEN_0, &config);
	fill_bmpimage("/media/mmcblk0p1/ft_lcd/sample_pip.bmp",(unsigned int*)(fbuf_vir+LCD_WIDTH*LCD_HEIGHT*4),320,240);
	#endif
















	while(gIsExitMainTask==FALSE)
	{
		OSA_waitMsecs(10);
		loop_cnt++;
		if(loop_cnt%200==0){
			if (test_flag==0){
				config_pip.enable = 0;
				test_flag = 1;
			}else{
				config_pip.enable = 1;
				test_flag = 0;
			}
			drv_dsp_set_layer_param(fd_disp, SCREEN_0, &config_pip);
		}
	}








err_free_ionmem:
	ret = drv_ion_free_buffer(fd_ion, &hndl_ion_data, (void*)fbuf_vir, nfbuf_size);
	if (ret){
		OSA_ERROR("Failed to free ion buffer.\n");
	}

err:
	/* Close DISP driver */
	close(fd_disp);

	/* Close ION driver */
	if(fd_ion){
		drv_ion_close(&fd_ion);
	}

	/*! Deinit OSA library */
	OSA_exit();
	return OSA_SOK;
}

/**
 * @file   drv_gpio.h
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.07.12
 * @brief  GPIO user space driver
 * 
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain
 * 
 * (c) Copyright mBrain(2015).  All Rights Reserved.
 */


/*! [ 1]. File classification */

#ifndef _DRV_GPIO_H_
#define _DRV_GPIO_H_

#  ifdef __cplusplus
extern "C" {
#  endif /* __cplusplus */

/*! [ 2]. File Directive (#include) */
#include <osa.h>


/*! [ 3]. Global macro and enum*/
enum GPIO_PORT_GROUP{
	PORT_A       =0,
	PORT_B         ,
	PORT_C         ,
	PORT_D         ,
	PORT_E         ,
	PORT_F         ,
	PORT_G         ,
	PORT_H         ,
	PORT_L         ,
	PORT_M         ,	
	PORT_AXP       ,	
    PORT_MAXCNT
};

/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
/*! [ 5]. Global Function prototype declaration */
extern int DRV_GpioSetVal(int portGroup, int nPinNum, int bVal);

/*! [ 6]. Global Variable declaration  */





#  ifdef __cplusplus
    }
#  endif /* __cplusplus */
#endif //_DRV_GPIO_H_


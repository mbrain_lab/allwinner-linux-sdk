/**
 * @file   drv_spi.c
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.07.12
 * @brief  SPI user space driver
 * 
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain
 * 
 * (c) Copyright mBrain(2015).  All Rights Reserved.
 */


/*! [ 1]. File Directive (#include) */
#include <osa.h>

/* Standard Header files */
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#include <drv_spi.h>


/*! [ 2]. Local Macro (#define, enum) */
/*! [ 3]. Type definition or structure declaration (typedef or struct */
/*! [ 4]. Local Function prototype declaration */


/**
 * @name	DRV_spiInit()
 * @brief	This function initialize spi device
 * @ingroup Group_DrvUser
 *
 * This API provides certain actions as an example.
 *
 * @param [devType] device type
 *
 * @retval	0 Success
 * @retval -1 Failure
 *
 * Example Usage:
 * @code
 *	  <Reserved>
 * @endcode
 */
DRV_SpiHndl* DRV_spiInit(DRV_SPICreatParam* pParam, OSA_MutexHndl* pLock)
{
	DRV_SpiHndl* hndl = NULL;

	if (pParam == NULL){
		OSA_ERROR("Invalid creation parameter\n");
		goto _exitInit;
	}

	if( access( pParam->dev, F_OK ) == -1 ) {
		OSA_ERROR("Given device[%s] doesn't exist\n",pParam->dev);
		goto _exitInit;
	}

	hndl = (DRV_SpiHndl*)OSA_memAlloc(sizeof(DRV_SpiHndl));
	if (hndl == NULL){
		OSA_ERROR("Out of memory\n");
		goto _exitInit;
	}

	hndl->fd = open(pParam->dev, O_RDWR);
	if(hndl->fd<0){
		OSA_ERROR("Cannot open device: %s\n",pParam->dev);
		goto _exitInit;
	}else{
		OSA_printf("SPI Device[%s] Opened...\n",pParam->dev);
	}

	if (pLock != NULL){
		OSA_printf("SPI Device[%s] using external mutex object\n",pParam->dev);
		hndl->spiBusLock = *pLock;
		hndl->isExternalLock = TRUE;
	}else{
		OSA_printf("SPI Device[%s] using create mutex object\n",pParam->dev);
		OSA_mutexCreate(&hndl->spiBusLock);
		hndl->isExternalLock = FALSE;
	}




	/*
	* SPI Operation Mode
	*/
	if ( ioctl(hndl->fd, SPI_IOC_WR_MODE, &pParam->mode)==OSA_EFAIL ){
		OSA_ERROR("can't set spi mode[%s]\n",pParam->dev );
		goto _exitDevIoctlFail;
	}

	if ( ioctl(hndl->fd, SPI_IOC_RD_MODE, &pParam->mode)==OSA_EFAIL ){
		OSA_ERROR("can't get spi mode[%s]\n",pParam->dev );
		goto _exitDevIoctlFail;
	}


	/*
	* bits per word
	*/
	if ( ioctl(hndl->fd, SPI_IOC_WR_BITS_PER_WORD, &pParam->bits )==OSA_EFAIL ){
		OSA_ERROR("can't set bits per word[%s]\n",pParam->dev );
		goto _exitDevIoctlFail;
	}

	if (  ioctl(hndl->fd, SPI_IOC_RD_BITS_PER_WORD, &pParam->bits)==OSA_EFAIL ){
		OSA_ERROR("can't get bits per word[%s]\n",pParam->dev );
		goto _exitDevIoctlFail;
	}



	/*
	* max speed hz
	*/
	if ( ioctl(hndl->fd, SPI_IOC_WR_MAX_SPEED_HZ, &pParam->speed)==OSA_EFAIL){
		OSA_ERROR("can't set max speed hz[%s]\n",pParam->dev );
		goto _exitDevIoctlFail;
	}


	if ( ioctl(hndl->fd, SPI_IOC_RD_MAX_SPEED_HZ, &pParam->speed)== OSA_EFAIL){
		OSA_ERROR("can't get max speed hz[%s]\n",pParam->dev);
		goto _exitDevIoctlFail;
	}

	if ( ioctl(hndl->fd, SPI_IOC_WR_LSB_FIRST, &pParam->byteOrder)== OSA_EFAIL){
		OSA_ERROR("can't Set Byte Order[%s]\n",pParam->dev);
		goto _exitDevIoctlFail;
	}

#if 1
	OSA_printf("spi mode: %d\n", pParam->mode);
	OSA_printf("bits per word: %d\n", pParam->bits);
	OSA_printf("max speed: %d Hz (%d KHz)\n", pParam->speed, pParam->speed/1000);
#endif

	memcpy((DRV_SPICreatParam*)&hndl->cParam, pParam, sizeof(DRV_SPICreatParam));
	return hndl;


_exitDevIoctlFail:
	OSA_mutexDelete(&hndl->spiBusLock);
	if(hndl->fd>0)
	close(hndl->fd);
	OSA_memFree(hndl);
	hndl = NULL;

_exitInit:
	return hndl;
}


/**
 * @name	DRV_spiRead8()
 * @brief	This function read(8 byte) from spi device
 * @ingroup Group_DrvUser
 *
 * This API provides certain actions as an example.
 *
 * @param [hndl] device handle
 * @param [buf] input buffer
 * @param [count] byte count
 * @param [Obuf] return buffer
 *
 * @retval	n n is read conut
 * @retval -1 Failure
 *
 * Example Usage:
 * @code
 *	  <Reserved>
 * @endcode
 */
int DRV_spiRead8(DRV_SpiHndl *hndl, Uint8 *buf,  Uint32 count, Uint8 *Obuf)
{
	int ret=OSA_EFAIL;
	struct spi_ioc_transfer tr;

	if ( hndl==NULL || buf==NULL || count<=0 || Obuf==NULL){
		OSA_ERROR("Invalid input parameters\n");
		return OSA_EFAIL;
	}

	if( hndl->fd<0 )
	{
		OSA_ERROR("Invalid Device[ID:%d] Desciptor Or, Initialization Filed...\n",hndl->fd);
		return OSA_EFAIL;
	}

	OSA_mutexLock(&hndl->spiBusLock);
	tr.tx_buf = (unsigned long)buf;
	tr.rx_buf = (unsigned long)Obuf;
	tr.len = count;
	tr.delay_usecs = hndl->cParam.delay;
	tr.speed_hz = hndl->cParam.speed;
	tr.bits_per_word = hndl->cParam.bits;

	ret = ioctl(hndl->fd, SPI_IOC_MESSAGE(1), &tr);
	if (ret==1){
		OSA_ERROR("can't send spi message(Read 8bit)\n");
	}
	else
	{
		ret=OSA_SOK;
	}
	OSA_mutexUnlock(&hndl->spiBusLock);

	OSA_waitMsecs(1);

	return ret;
}



/**
 * @name	DRV_spiWrite8()
 * @brief	This function write(8 byte) to spi device
 * @ingroup Group_DrvUser
 *
 * This API provides certain actions as an example.
 *
 * @param [hndl] device handle
 * @param [buf] write buffer
 * @param [count] byte count
 *
 * @retval	n n is written conut
 * @retval -1 Failure
 *
 * Example Usage:
 * @code
 *	  <Reserved>
 * @endcode
 */
int DRV_spiWrite8(DRV_SpiHndl *hndl, Uint8 *buf, Uint32 count)
{
	int ret=OSA_EFAIL;
	struct spi_ioc_transfer tr;

	if ( hndl==NULL || buf==NULL || count<=0 ){
		OSA_ERROR("Invalid input parameters\n");
		return OSA_EFAIL;
	}

	if( hndl->fd<0 )
	{
		OSA_ERROR("Invalid Device[ID:%d] Desciptor Or, Initialization Filed...\n",hndl->fd);
		return OSA_EFAIL;
	}

	OSA_mutexLock(&hndl->spiBusLock);
	tr.tx_buf = (unsigned long)buf;
	tr.rx_buf = 0;
	tr.len = count;
	tr.delay_usecs = hndl->cParam.delay;
	tr.speed_hz = hndl->cParam.speed;
	tr.bits_per_word = hndl->cParam.bits;


	ret = ioctl(hndl->fd, SPI_IOC_MESSAGE(1), &tr);
	if (ret==1){
		OSA_ERROR("can't send spi message(Write 8bit)\n");
	}
	else
	{
		ret=OSA_SOK;
	}
	OSA_mutexUnlock(&hndl->spiBusLock);

	OSA_waitMsecs(1);

	return ret;
}



/**
 * @name	DRV_spiRead16()
 * @brief	This function read(16 byte) from spi device
 * @ingroup Group_DrvUser
 *
 * This API provides certain actions as an example.
 *
 * @param [hndl] device handle
 * @param [buf] input buffer
 * @param [count] byte count
 * @param [Obuf] return buffer
 *
 * @retval	n n is read conut
 * @retval -1 Failure
 *
 * Example Usage:
 * @code
 *	  <Reserved>
 * @endcode
 */
int DRV_spiRead16(DRV_SpiHndl *hndl, Uint16 *buf,  Uint32 count, Uint8 *Obuf)
{
	int ret=OSA_EFAIL;
	struct spi_ioc_transfer tr;

	if ( hndl==NULL || buf==NULL || count<=0 || Obuf==NULL){
		OSA_ERROR("Invalid input parameters\n");
		return OSA_EFAIL;
	}

	if( hndl->fd<0 )
	{
		OSA_ERROR("Invalid Device[ID:%d] Desciptor Or, Initialization Filed...\n",hndl->fd);
		return OSA_EFAIL;
	}


	OSA_mutexLock(&hndl->spiBusLock);
	tr.tx_buf = (unsigned long)buf;
	tr.rx_buf = (unsigned long)Obuf;
	tr.len = count;
	tr.delay_usecs = hndl->cParam.delay;
	tr.speed_hz = hndl->cParam.speed;
	tr.bits_per_word = hndl->cParam.bits;

	ret = ioctl(hndl->fd, SPI_IOC_MESSAGE(1), &tr);
	if (ret==1){
		OSA_ERROR("can't send spi message(Read 16)\n");
	}
	else
	{
		ret=OSA_SOK;
	}
	OSA_mutexUnlock(&hndl->spiBusLock);

	return ret;
}


/**
 * @name	DRV_spiWrite8()
 * @brief	This function write(16 byte) to spi device
 * @ingroup Group_DrvUser
 *
 * This API provides certain actions as an example.
 *
 * @param [hndl] device handle
 * @param [buf] write buffer
 * @param [count] byte count
 *
 * @retval	n n is written conut
 * @retval -1 Failure
 *
 * Example Usage:
 * @code
 *	  <Reserved>
 * @endcode
 */
int DRV_spiWrite16(DRV_SpiHndl *hndl, Uint16 *buf, Uint32 count)
{
	int ret=OSA_EFAIL;
	struct spi_ioc_transfer tr;

	if ( hndl==NULL || buf==NULL || count<=0 ){
		OSA_ERROR("Invalid input parameters\n");
		return OSA_EFAIL;
	}

	if( hndl->fd<0 )
	{
		OSA_ERROR("Invalid Device[ID:%d] Desciptor Or, Initialization Filed...\n",hndl->fd);
		return OSA_EFAIL;
	}

	OSA_mutexLock(&hndl->spiBusLock);
	tr.tx_buf = (unsigned long)buf;
	tr.rx_buf = 0;
	tr.len = count;
	tr.delay_usecs = hndl->cParam.delay;
	tr.speed_hz = hndl->cParam.speed;
	tr.bits_per_word = hndl->cParam.bits;

	ret = ioctl(hndl->fd, SPI_IOC_MESSAGE(1), &tr);
	if (ret==1){
		OSA_ERROR("can't send spi message");
	}else{
		ret=OSA_SOK;
	}
	OSA_mutexUnlock(&hndl->spiBusLock);

	return ret;
}



/**
 * @name	DRV_spiDeinit()
 * @brief	This function de-initialize spi device
 * @ingroup Group_DrvUser
 *
 * This API provides certain actions as an example.
 *
 * @param [devType] device type
 *
 * @retval	0 Success
 *
 * Example Usage:
 * @code
 *	  <Reserved>
 * @endcode
 */
int DRV_spiDeinit(DRV_SpiHndl *hndl)
{
	if(hndl)
	{
		if(hndl->fd){
			close(hndl->fd);
		}

		if(hndl->isExternalLock == FALSE){
			OSA_mutexDelete(&hndl->spiBusLock);
		}

		OSA_memFree(hndl);
	}
	return OSA_SOK;
}





# (c) mBrain

include $(DEMO_BUILD_DIR)/COMMON_HEADER.MK

libs:
	make -fMAKEFILE.MK -C./spi MODULE=drv $(MAKE_TARGET)
	make -fMAKEFILE.MK -C./gpio MODULE=drv $(MAKE_TARGET)

all:
	make -fMAKEFILE.MK depend
	make -fMAKEFILE.MK clean
	make -fMAKEFILE.MK libs

clean:
	make -fMAKEFILE.MK libs MAKE_TARGET=clean

depend:
	make -fMAKEFILE.MK libs MAKE_TARGET=depend

install:
	make -fMAKEFILE.MK libs MAKE_TARGET=install

.PHONY : install clean depend all libs


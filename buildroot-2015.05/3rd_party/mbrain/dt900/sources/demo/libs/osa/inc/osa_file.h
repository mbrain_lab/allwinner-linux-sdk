/**
 * @file		osa_file.h
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date	2015.07.12
 * @brief	File helper APIs
 *
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain
 * 
 * (c) Copyright mBrain(2015).	All Rights Reserved.
 */
/*! [ 1]. File classification */
#ifndef _OSA_FILE_H_
#define _OSA_FILE_H_

/*! [ 2]. File Directive (#include) */
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <osa.h>

/*! [ 3]. Global macro and enum */
/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
/*! [ 5]. Global Function prototype declaration */
int OSA_fileReadFile(char *fileName, Uint8 *addr, Uint32 readSize, Uint32 *actualReadSize);
int OSA_fileWriteFile(char *fileName, Uint8 *addr, Uint32 size);
int OSA_fileWriteVideoData(char *fileName, Uint8 *Yaddr, Uint8 *UVaddr, Uint32 width, Uint32 height, Uint32 pitch);
int OSA_CheckDirectory(char *dirName);
int OSA_CheckFile(char *fileName);

/*! [ 6]. Global Variable declaration  */

#endif /* _OSA_FILE_H_ */




/**
 * @file		  osa_tsk.h
 * @Author 	      Joel,Kim (Joel.Kim@mbrain.org)
 * @date		  2015.07.12
 * @brief		  Task helper APIs
 * 
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain
 * 
 * (c) Copyright mBrain(2015).		  All Rights Reserved.
 */

/*! [ 1]. File classification */
#ifndef _OSA_TSK_H_
#define _OSA_TSK_H_

/*! [ 2]. File Directive (#include) */
#include <osa_thr.h>
#include <osa_mbx.h>
#include <osa_buf.h>


/*! [ 3]. Global macro and enum */
#define TASK_DEFAULT_LOOPDELAY	1000	/* 1000 usec */

#define TASK_NAME_STR_MAX	128

/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
struct OSA_TskHndl;

typedef int (*OSA_TskFncMain)(struct OSA_TskHndl *pTsk, OSA_MsgHndl *pMsg, Uint32 curState );

/*
 * @brief Task Handle
 */
typedef struct OSA_TskHndl {
	Bool taskValid;                 ///< flag for validation
	OSA_MbxHndl mbxHndl;            ///< Mailbox handle
	OSA_ThrHndl thrHndl;            ///< OS thread handle

	Uint32 curState;                ///< Task state as defined by user
	OSA_TskFncMain fncMain;         ///< Task Main, this function is entered when a message is received by the process
	void* pParamCfg;                ///< Task config parameter
	OSA_BufHndl *buffer[10];        ///< Buffer
	Uint32 nLoopDelay;				///< Task loop delay
	char szName[TASK_NAME_STR_MAX];

} OSA_TskHndl;


/*! [ 5]. Global Function prototype declaration */
int OSA_tskCreate(OSA_TskHndl *pPrc, OSA_TskFncMain fncMain, Uint32 tskPri, Uint32 tskStackSize, Uint32 initState, Uint32 nLoopDelay, const char* pSzName);
int OSA_tskDelete(OSA_TskHndl *pTsk);
int OSA_tskSendMsg(OSA_TskHndl *pTskTo, OSA_TskHndl *pTskFrom, Uint16 cmd, void *pPrm, Uint16 flags);
int OSA_tskBroadcastMsg(OSA_TskHndl *pTskToList[], OSA_TskHndl *pTskFrom, Uint16 cmd, void *pPrm, Uint16 flags);
int OSA_tskAckOrFreeMsg(OSA_MsgHndl *pMsg, int ackRetVal);
int OSA_tskWaitMsg(OSA_TskHndl *pTsk, OSA_MsgHndl **pMsg);
int OSA_tskCheckMsg(OSA_TskHndl *pTsk, OSA_MsgHndl **pMsg);
int OSA_tskWaitCmd(OSA_TskHndl *pTsk, OSA_MsgHndl **pMsg, Uint16 waitCmd);
int OSA_tskFlushMsg(OSA_TskHndl *pTsk);

int OSA_tskSetState(OSA_TskHndl *pPrc, Uint32 curState);
Uint32 OSA_tskGetState(OSA_TskHndl *pPrc);


/*! [ 6]. Global Variable declaration  */

#endif /* _OSA_TSK_H_ */


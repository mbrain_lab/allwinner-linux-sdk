/**
 * @file		osa_buf.h
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date	2015.07.12
 * @brief	Buffer helper APIs
 *
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain
 * 
 * (c) Copyright mBrain(2015).	All Rights Reserved.
 */

/*! [ 1]. File classification */
#ifndef _OSA_BUF_H_
#define _OSA_BUF_H_

/*! [ 2]. File Directive (#include) */
#include <osa_que.h>
#include <osa_mutex.h>


/*! [ 3]. Global macro and enum */

#define OSA_BUF_NUM_MAX			(8*4)
#define OSA_BUF_ID_INVALID		(-1)
#define OSA_BUF_CHUNK_CNT		(3)


/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
typedef struct {

	int 		size;
	int 		count;
	unsigned long physAddr;
	void		*virtAddr;
	int 		flags;
	unsigned long long	timestamp;
	int 		width;
	int 		height;
	Uint32	isKeyFrame;
	Uint16	codecType;
	int         pixFmt;
	

	/* Chunk information */
	void		*addrPhyY;
	void		*addrVirY;
	void		*addrPhyC;
	void		*addrVirC;

	int			isPreviewAvailable;
	void		*PrevAddrPhyY;
	void		*PrevAddrVirY;
	void		*PrevAddrPhyC;
	void		*PrevAddrVirC;
	
	int			PrevWidth;
	int			PrevHeight;
} OSA_BufInfo;

typedef struct {

	OSA_BufInfo bufInfo[OSA_BUF_NUM_MAX];

	OSA_QueHndl emptyQue;
	OSA_QueHndl fullQue;

	int numBuf;

} OSA_BufHndl;

typedef struct {

	unsigned long bufPhysAddr[OSA_BUF_NUM_MAX];
	void *bufVirtAddr[OSA_BUF_NUM_MAX];
	Uint32 nBufLen[OSA_BUF_NUM_MAX];

	int numBuf;

} OSA_BufCreate;


/*! [ 5]. Global Function prototype declaration */
int  OSA_bufCreate(OSA_BufHndl *hndl, OSA_BufCreate *bufInit);
int  OSA_bufDelete(OSA_BufHndl *hndl);

int  OSA_bufGetFull(OSA_BufHndl *hndl, int *bufId, Uint32 timeout);
int  OSA_bufPutEmpty(OSA_BufHndl *hndl, int bufId);

int  OSA_bufGetEmpty(OSA_BufHndl *hndl, int *bufId, Uint32 timeout);
int  OSA_bufPutFull(OSA_BufHndl *hndl, int bufId);

int  OSA_bufSwitchFull (OSA_BufHndl *hndl, int *bufId);
int  OSA_bufSwitchEmpty(OSA_BufHndl *hndl, int *bufId);

OSA_BufInfo *OSA_bufGetBufInfo(OSA_BufHndl *hndl, int bufId);


/*! [ 6]. Global Variable declaration  */


#endif /* _OSA_BUF_H_ */




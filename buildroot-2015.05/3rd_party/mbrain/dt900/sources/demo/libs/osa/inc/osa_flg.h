/**
 * @file		osa_flg.h
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date	 2015.07.12
 * @brief	 Flag helper APIs
 *
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain
 * 
 * (c) Copyright mBrain(2015).	 All Rights Reserved.
 */

/*! [ 1]. File classification */
#ifndef _OSA_FLG_H_
#define _OSA_FLG_H_

/*! [ 2]. File Directive (#include) */
#include <osa.h>

/*! [ 3]. Global macro and enum */
#define OSA_FLG_MODE_AND	 0x1
#define OSA_FLG_MODE_OR 	 0x2
#define OSA_FLG_MODE_CLR	 0x4

#define OSA_FLG_MODE_AND_CLR	 (OSA_FLG_MODE_AND|OSA_FLG_MODE_CLR)
#define OSA_FLG_MODE_OR_CLR 	 (OSA_FLG_MODE_OR |OSA_FLG_MODE_CLR)


/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
typedef struct {

	Uint32 pattern;
	pthread_mutex_t lock;
	pthread_cond_t	cond;

} OSA_FlgHndl;


/*! [ 5]. Global Function prototype declaration */
int  OSA_flgCreate(OSA_FlgHndl *hndl, Uint32 initPattern);
int  OSA_flgWait(OSA_FlgHndl *hndl, Uint32 pattern, Uint32 mode, Uint32 timeout);
int  OSA_flgSet(OSA_FlgHndl *hndl, Uint32 pattern);
int  OSA_flgClear(OSA_FlgHndl *hndl, Uint32 pattern);
Bool OSA_flgIsSet(OSA_FlgHndl *hndl, Uint32 pattern);
int  OSA_flgDelete(OSA_FlgHndl *hndl);


/*! [ 6]. Global Variable declaration  */

#endif /* _OSA_FLG_H_ */


/**
 * @file		 osa_que.h
 * @Author 	     Joel,Kim (Joel.Kim@mbrain.org)
 * @date		 2015.07.12
 * @brief		 Queue helper APIs
 * 
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain
 * 
 * (c) Copyright mBrain(2015).		 All Rights Reserved.
 */

/*! [ 1]. File classification */

#ifndef _OSA_QUE_H_
#define _OSA_QUE_H_

/*! [ 2]. File Directive (#include) */
#include <osa.h>


/*! [ 3]. Global macro and enum */


/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
typedef struct {

	Uint32 curRd;
	Uint32 curWr;
	Uint32 len;
	Uint32 count;

	Int32 *queue;

	pthread_mutex_t lock;
	pthread_cond_t	condRd;
	pthread_cond_t	condWr;

} OSA_QueHndl;


/*! [ 5]. Global Function prototype declaration */
int OSA_queCreate(OSA_QueHndl *hndl, Uint32 maxLen);
int OSA_queDelete(OSA_QueHndl *hndl);
int OSA_quePut(OSA_QueHndl *hndl, Int32  value, Uint32 timeout);
int OSA_queGet(OSA_QueHndl *hndl, Int32 *value, Uint32 timeout);


/*! [ 6]. Global Variable declaration  */

#endif /* _OSA_QUE_H_ */


#include <ctype.h>
#include "osa_ini.h"
#define ASCIILINESZ 				(1024)
#define INI_INVALID_KEY 		((char*)-1)

/**
 * This enum stores the status for each parsed line (internal use only).
 */
typedef enum _line_status_ {
	LINE_UNPROCESSED,
	LINE_ERROR,
	LINE_EMPTY,
	LINE_COMMENT,
	LINE_SECTION,
	LINE_VALUE
} line_status ;


/*
 ******************************************************************************
 SOURCE CODE - strlwc() routine BEGINS HERE
 ******************************************************************************
 // Function Name: strlwc()
 // Argument:
	a) Input: 	s	String to convert.
	b) Output:	ptr to statically allocated string.
 // Description  :
	Convert a string to lowercase.

	This function returns a pointer to a statically allocated string
	containing a lowercased version of the input string. Do not free
	or modify the returned string! Since the returned string is statically
	allocated, it will be modified at each function call (not re-entrant).

 // FYI:
 *****************************************************************************
*/
static char * strlwc(const char * s)
{
	static char l[ASCIILINESZ+1];
	int i ;

	if (s==NULL) return NULL ;
	memset(l, 0, ASCIILINESZ+1);
	i=0 ;
	while (s[i] && i<ASCIILINESZ) {
		l[i] = (char)tolower((int)s[i]);
		i++ ;
	}
	l[ASCIILINESZ]=(char)0;
	return l ;
}



/*
 ******************************************************************************
 SOURCE CODE - strstrip() routine BEGINS HERE
 ******************************************************************************
 // Function Name: strstrip()
 // Argument:
	a) Input: 	s	String to parse.
	b) Output:	ptr to statically allocated string.
 // Description  :
	Remove blanks at the beginning and the end of a string.

	This function returns a pointer to a statically allocated string,
	which is identical to the input string, except that all blank
	characters at the end and the beg. of the string have been removed.
	Do not free or modify the returned string! Since the returned string
	is statically allocated, it will be modified at each function call
	(not re-entrant).

 // FYI:
 *****************************************************************************
*/
static char * strstrip(char * s)
{
	static char l[ASCIILINESZ+1];
	char * last ;

	if (s==NULL) return NULL ;

	while (isspace((int)*s) && *s) s++;
	memset(l, 0, ASCIILINESZ+1);
	strcpy(l, s);
	last = l + strlen(l);
	while (last > l) {
		if (!isspace((int)*(last-1)))
			break ;
		last -- ;
	}
	*last = (char)0;
	return (char*)l ;
}


/*
 ******************************************************************************
 SOURCE CODE - OSA_iniPsr_getnsec() routine BEGINS HERE
 ******************************************************************************
 // Function Name: OSA_iniPsr_getnsec()
 // Argument:
	a) Input: 	d 	Dictionary to examine
	b) Output:	int Number of sections found in OSA_iniDic
 // Description  :
	Get number of sections in a OSA_iniDic

	This function returns the number of sections found in a OSA_iniDic.
	The test to recognize sections is done on the string stored in the
	OSA_iniDic: a section name is given as "section" whereas a key is
	stored as "section:key", thus the test looks for entries that do not
	contain a colon.

	This clearly fails in the case a section name contains a colon, but
	this should simply be avoided.

	This function returns -1 in case of error.

 // FYI:
 *****************************************************************************
*/
int OSA_iniPsr_getnsec(OSA_iniDic * d)
{
	int i ;
	int nsec ;

	if (d==NULL) return -1 ;
	nsec=0 ;
	for (i=0 ; i<d->size ; i++) {
		if (d->key[i]==NULL)
			continue ;
		if (strchr(d->key[i], ':')==NULL) {
			nsec ++ ;
		}
	}
	return nsec ;
}


/*
 ******************************************************************************
 SOURCE CODE - OSA_iniPsr_getsecname() routine BEGINS HERE
 ******************************************************************************
 // Function Name: OSA_iniPsr_getsecname()
 // Argument:
	a) Input: 	d 	Dictionary to examine
							n 	Section number (from 0 to nsec-1).
	b) Output:	Pointer to char string
 // Description  :
	Get name for section n in a OSA_iniDic.

	This function locates the n-th section in a OSA_iniDic and returns
	its name as a pointer to a string statically allocated inside the
	OSA_iniDic. Do not free or modify the returned string!

	This function returns NULL in case of error.

 // FYI:
 *****************************************************************************
*/
char * OSA_iniPsr_getsecname(OSA_iniDic * d, int n)
{
	int i ;
	int foundsec ;

	if (d==NULL || n<0) return NULL ;
	foundsec=0 ;
	for (i=0 ; i<d->size ; i++) {
		if (d->key[i]==NULL)
			continue ;
		if (strchr(d->key[i], ':')==NULL) {
			foundsec++ ;
			if (foundsec>n)
				break ;
		}
	}
	if (foundsec<=n) {
		return NULL ;
	}
	return d->key[i] ;
}


/*
 ******************************************************************************
 SOURCE CODE - OSA_iniPsr_dump() routine BEGINS HERE
 ******************************************************************************
 // Function Name: OSA_iniPsr_dump()
 // Argument:
	a) Input: 	d 	Dictionary to dump.
							f 	Opened file pointer to dump to.
	b) Output:	void
 // Description  :
	Dump a OSA_iniDic to an opened file pointer.

	This function prints out the contents of a OSA_iniDic, one element by
	line, onto the provided file pointer. It is OK to specify @c stderr
	or @c stdout as output files. This function is meant for debugging
	purposes mostly.

 // FYI:
 *****************************************************************************
*/
void OSA_iniPsr_dump(OSA_iniDic * d, FILE * f)
{
	int 		i ;

	if (d==NULL || f==NULL) return ;
	for (i=0 ; i<d->size ; i++) {
		if (d->key[i]==NULL)
			continue ;
		if (d->val[i]!=NULL) {
			fprintf(f, "[%s]=[%s]\n", d->key[i], d->val[i]);
		} else {
			fprintf(f, "[%s]=UNDEF\n", d->key[i]);
		}
	}
	return ;
}


/*
 ******************************************************************************
 SOURCE CODE - OSA_iniPsr_dump_ini() routine BEGINS HERE
 ******************************************************************************
 // Function Name: OSA_iniPsr_dump_ini()
 // Argument:
	a) Input: 	d 	Dictionary to dump
							f 	Opened file pointer to dump to
	b) Output:	void
 // Description  :
	Save a OSA_iniDic to a loadable ini file

	This function dumps a given OSA_iniDic into a loadable ini file.
	It is Ok to specify @c stderr or @c stdout as output files.

 // FYI:
 *****************************************************************************
*/
void OSA_iniPsr_dump_ini(OSA_iniDic * d, FILE * f)
{
	int 		i, j ;
	char		keym[ASCIILINESZ+1];
	int 		nsec ;
	char *	secname ;
	int 		seclen ;

	if (d==NULL || f==NULL) return ;

	nsec = OSA_iniPsr_getnsec(d);
	if (nsec<1) {
		/* No section in file: dump all keys as they are */
		for (i=0 ; i<d->size ; i++) {
			if (d->key[i]==NULL)
				continue ;
			fprintf(f, "%s = %s\n", d->key[i], d->val[i]);
		}
		return ;
	}
	for (i=0 ; i<nsec ; i++) {
		secname = OSA_iniPsr_getsecname(d, i) ;
		seclen	= (int)strlen(secname);
		fprintf(f, "\n[%s]\n", secname);
		sprintf(keym, "%s:", secname);
		for (j=0 ; j<d->size ; j++) {
			if (d->key[j]==NULL)
				continue ;
			if (!strncmp(d->key[j], keym, seclen+1)) {
				fprintf(f,
						"%-30s = %s\n",
						d->key[j]+seclen+1,
						d->val[j] ? d->val[j] : "");
			}
		}
	}
	fprintf(f, "\n");
	return ;
}


/*
 ******************************************************************************
 SOURCE CODE - OSA_iniPsr_getstring() routine BEGINS HERE
 ******************************************************************************
 // Function Name: OSA_iniPsr_getstring()
 // Argument:
	a) Input: 	Get the string associated to a key
				key 		Key string to look for
				def 		Default value to return if key not found.
	b) Output:	pointer to statically allocated character string
 // Description  :
	Get the string associated to a key

	This function queries a OSA_iniDic for a key. A key as read from an
	ini file is given as "section:key". If the key cannot be found,
	the pointer passed as 'def' is returned.
	The returned char pointer is pointing to a string allocated in
	the OSA_iniDic, do not free or modify it.

 // FYI:
 *****************************************************************************
*/
char * OSA_iniPsr_getstring(OSA_iniDic * d, const char * key, char * def)
{
	char * lc_key ;
	char * sval ;

	if (d==NULL || key==NULL)
		return def ;

	lc_key = strlwc(key);
	sval = dictionary_get(d, lc_key, def);
	return sval ;
}


/*
 ******************************************************************************
 SOURCE CODE - OSA_iniPsr_getint() routine BEGINS HERE
 ******************************************************************************
 // Function Name: OSA_iniPsr_getint()
 // Argument:
	a) Input: 	d Dictionary to search
				key Key string to look for
				notfound Value to return in case of error
	b) Output:	integer
 // Description  :
	Get the string associated to a key, convert to an int

	This function queries a OSA_iniDic for a key. A key as read from an
	ini file is given as "section:key". If the key cannot be found,
	the notfound value is returned.

	Supported values for integers include the usual C notation
	so decimal, octal (starting with 0) and hexadecimal (starting with 0x)
	are supported. Examples:

	"42"			->	42
	"042" 		->	34 (octal -> decimal)
	"0x42"		->	66 (hexa	-> decimal)

	Warning: the conversion may overflow in various ways. Conversion is
	totally outsourced to strtol(), see the associated man page for overflow
	handling.

 // FYI:
 *****************************************************************************
*/
int OSA_iniPsr_getint(OSA_iniDic * d, const char * key, int notfound)
{
	char* str ;

	str = OSA_iniPsr_getstring(d, key, INI_INVALID_KEY);
	if (str==INI_INVALID_KEY) return notfound ;
	return (int)strtol(str, NULL, 0);
}


/*
 ******************************************************************************
 SOURCE CODE - OSA_iniPsr_getdouble() routine BEGINS HERE
 ******************************************************************************
 // Function Name: OSA_iniPsr_getdouble()
 // Argument:
	a) Input: 	d Dictionary to search
							key Key string to look for
							notfound Value to return in case of error
	b) Output:	double
 // Description  :
	Get the string associated to a key, convert to a double

	This function queries a OSA_iniDic for a key. A key as read from an
	ini file is given as "section:key". If the key cannot be found,
	the notfound value is returned.

 // FYI:
 *****************************************************************************
*/
double OSA_iniPsr_getdouble(OSA_iniDic * d, char * key, double notfound)
{
	char* str ;

	str = OSA_iniPsr_getstring(d, key, INI_INVALID_KEY);
	if (str==INI_INVALID_KEY) return notfound ;
	return atof(str);
}


/*
 ******************************************************************************
 SOURCE CODE - OSA_iniPsr_getboolean() routine BEGINS HERE
 ******************************************************************************
 // Function Name: OSA_iniPsr_getboolean()
 // Argument:
	a) Input: 	d Dictionary to search
							key Key string to look for
							notfound Value to return in case of error
	b) Output:	integer
 // Description  :
	Get the string associated to a key, convert to a boolean

	This function queries a OSA_iniDic for a key. A key as read from an
	ini file is given as "section:key". If the key cannot be found,
	the notfound value is returned.

	A true boolean is found if one of the following is matched:

	- A string starting with 'y'
	- A string starting with 'Y'
	- A string starting with 't'
	- A string starting with 'T'
	- A string starting with '1'

	A false boolean is found if one of the following is matched:

	- A string starting with 'n'
	- A string starting with 'N'
	- A string starting with 'f'
	- A string starting with 'F'
	- A string starting with '0'

	The notfound value returned if no boolean is identified, does not
	necessarily have to be 0 or 1.

 // FYI:
 *****************************************************************************
*/
int OSA_iniPsr_getboolean(OSA_iniDic * d, const char * key, int notfound)
{
	char* c;
	int ret ;

	c = OSA_iniPsr_getstring(d, key, INI_INVALID_KEY);
	if (c==INI_INVALID_KEY) return notfound ;
	if (c[0]=='y' || c[0]=='Y' || c[0]=='1' || c[0]=='t' || c[0]=='T') {
		ret = 1 ;
	} else if (c[0]=='n' || c[0]=='N' || c[0]=='0' || c[0]=='f' || c[0]=='F') {
		ret = 0 ;
	} else {
		ret = notfound ;
	}
	return ret;
}


/*
 ******************************************************************************
 SOURCE CODE - OSA_iniPsr_find_entry() routine BEGINS HERE
 ******************************************************************************
 // Function Name: OSA_iniPsr_find_entry()
 // Argument:
	a) Input: 	ini 		Dictionary to search
							entry 	Name of the entry to look for
	b) Output:	integer 1 if entry exists, 0 otherwise
 // Description  :
	Finds out if a given entry exists in a OSA_iniDic

	Finds out if a given entry exists in the OSA_iniDic. Since sections
	are stored as keys with NULL associated values, this is the only way
	of querying for the presence of sections in a OSA_iniDic.

 // FYI:
 *****************************************************************************
*/
int OSA_iniPsr_find_entry(OSA_iniDic	*ini, char *entry)
{
	int found=0 ;
	if (OSA_iniPsr_getstring(ini, entry, INI_INVALID_KEY)!=INI_INVALID_KEY) {
		found = 1 ;
	}
	return found ;
}


/*
 ******************************************************************************
 SOURCE CODE - OSA_iniPsr_set() routine BEGINS HERE
 ******************************************************************************
 // Function Name: OSA_iniPsr_set()
 // Argument:
	a) Input: 	ini 		Dictionary to modify.
							entry 	Entry to modify (entry name)
							val 		New value to associate to the entry.
	b) Output:	int 0 if Ok, -1 otherwise.
 // Description  :
	Set an entry in a OSA_iniDic.

	If the given entry can be found in the OSA_iniDic, it is modified to
	contain the provided value. If it cannot be found, -1 is returned.
	It is Ok to set val to NULL.

 // FYI:
 *****************************************************************************
*/
int OSA_iniPsr_set(OSA_iniDic * ini, char * entry, char * val)
{
		return dictionary_set(ini, strlwc(entry), val) ;
}


/*
 ******************************************************************************
 SOURCE CODE - OSA_iniPsr_unset() routine BEGINS HERE
 ******************************************************************************
 // Function Name: OSA_iniPsr_unset()
 // Argument:
	a) Input: 	ini 		Dictionary to modify.
							entry 	Entry to delete (entry name)
							val 		New value to associate to the entry.
	b) Output:	void
 // Description  :
	Delete an entry in a OSA_iniDic

	If the given entry can be found, it is deleted from the OSA_iniDic.

 // FYI:
 *****************************************************************************
*/
void OSA_iniPsr_unset(OSA_iniDic * ini, char * entry)
{
	dictionary_unset(ini, strlwc(entry));
}


/*
 ******************************************************************************
 SOURCE CODE - OSA_iniPsr_line() routine BEGINS HERE
 ******************************************************************************
 // Function Name: OSA_iniPsr_line()
 // Argument:
	a) Input: 	input_line	Input line, may be concatenated multi-line input
							section 		Output space to store section
							key 				Output space to store key
							value 			Output space to store value
	b) Output:	line_status value
 // Description  :
	Load a single line from an INI file

	If the given entry can be found, it is deleted from the OSA_iniDic.

 // FYI:
 *****************************************************************************
*/
static line_status OSA_iniPsr_line(
		char * input_line,
		char * section,
		char * key,
		char * value)
{
	line_status sta ;
	char				line[ASCIILINESZ+1];
	int 				len ;

	strcpy(line, strstrip(input_line));
	len = (int)strlen(line);

	sta = LINE_UNPROCESSED ;
	if (len<1) {
		/* Empty line */
		sta = LINE_EMPTY ;
	} else if (line[0]=='#') {
		/* Comment line */
		sta = LINE_COMMENT ;
	} else if (line[0]=='[' && line[len-1]==']') {
		/* Section name */
		sscanf(line, "[%[^]]", section);
		strcpy(section, strstrip(section));
		strcpy(section, strlwc(section));
		sta = LINE_SECTION ;
	} else if (sscanf (line, "%[^=] = \"%[^\"]\"", key, value) == 2
				 ||  sscanf (line, "%[^=] = '%[^\']'",	 key, value) == 2
				 ||  sscanf (line, "%[^=] = %[^;#]",		 key, value) == 2) {
		/* Usual key=value, with or without comments */
		strcpy(key, strstrip(key));
		strcpy(key, strlwc(key));
		strcpy(value, strstrip(value));
		/*
		 * sscanf cannot handle '' or "" as empty values
		 * this is done here
		 */
		if (!strcmp(value, "\"\"") || (!strcmp(value, "''"))) {
			value[0]=0 ;
		}
		sta = LINE_VALUE ;
	} else if (sscanf(line, "%[^=] = %[;#]", key, value)==2
				 ||  sscanf(line, "%[^=] %[=]", key, value) == 2) {
		/*
		 * Special cases:
		 * key=
		 * key=;
		 * key=#
		 */
		strcpy(key, strstrip(key));
		strcpy(key, strlwc(key));
		value[0]=0 ;
		sta = LINE_VALUE ;
	} else {
		/* Generate syntax error */
		sta = LINE_ERROR ;
	}
	return sta ;
}


/*
 ******************************************************************************
 SOURCE CODE - OSA_iniPsr_load() routine BEGINS HERE
 ******************************************************************************
 // Function Name: OSA_iniPsr_load()
 // Argument:
	a) Input: 	ininame Name of the ini file to read.
	b) Output:	Pointer to newly allocated OSA_iniDic
 // Description  :
	Parse an ini file and return an allocated OSA_iniDic object

	his is the parser for ini files. This function is called, providing
	the name of the file to be read. It returns a OSA_iniDic object that
	should not be accessed directly, but through accessor functions
	instead.

	The returned OSA_iniDic must be freed using OSA_iniPsr_freedict().

 // FYI:
 *****************************************************************************
*/
OSA_iniDic * OSA_iniPsr_load(const char * ininame)
{
	FILE * in ;

	char line 	 [ASCIILINESZ+1] ;
	char section [ASCIILINESZ+1] ;
	char key		 [ASCIILINESZ+1] ;
	char tmp		 [ASCIILINESZ+1] ;
	char val		 [ASCIILINESZ+1] ;

	int  last=0 ;
	int  len ;
	int  lineno=0 ;
	int  errs=0;

	OSA_iniDic * dict ;

	if ((in=fopen(ininame, "r"))==NULL) {
		OSA_ERROR("OSA_iniPsr: cannot open %s\n", ininame);
		return NULL ;
	}

	dict = dictionary_new(0) ;
	if (!dict) {
		fclose(in);
		return NULL ;
	}

	memset(line,		0, ASCIILINESZ);
	memset(section, 0, ASCIILINESZ);
	memset(key, 		0, ASCIILINESZ);
	memset(val, 		0, ASCIILINESZ);
	last=0 ;

	while (fgets(line+last, ASCIILINESZ-last, in)!=NULL) {
		lineno++ ;
		len = (int)strlen(line)-1;
		/* Safety check against buffer overflows */
		if (line[len]!='\n') {
			OSA_ERROR("OSA_iniPsr: input line too long in %s (%d)\n",ininame,lineno);
			dictionary_del(dict);
			fclose(in);
			return NULL ;
		}
		/* Get rid of \n and spaces at end of line */
		while ((len>=0) &&
				((line[len]=='\n') || (isspace(line[len])))) {
			line[len]=0 ;
			len-- ;
		}
		/* Detect multi-line */
		if (line[len]=='\\') {
			/* Multi-line value */
			last=len ;
			continue ;
		} else {
			last=0 ;
		}
		switch (OSA_iniPsr_line(line, section, key, val)) {
			case LINE_EMPTY:
			case LINE_COMMENT:
			break ;

			case LINE_SECTION:
			errs = dictionary_set(dict, section, NULL);
			break ;

			case LINE_VALUE:
			sprintf(tmp, "%s:%s", section, key);
			errs = dictionary_set(dict, tmp, val) ;
			break ;

			case LINE_ERROR:
			OSA_ERROR("OSA_iniPsr: syntax error in %s (%d):\n",
							ininame,
							lineno);
			fprintf(stderr, "-> %s\n", line);
			errs++ ;
			break;

			default:
			break ;
		}
		memset(line, 0, ASCIILINESZ);
		last=0;
		if (errs<0) {
			OSA_ERROR("OSA_iniPsr: memory allocation failure\n");
			break ;
		}
	}
	if (errs) {
		dictionary_del(dict);
		dict = NULL ;
	}
	fclose(in);
	return dict ;
}


/*
 ******************************************************************************
 SOURCE CODE - OSA_iniPsr_freedict() routine BEGINS HERE
 ******************************************************************************
 // Function Name: OSA_iniPsr_freedict()
 // Argument:
	a) Input: 	d Dictionary to free
	b) Output:	void
 // Description  :
	Free all memory associated to an ini OSA_iniDic

	Free all memory associated to an ini OSA_iniDic.
	It is mandatory to call this function before the OSA_iniDic object
	gets out of the current context.

 // FYI:
 *****************************************************************************
*/
void OSA_iniPsr_freedict(OSA_iniDic * d)
{
	dictionary_del(d);
}

/* vim: set ts=4 et sw=4 tw=75 */

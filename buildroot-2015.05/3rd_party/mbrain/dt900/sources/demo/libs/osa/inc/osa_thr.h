/**
 * @file		  osa_thr.h
 * @Author 	      Joel,Kim (Joel.Kim@mbrain.org)
 * @date		  2015.07.12
 * @brief		  Thread helper APIs
 * 
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain
 * 
 * (c) Copyright mBrain(2015).		  All Rights Reserved.
 */

/*! [ 1]. File classification */
#ifndef _OSA_THR_H_
#define _OSA_THR_H_


/*! [ 2]. File Directive (#include) */
#include <osa.h>


/*! [ 3]. Global macro and enum */
#define OSA_THR_PRI_MAX                         sched_get_priority_max(SCHED_FIFO)
#define OSA_THR_PRI_MIN                         sched_get_priority_min(SCHED_FIFO)

#define OSA_THR_PRI_DEFAULT                     ( OSA_THR_PRI_MIN + (OSA_THR_PRI_MAX-OSA_THR_PRI_MIN)/2 )

#define OSA_THR_STACK_SIZE_DEFAULT              0


/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
typedef void * (*OSA_ThrEntryFunc)(void *);

typedef struct {
	pthread_t 		 hndl;
} OSA_ThrHndl;


/*! [ 5]. Global Function prototype declaration */
int OSA_thrCreate(OSA_ThrHndl *hndl, OSA_ThrEntryFunc entryFunc, Uint32 pri, Uint32 stackSize, void *prm);
int OSA_thrDelete(OSA_ThrHndl *hndl);
int OSA_thrJoin(OSA_ThrHndl *hndl);
int OSA_thrChangePri(OSA_ThrHndl *hndl, Uint32 pri);
int OSA_thrExit(void *returnVal);

/*! [ 6]. Global Variable declaration  */


#endif /* _OSA_THR_H_ */


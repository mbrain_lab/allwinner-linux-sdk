/**
 * @file		osa_mutex.h
 * @Author 	    Joel,Kim (Joel.Kim@mbrain.org)
 * @date		2015.07.12
 * @brief		Mutex helper APIs
 * 
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain
 * 
 * (c) Copyright mBrain(2015).		All Rights Reserved.
 */

/*! [ 1]. File classification */
#ifndef _OSA_MUTEX_H_
#define _OSA_MUTEX_H_

/*! [ 2]. File Directive (#include) */
#include <pthread.h>



/*! [ 3]. Global macro and enum */


/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
typedef struct {

	pthread_mutex_t lock;

} OSA_MutexHndl;

#include <osa.h>

/*! [ 5]. Global Function prototype declaration */
int OSA_mutexCreate(OSA_MutexHndl *hndl);
int OSA_mutexDelete(OSA_MutexHndl *hndl);
int OSA_mutexLock(OSA_MutexHndl *hndl);
int OSA_mutexUnlock(OSA_MutexHndl *hndl);


/*! [ 6]. Global Variable declaration  */


#endif /* _OSA_MUTEX_H_ */


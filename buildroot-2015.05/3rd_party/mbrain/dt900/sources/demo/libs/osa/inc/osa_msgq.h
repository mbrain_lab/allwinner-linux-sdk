/**
 * @file	   osa_msgq.h
 * @Author     Joel,Kim (Joel.Kim@mbrain.org)
 * @date	   2015.07.12
 * @brief	   MessageQueue helper APIs
 * 
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain
 * 
 * (c) Copyright mBrain(2015).	   All Rights Reserved.
 */

/*! [ 1]. File classification */
#ifndef _OSA_MSGQ_H_
#define _OSA_MSGQ_H_


/*! [ 2]. File Directive (#include) */
#include <osa.h>


/*! [ 3]. Global macro and enum */
#define OSA_MSGQ_LEN_MAX		32

struct OSA_MsgHndl;

#define OSA_msgGetCmd(msg)             ( (msg)->cmd )
#define OSA_msgGetPrm(msg)             ( (msg)->pPrm )
#define OSA_msgGetAckStatus(msg)       ( (msg)->status )


/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
typedef struct {

	Uint32 curRd;
	Uint32 curWr;
	Uint32 len;
	Uint32 count;

	struct OSA_MsgHndl *queue[OSA_MSGQ_LEN_MAX];

	pthread_mutex_t lock;
	pthread_cond_t	condRd;
	pthread_cond_t	condWr;
	
} OSA_MsgqHndl;

typedef struct OSA_MsgHndl {

	OSA_MsgqHndl *pTo;
	OSA_MsgqHndl *pFrom;	
	void				 *pPrm;
	int 					status;
	Uint16				cmd;
	Uint16				flags;

} OSA_MsgHndl;


/*! [ 5]. Global Function prototype declaration */
int OSA_msgqCreate(OSA_MsgqHndl *hndl);
int OSA_msgqDelete(OSA_MsgqHndl *hndl);
int OSA_msgqSendMsg(OSA_MsgqHndl *to, OSA_MsgqHndl *from, Uint16 cmd, void *prm, Uint16 msgFlags, OSA_MsgHndl **msg);
int OSA_msgqRecvMsg(OSA_MsgqHndl *hndl, OSA_MsgHndl **msg, Uint32 timeout);
int OSA_msgqSendAck(OSA_MsgHndl *msg, int ackRetVal);
int OSA_msgqFreeMsgHndl(OSA_MsgHndl *msg);


/*! [ 6]. Global Variable declaration  */


#endif /* _OSA_FLG_H_ */


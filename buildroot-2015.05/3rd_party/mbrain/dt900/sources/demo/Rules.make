# (c) mBrain

CURDIR := $(shell if [ "$$PWD" != "" ]; then echo $$PWD; else pwd; fi)

# Linux kernel schedule type
DEMO_KERNEL_SCHEDULE_TYPE := NORNAL

ifeq ($(DEMO_PLATFORM),x86)
$(info : "X86 platform selected")
TARGET_CC :=
DEMO_INSTALL_DIR := ""
else ifeq ($(DEMO_PLATFORM),ARM)
$(info : "ARM platform selected")
TARGET_CC := $(CROSS_COMPILE)
DEMO_INSTALL_DIR := $(INSTALLDIR)
ifeq ($(TARGET_CC),)
$(error : "Compiler doesn't defiled...")
endif
else
$(error : "Invalid DEMO_PLATFORM, $(DEMO_PLATFORM)")
endif




DEMO_ROOT_DIR := $(CURDIR)
DEMO_BUILD_DIR := $(DEMO_ROOT_DIR)/build
DEMO_SDK_DIR := $(SDKROOT)


DEMO_CONFIG :=
ifeq ($(DEMO_CONFIG), )
DEMO_CONFIG := debug
endif




#
#export GLOBAL Variables
#
export DEMO_SDK_DIR
export DEMO_PLATFORM
export DEMO_ROOT_DIR
export DEMO_BUILD_DIR
export DEMO_CONFIG
export TARGET_CC
export DEMO_INSTALL_DIR


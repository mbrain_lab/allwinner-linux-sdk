/*
 * (C) Copyright 2007-2013
 * Allwinner Technology Co., Ltd. <www.allwinnertech.com>
 * Char <yanjianbo@allwinnertech.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <boot_type.h>
#include <sunxi_mbr.h>
#include <sys_partition.h>

#define MAGIC		"sunxi"
#define PART_NAME	"private"
#define USER_DATA_MAXSIZE	(8 * 1024)


#define	NAME_SIZE	32
#define VALUE_SIZE	128

typedef struct {
	char magic_name[8];
	int count;
	int reserved[3];
}USER_DATA_HEAR;

typedef struct {
	char name[NAME_SIZE];
	char value[VALUE_SIZE];
	int valid;					//0: Valid 1: Invalid (not being updated
	int reserved[3];
}USER_PRIVATE_DATA;

char *IGNORE_ENV_VARIABLE[] = { "console",
								"root",
								"init",
								"loglevel",
								"partitions"
							};

int USER_DATA_NUM;									//The number of variables the user's environment
char USER_DATA_NAME[10][NAME_SIZE] = {{'\0'}};		//User environment variables (available from env.fex)

void check_user_data(void);
/*
************************************************************************************************************
*
*                                             function
*
*    name          :
*
*    parmeters     :
*
*    return        :
*
*    note          :
*
*
************************************************************************************************************
*/
int save_user_private_data(char *name, char *data)
{
	int j;
	unsigned int part_offset;					//Address offset partition
	unsigned int part_size;						//Size of the partition
	unsigned int user_data_offset;				//Users store data address offset
	char user_data_buffer[USER_DATA_MAXSIZE] = {0};	//
	USER_PRIVATE_DATA *user_data_p = NULL;
	USER_DATA_HEAR *user_data_head = NULL;
	char cmp_data_name[8] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};		//It used to verify whether an area is to use flash

	if (!name || !data) {
		printf("error: the name (data) is null\n");
		return 0;
	}

	part_size = sunxi_partition_get_size_byname(PART_NAME);
	if (part_size > 0)
	{
		part_offset = sunxi_partition_get_offset_byname(PART_NAME);
		user_data_offset = part_offset + part_size - (USER_DATA_MAXSIZE >> 9);		//Access to users store data address offset
		if (!sunxi_flash_read(user_data_offset, USER_DATA_MAXSIZE >> 9, user_data_buffer)) {
			printf("read flash error\n");
			return 0;
		}

		user_data_head = (USER_DATA_HEAR *)user_data_buffer;
		user_data_p = (USER_PRIVATE_DATA *)(user_data_buffer + sizeof(USER_DATA_HEAR));
		if (strncmp(user_data_head->magic_name, MAGIC, 5)) {
			memset(user_data_buffer, 0xff, USER_DATA_MAXSIZE);
			strcpy(user_data_head->magic_name, MAGIC);
			//sunxi_flash_write(user_data_offset, USER_DATA_MAXSIZE >> 9, user_data_buffer);	//Empty flash, flash to prevent other useless data
			printf("init the (user) private space\n");
		}

		if (strncmp(cmp_data_name, (char *)user_data_head, 4)) {			//Determining whether a piece of flash area to be used
			if (user_data_head->count > 0) {
				for (j = 0; j < user_data_head->count; j++) {
					if (!strcmp(name, user_data_p->name)) {					//Matching the data item name
						strcpy(user_data_p->value, data);					//Update Data
						user_data_p->valid = 1;							    //Update RMS
						printf("Saving Environment to \n");
						break;
					}
					user_data_p++;
				}
				if (j == user_data_head->count) {
						strcpy(user_data_p->name, name);					//In the tail adding new data
						strcpy(user_data_p->value, data);
						user_data_p->valid = 1;
						user_data_head->count++;
						printf("Saving Environment to \n");
				}
			}
			else {
				strcpy(user_data_p->name, name);					//First data
				strcpy(user_data_p->value, data);
				user_data_p->valid = 1;
				user_data_head->count++;
				printf("Saving Environment to \n");
			}
		}
		else {
			user_data_head->count = 1;							//First data
			user_data_p = (USER_PRIVATE_DATA *)(user_data_buffer + sizeof(USER_DATA_HEAR));
			strcpy(user_data_p->name, name);
			strcpy(user_data_p->value, data);
			user_data_p->valid = 1;
			printf("Saving Environment to \n");
		}
		sunxi_flash_write(user_data_offset, USER_DATA_MAXSIZE >> 9, user_data_buffer);	//Written back to the flash
		sunxi_flash_flush();

		return 0;
	}
	printf("the part isn't exist\n");
	return 0;
}


int do_save_user_data (cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
	if (argc < 3) {
		printf("usage: saveud <name> <data>\n");
		return 0;
	}
	if (argc == 3) {
		save_user_private_data(argv[1], argv[2]);
	}

	return 0;
}

U_BOOT_CMD(
	save_userdata,	3,	1,	do_save_user_data,
	"save user data",
	"<name> <data>\n"
);

/*
************************************************************************************************************
*
*                                             function
*
*    name          :
*
*    parmeters     :
*
*    return        :
*
*    note          :
*
*
************************************************************************************************************
*/
//int user_data_list()
//{
//	int j;
//	unsigned int part_offset;					//Address offset partition
//	unsigned int part_size;						//Size of the partition
//	unsigned int user_data_offset;				//Users store data address offset
//	char user_data_buffer[USER_DATA_MAXSIZE];	//
//	USER_PRIVATE_DATA *user_data_p = NULL;
//	USER_DATA_HEAR *user_data_head = NULL;
//	char cmp_data_name[8] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};		//It used to verify whether an area is to use flash
//
//	printf("user_data_list\n");
//	part_size = sunxi_partition_get_size_byname(PART_NAME);
//	if (part_size > 0) {
//		part_offset = sunxi_partition_get_offset_byname(PART_NAME);
//
//		user_data_offset = part_offset + part_size - (USER_DATA_MAXSIZE >> 9);		//Access to users store data address offset
//		if (!sunxi_flash_read(user_data_offset, USER_DATA_MAXSIZE >> 9, user_data_buffer)) {
//			printf("read flash error\n");
//			return 0;
//		}
//
//		user_data_head = (USER_DATA_HEAR *)user_data_buffer;
//		user_data_p = (USER_PRIVATE_DATA *)(user_data_buffer + sizeof(USER_DATA_HEAR));
//		if (strncmp(cmp_data_name, (char *)user_data_head, 4)) {			//Determining whether a piece of flash area to be used
//			if (user_data_head->count > 0) {
//			printf("count = %d\n", user_data_head->count);
//				for (j = 0; j < user_data_head->count; j++) {
//					printf("read: %s		%d		%s\n", user_data_p->name, user_data_p->valid, user_data_p->value);
//					user_data_p++;
//				}
//				return 0;
//			}
//		}
//		printf("not user data\n");
//		return 0;
//	}
//	printf("the part isn't exist\n");
//	return 0;
//}

/*
************************************************************************************************************
*
*                                             function
*
*    name          :
*
*    parmeters     :
*
*    return        :
*
*    note          :
*
*
************************************************************************************************************
*/
int update_user_data(void)
{
	int i, j;
	unsigned int part_offset;					//Address offset partition
	unsigned int part_size;						//Size of the partition
	unsigned int user_data_offset;				//Users store data address offset
	char user_data_buffer[USER_DATA_MAXSIZE];	//
	USER_PRIVATE_DATA *user_data_p = NULL;
	USER_DATA_HEAR *user_data_head = NULL;
	char cmp_data_name[8] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};		//It used to verify whether an area is to use flash

	part_size = sunxi_partition_get_size_byname(PART_NAME);
	if (part_size > 0) {
		part_offset = sunxi_partition_get_offset_byname(PART_NAME);
		user_data_offset = part_offset + part_size - (USER_DATA_MAXSIZE >> 9);		//Access to users store data address offset
		if (!sunxi_flash_read(user_data_offset, USER_DATA_MAXSIZE >> 9, user_data_buffer)) {
			printf("read flash error\n");
			return 0;
		}

		user_data_head = (USER_DATA_HEAR *)user_data_buffer;

		if (strncmp(user_data_head->magic_name, MAGIC, 5)) { 				//Check whether the data is valid
			printf("the user data'magic is bad\n");
			return 0;
		}

		check_user_data();													//Env detected from the user's environment variables

		if (strncmp(cmp_data_name, (char *)user_data_head, 4)) {			//Determining whether a piece of flash area to be used
			if (user_data_head->count > 0) {
				for (i = 0; i < USER_DATA_NUM; i++) {
					user_data_p = (USER_PRIVATE_DATA *)(user_data_buffer + sizeof(USER_DATA_HEAR));
					for (j = 0; j < user_data_head->count; j++) {
						//if (!strcmp(USER_DATA_NAME[i], user_data_p->name) && user_data_p->valid) {					//Matching the data item name
						if (!strcmp(USER_DATA_NAME[i], user_data_p->name))
						{
							setenv(user_data_p->name, user_data_p->value);
							printf("user data is updataed\n");
						}
						user_data_p++;
					}
				}
				return 0;
			}
		}
		printf("not the user data to updata\n");
		return 0;
	}
	printf("the part isn't exist\n");
	return 0;
}

/*
************************************************************************************************************
*
*                                             function
*
*    name          :
*
*    parmeters     :
*
*    return        :
*
*    note          :		 Get the user's environment variables
*
*
************************************************************************************************************
*/
void check_user_data(void)
{
	char *command_p  =NULL;
	char temp_name[32] = {'\0'};
	int i, j;

	//command_p = getenv("boot_base");  //The combined method parameters nand and mmc start after being not
	if((uboot_spare_head.boot_data.storage_type == 1) || (uboot_spare_head.boot_data.storage_type == 2))
	{
		command_p = getenv("setargs_mmc");
	}
	else
	{
		command_p = getenv("setargs_nand");
	}
	if (!command_p) {
		printf("cann't get the boot_base from the env\n");
		return ;
	}

	while (*command_p != '\0' && *command_p != ' ') {		//The first environment variable filter
		command_p++;
	}
	command_p++;
	while (*command_p == ' ') {								//Filtration extra space
		command_p++;
	}
	while (*command_p != '\0' && *command_p != ' ') { 		//The second filter environmental variables
		command_p++;
	}
	command_p++;
	while (*command_p == ' ') {
		command_p++;
	}

	USER_DATA_NUM =0;
	while (*command_p != '\0') {
		i = 0;
		while (*command_p != '=') {
			temp_name[i++] = *command_p;
			command_p++;
		}
		temp_name[i] = '\0';
		if (i != 0) {
			for (j = 0; j < sizeof(IGNORE_ENV_VARIABLE) / sizeof(int); j++) {
				if (!strcmp(IGNORE_ENV_VARIABLE[j], temp_name)) {			//The dictionary database, excluding system environment variables, users get the data
					break;
				}
			}
			if (j >= sizeof(IGNORE_ENV_VARIABLE) / sizeof(int)) {
				strcpy(USER_DATA_NAME[USER_DATA_NUM], temp_name);
				USER_DATA_NUM++;
			}
		}
		while (*command_p != '\0' && *command_p != ' ') {					//Under a variable
			command_p++;
		}
		while (*command_p == ' ') {
			command_p++;
		}
	}
/*
	printf("USER_DATA_NUM = %d\n", USER_DATA_NUM);
	for (i = 0; i < USER_DATA_NUM; i++) {
		printf("user data = %s\n", USER_DATA_NAME[i]);
	}
*/
}


/**
 * @file   ft_hdmi.c
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.07.07
 * @brief  HDMI funnction test for Hummingbird A31
 *
 * Detailed description of file.
 *
 * @copyright
 * (c) Copyright MBRAIN & mBrain(2015).  All Rights Reserved.
 */

/*! [ 1]. File Directive (#include) */
#include <stdio.h>
#include <osa.h>
#include <drv_gpio.h>
#include <drv_spi.h>
#include <drv_gpio.h>
#include "imx122.reg"


/*! [ 2]. Local Macro (#define, enum) */
//#define STEP_BY_STEP_TEST
/*! [ 3]. Type definition or structure declaration (typedef or struct) */
/*! [ 4]. Local Function prototype declaration */
/*! [ 5]. Local Variable declaration */
int gIsExitMainTask=FALSE;
DRV_SpiHndl* gDrvHndl=NULL;


static void SignalTraps(void);
static void SignalHandler(int sig);

void SignalTraps(void)
{
	OSA_attachSignalHandler(SIGTERM, SignalHandler);
	OSA_attachSignalHandler(SIGINT,  SignalHandler);
}

void SignalHandler(int sig)
{
	if ( (sig==SIGTERM)||(sig==SIGINT) ){
		OSA_printf("gIsExitRecvThr was setted with 1\n");
		gIsExitMainTask=TRUE;
	}

}




int _imgsIMX122_SetReg(unsigned char nDev, unsigned char nAddr, unsigned char nRegVal)
{
	int count=0,status=0;
	unsigned char regset[5]; 

	if( gDrvHndl==NULL ){
		OSA_ERROR("Invalid driver handle\n");
		return OSA_EFAIL;
	}

	regset[count++]=nDev;
	regset[count++]=nAddr;
	regset[count++]=nRegVal; 


	DRV_GpioSetVal(PORT_H, 9, 0);
	status = DRV_spiWrite8(gDrvHndl, regset, count);
	DRV_GpioSetVal(PORT_H, 9, 1);

	if(status){
		return OSA_EFAIL;
	}

	return OSA_SOK;
}

 
int _imgsIMX122_GetReg(unsigned char nDev, unsigned char nAddr, unsigned char* pRegVal)
{ 
	int count=0, status=0;
	Uint8 cmdset[3];
	Uint8 regset[4]={0,0,0};

	if( gDrvHndl==NULL ){
		OSA_ERROR("Invalid driver handle\n");
		return OSA_EFAIL;
	}

	cmdset[count++] = nDev|0x80;
	cmdset[count++] = nAddr;
	DRV_GpioSetVal(PORT_H, 9, 0);
	status = DRV_spiRead8(gDrvHndl, cmdset, 3, regset);
	DRV_GpioSetVal(PORT_H, 9, 1);

	if(status){
		return OSA_EFAIL;
	}

	*pRegVal = regset[2];
	OSA_printf("regset[%02x][%02x][%02x]\n",regset[0],regset[1],regset[2]);

	return OSA_SOK;
}





/**
 * @name    main()
 * @brief   main function
 * @ingroup Group_FunctionTest
 * @param <Reserved>
 *
 * @retval <Reserved>
 *
 * Example Usage:
 * @code
 * @endcode
 */
int main(int argc, char **argv)
{
	int i;
	#ifdef STEP_BY_STEP_TEST
	int ch;
	unsigned char tmpBuf[32]={0,};
	#endif
	DRV_SPICreatParam drvParam;

	/* Init OSA library */
	OSA_init();

	/* Register signal */
	SignalTraps();


	OSA_printf("[%s] Start\n",__FILE__);

	snprintf(drvParam.dev,128,"/dev/spidev2.0");
	drvParam.bits = SPI_DRV_8BIT;
	drvParam.byteOrder = SPI_MODE_3;
	drvParam.mode = 0;
	//drvParam.speed = 4000000;
	drvParam.speed = 40000;
	drvParam.delay = 0;

	gDrvHndl = DRV_spiInit(&drvParam,NULL);
	if (gDrvHndl==NULL){
		OSA_ERROR("Fail to init %s\n",drvParam.dev);
	}

	OSA_printf("IMX122 Register Setup...\n");
	for ( i=0;i<513; i++ )
	{
		#ifdef STEP_BY_STEP_TEST
		OSA_printf("Press Any button to Write Value...\n");
		ch=getchar();
		if (ch=='q'){
			DRV_spiDeinit(gDrvHndl);
			return OSA_EFAIL;
		}
		#endif
		_imgsIMX122_SetReg( IMX122_DEFAULT_REG[i].devid, 
							IMX122_DEFAULT_REG[i].addr, 
							IMX122_DEFAULT_REG[i].val);
		#ifdef STEP_BY_STEP_TEST
		OSA_printf("Press Any button to Read Value...\n");
		ch=getchar();
		memset(tmpBuf,0x00,32);
		_imgsIMX122_GetReg( IMX122_DEFAULT_REG[i].devid, 
							IMX122_DEFAULT_REG[i].addr, 
							tmpBuf);
		OSA_printf("Dev[%02x]:Reg Addr=0x%02x, Value=0x%02x\n",
							IMX122_DEFAULT_REG[i].devid,
							IMX122_DEFAULT_REG[i].addr, 
							tmpBuf[0]);
		#endif
						
	}

#ifdef __DUMP_IMX122_REG__
	OSA_printf("\n=========================[REG Group A]\n");
	for ( i=0;i<ID00_CNT; i++ )
	{
	_imgsIMX122_GetRegA(i,&nRdVal);
	OSA_waitUsecs(10);
	OSA_printf("Reg Addr=0x%02x, Value=0x%02x\n",i,nRdVal);
	}

	OSA_printf("\n=========================[REG Group B]\n");
	for ( i=0;i<ID01_CNT; i++ ){
	_imgsIMX122_GetRegB(i,&nRdVal);
	OSA_waitUsecs(10);
	OSA_printf("Reg Addr=0x%02x, Value=0x%02x\n",i,nRdVal);
	}
#endif


	DRV_spiDeinit(gDrvHndl);

	/*! Deinit OSA library */
	OSA_exit();
	return OSA_SOK;
}


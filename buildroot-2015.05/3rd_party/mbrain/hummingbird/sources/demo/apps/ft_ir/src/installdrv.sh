#!/bin/bash

drv_list=("sun6i-ir.ko")

for drv in "${drv_list[@]}"; do
	#echo $drv
	file_path=$(find /lib/modules/ -name ${drv})
	if [[ -z "$file_path" ]]; then
		echo "Cannot find device driver[${drv}]"
	else
		echo "Install device driver[${drv}]"
		insmod $file_path
		sleep 0.1
	fi
done

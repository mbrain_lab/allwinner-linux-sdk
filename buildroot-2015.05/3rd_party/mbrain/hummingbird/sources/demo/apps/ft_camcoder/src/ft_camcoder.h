/**
 * @file   ft_camcoder.h
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.08.02
 * @brief  ft_camcoder header file
 * 
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain & MidmeSoft
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain & MidmeSoft
 * 
 * (c) Copyright mBrain & MidmeSoft(2015).  All Rights Reserved.
 */


/*! [ 1]. File classification */

#ifndef _FT_CAMCODER_H_
#define _FT_CAMCODER_H_

#  ifdef __cplusplus
extern "C" {
#  endif /* __cplusplus */

/*! [ 2]. File Directive (#include) */
#include <osa.h>
#include <osa_tsk.h>
#include <osa_mutex.h>
#include <osa_rendezvous.h>




/*! [ 3]. Global macro and enum */
#define SCREEN_1080P_WIDTH	1920
#define SCREEN_1080P_HEIGHT	1080

#define SCREEN_720P_WIDTH	1280
#define SCREEN_720P_HEIGHT	720

#define CAM_CH0_KBUFCNT		5


typedef enum
{
	TASK_MAIN              =0,
    TASK_CAPTURE             ,
    TASK_DISPLAY             ,
    TASK_ENCODER             ,
    TASK_MAXCNT
}TASK;


typedef enum
{
	CAM_CH0                =0,
	CAM_CH1                  ,
	CAM_CH_MAXCNT
}CAM_CHANNEL;

typedef enum
{
	DISP_CH0                =0,
    DISP_CH_MAXCNT
}DISP_CHANNEL;


#define SZ_DESC_MAX			128

/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
typedef struct {
	Int32   nSX;
	Int32   nSY;
	Uint32  nWidth;
	Uint32  nHeight;
} _win;

typedef struct {
	char    szName[SZ_DESC_MAX];
	_win    win;
	int     kBuf_cnt;                     //<< Kernel buffer count 
	int     isp_fmt;                      //<< ISP output format
	int     sen_fmt;                      //<< Image sensor Format
} _cam;

typedef struct {
	char    szName[SZ_DESC_MAX];
	_win    win[CAM_CH_MAXCNT];
	_win	canvas;

	int     inPixFmt;
	
	int		nOutputIF;
	int     nOutputStd;
} _disp;



typedef struct {
	_cam    camera[CAM_CH_MAXCNT];
	_disp   display[DISP_CH_MAXCNT];
} Config;


typedef struct {
	Bool                 isExit;
	OSA_TskHndl          task[TASK_MAXCNT];
	OSA_MbxHndl          task_Mbox;
	Rndez_Handle         task_Syncker;
} Ctrl;



/*! [ 5]. Global Function prototype declaration */


/*! [ 6]. Global Variable declaration  */





#  ifdef __cplusplus
    }
#  endif /* __cplusplus */
#endif //_FT_CAMCODER_H_


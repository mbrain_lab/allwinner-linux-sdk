/**
 * @file   encoder.c
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.08.02
 * @brief  Encoder task
 *
 * Detailed description of file.
 *
 * @copyright
 * (c) Copyright MBRAIN & mBrain(2015).  All Rights Reserved.
 */

/*! [ 1]. File Directive (#include) */
#include "encoder.h"
#include "task.h"



/*! [ 2]. Local Macro (#define, enum) */

/*! [ 3]. Type definition or structure declaration (typedef or struct) */

/*! [ 4]. Local Function prototype declaration */

/*! [ 5]. Local Variable declaration */
/*! [ 6]. Global Variable declaration */






/**
 * @name    TaskEncoder()
 * @brief   Encoder Task
 * @ingroup Group_FunctionTest
 * @param: None
 *
 * @retval: None
 *
 * Example Usage:
 * @code
 *  <reserved>
 * @endcode
 */
int TaskEncoder( struct OSA_TskHndl *pPrc, OSA_MsgHndl *pMsg, Uint32 curState )
{
	int retVal = OSA_SOK;
	Uint16 cmd = NULL;

	/**! Loop operation */
	if (pMsg==NULL)
	{
		//OSA_waitMsecs(1000);
		//OSA_printf("Task[Encoder] Loop Operation...\n");
	}
	else
	{
		cmd = OSA_msgGetCmd(pMsg);

		switch(cmd)
		{
			case TASK_CMD_READY:
				OSA_printf("[ENCODER TASK] Get READY command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_READY);
				break;
				
			case TASK_CMD_INIT:
				OSA_printf("[ENCODER TASK] Get INIT command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_INIT);
				break;

			case TASK_CMD_DEINIT:
				OSA_printf("[ENCODER TASK] Get DEINIT command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_DEINIT);
				break;
				
			case TASK_CMD_RUN:
				OSA_printf("[ENCODER TASK] Get RUN command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_RUN);
				break;
				
			case TASK_CMD_STOP:
				OSA_printf("[ENCODER TASK] Get STOP command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_STOP);
				break;
				
			case TASK_CMD_RESUME:
				OSA_printf("[ENCODER TASK] Get RESUME command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_RESUME);
				break;
				
			case TASK_CMD_PAUSE:
				OSA_printf("[ENCODER TASK] Get PAUSE command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_PAUSE);
				break;
				
			case TASK_CMD_RESET:
				OSA_printf("[ENCODER TASK] Get RESET command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_RESET);
				break;
			default:
				break;
		}
	}

	return retVal;
}




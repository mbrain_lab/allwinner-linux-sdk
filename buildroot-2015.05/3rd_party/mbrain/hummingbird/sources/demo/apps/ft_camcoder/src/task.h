/**
 * @file   task.h
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.08.02
 * @brief  App Task header
 * 
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain & MidmeSoft
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain & MidmeSoft
 * 
 * (c) Copyright mBrain & MidmeSoft(2015).  All Rights Reserved.
 */


/*! [ 1]. File classification */
#ifndef _TASK_H_
#define _TASK_H_


/*! [ 2]. File Directive (#include) */
#include <osa.h>
#include <osa_tsk.h>

/*! [ 3]. Global macro and enum*/
typedef enum
{
    TASK_CMD_READY           =0,
    TASK_CMD_CREATE            ,
    TASK_CMD_DELETE            ,
    TASK_CMD_INIT              ,
    TASK_CMD_DEINIT            ,
    TASK_CMD_RUN               ,
    TASK_CMD_STOP              ,
    TASK_CMD_RESUME            ,
    TASK_CMD_PAUSE             ,
    TASK_CMD_RESET             ,
    TASK_CMD_MAXCNT
}TASK_CMD;

typedef enum
{
    TASK_STATUS_READY        =0,
    TASK_STATUS_INIT           ,
    TASK_STATUS_DEINIT         ,
    TASK_STATUS_RUN            ,
    TASK_STATUS_STOP           ,
    TASK_STATUS_RESUME         ,
    TASK_STATUS_PAUSE          ,
    TASK_STATUS_RESET          ,
    TASK_STATUS_ERROR          ,
    TASK_STATUS_MAXCNT
}TASK_STATUS;


/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
/*! [ 5]. Global Function prototype declaration */
/*! [ 6]. Global Variable declaration  */




#endif //_TASK_H_





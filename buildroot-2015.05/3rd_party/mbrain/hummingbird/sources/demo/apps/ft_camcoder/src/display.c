/**
 * @file   display.c
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.08.02
 * @brief  Display task
 *
 * Detailed description of file.
 *
 * @copyright
 * (c) Copyright MBRAIN & mBrain(2015).  All Rights Reserved.
 */

/*! [ 1]. File Directive (#include) */
#include <osa.h>
#include <osa_debug.h>
#include <osa_buf.h>

#include <linux/videodev2_new.h>

#include "display.h"
#include "task.h"
#include "ft_camcoder.h"



/*! [ 2]. Local Macro (#define, enum) */
#define PMEM_DEVICE	        "/dev/sunxi_mem"
#define DISP_DEVICE			"/dev/disp"
#define FB0_DEVICE          "/dev/fb0"
#define FB1_DEVICE          "/dev/fb1"

#define SCREEN_0	        0


/*! [ 3]. Type definition or structure declaration (typedef or struct) */
typedef struct {
	char  szName[SZ_DESC_MAX];
	int   phyAddr;
	void* virAddr;
	int   length;
	int   isValid;
} dspBufPool;

typedef struct {
	int                 fd_phymem;
	
	int                 fd_dsp;
	int                 fd_fb[DISP_CH_MAXCNT][DISP_LAYER_MAXCNT];
	dspBufPool          fb_bufPool[DISP_CH_MAXCNT][DISP_LAYER_MAXCNT];
	int                 fb_mode[DISP_LAYER_MAXCNT];
	int                 fb_fmt[DISP_LAYER_MAXCNT];
	int                 fb_seq[DISP_LAYER_MAXCNT];
	int                 if_type[DISP_CH_MAXCNT];
	int                 output_std[DISP_CH_MAXCNT];

	Uint32              layerId[DISP_LAYER_MAXCNT];
	__disp_layer_info_t layerParam[DISP_LAYER_MAXCNT];

	OSA_ThrHndl         thrDsp;
	int                 exitThrDsp;
} sys_disp;

/*! [ 4]. Local Function prototype declaration */

/*! [ 5]. Local Variable declaration */
/*! [ 6]. Global Variable declaration */
sys_disp gSysDisp;


#define COLOR_WHITE                     0xffffffff
#define COLOR_YELLOW                    0xffffff00
#define COLOR_GREEN                     0xff00ff00
#define COLOR_CYAN                      0xff00ffff
#define COLOR_MAGENTA                   0xffff00ff
#define COLOR_RED                       0xffff0000
#define COLOR_BLUE                      0xff0000ff
#define COLOR_BLACK                     0x00000000


/*! [ 3]. Type definition or structure declaration (typedef or struct) */
static unsigned int colorbar[8] = 
{
    COLOR_WHITE,
    COLOR_YELLOW,
    COLOR_CYAN,
    COLOR_GREEN,
    COLOR_MAGENTA,
    COLOR_RED,
    COLOR_BLUE,
    COLOR_BLACK
};


int Display_Init(_disp *cfg)
{
	int status = OSA_EFAIL;
	Uint32 reqLayerId = 0;
	int phyFreeMemSize = 0;
	int chIdx=0, disp_idx=0, lIdx=0;
	unsigned long disp_param[4]={0,};
	unsigned long pmem_param[4]={0,};
	__disp_output_type_t disp_output_type;
	void *fb_tmpBuf = NULL;

	for (disp_idx=0; disp_idx<DISP_CH_MAXCNT; disp_idx++)
	{
		OSA_printf("DISP Name=%s\n", cfg[disp_idx].szName);
		OSA_printf("   sx=%d\n",cfg[disp_idx].canvas.nSX);
		OSA_printf("   sy=%d\n",cfg[disp_idx].canvas.nSY);
		OSA_printf("   width=%d\n",cfg[disp_idx].canvas.nWidth);
		OSA_printf("   height=%d\n\n",cfg[disp_idx].canvas.nHeight);
	}	

	memset(&gSysDisp,0x00,sizeof(sys_disp));
	gSysDisp.output_std[DISP_CH0] = cfg[DISP_CH0].nOutputStd;
	gSysDisp.if_type[DISP_CH0] = cfg[DISP_CH0].nOutputIF;

	gSysDisp.exitThrDsp = FALSE;
	
	gSysDisp.fb_fmt[DISP_LAYER_0] = cfg[DISP_CH0].inPixFmt;
	gSysDisp.fb_mode[DISP_LAYER_0] = DISP_MOD_NON_MB_UV_COMBINED;
    gSysDisp.fb_seq[DISP_LAYER_0] = DISP_SEQ_UVUV;

    gSysDisp.fb_fmt[DISP_LAYER_1] = DISP_FORMAT_ARGB8888;
	gSysDisp.fb_mode[DISP_LAYER_1] = DISP_MOD_INTERLEAVED;
	gSysDisp.fb_seq[DISP_LAYER_1] = DISP_SEQ_ARGB;
	



	gSysDisp.fd_phymem = open(PMEM_DEVICE, O_RDWR /* required */ | O_NONBLOCK, 0);
	if (gSysDisp.fd_phymem<0){
		OSA_ERROR("Fail to open %s\n",PMEM_DEVICE);
		return OSA_EFAIL;
	}

	


	if((gSysDisp.fd_dsp = open(DISP_DEVICE,O_RDWR)) == -1)
	{
		OSA_ERROR("Fail to open %s\n",DISP_DEVICE);
		return OSA_EFAIL;
	}

	disp_param[0] = 0;
	disp_output_type = (__disp_output_type_t)ioctl(gSysDisp.fd_dsp, DISP_CMD_GET_OUTPUT_TYPE,(void*)disp_param);
	OSA_printf("DISPSEL[%lld]'s disp_output_type=%d\n",disp_param[0],disp_output_type);   

	if(gSysDisp.if_type[DISP_CH0] == DISP_OIF_HDMI)
	{
		disp_param[0] = 0;
		status = ioctl(gSysDisp.fd_dsp, DISP_CMD_HDMI_GET_HPD_STATUS, disp_param);
		OSA_printf("ret=%d\n",status);


		disp_param[0] = SCREEN_0;
		disp_param[1] = gSysDisp.output_std[DISP_CH0];
		status = ioctl(gSysDisp.fd_dsp, DISP_CMD_HDMI_SET_MODE, disp_param);
		if (status < 0) {
			OSA_ERROR("hdmitester: set hdmi output mode failed(%d)\n", status);
			goto __error_close_dsp_dev;
		}
	}


	/* Reset layer id for requesting new layer */
	reqLayerId = 0;

	/* Request New Layer */
	disp_param[0] = SCREEN_0;
    disp_param[1] = DISP_LAYER_WORK_MODE_SCALER;
    reqLayerId = ioctl(gSysDisp.fd_dsp, DISP_CMD_LAYER_REQUEST, (void*)disp_param);
    if (reqLayerId == 0) 
	{
		OSA_ERROR("Fail to request layer\n");
        goto __error_close_dsp_dev;
	}
	OSA_printf("video layer id:%d\n", reqLayerId);

	/**! Assin layer 0 */
	gSysDisp.layerId[DISP_LAYER_0] = reqLayerId;
	

	gSysDisp.layerParam[DISP_LAYER_0].mode             = DISP_LAYER_WORK_MODE_SCALER; 
	gSysDisp.layerParam[DISP_LAYER_1].prio             = DISP_LAYER_0; //<< Priority 0
	gSysDisp.layerParam[DISP_LAYER_0].pipe             = 0; 
	gSysDisp.layerParam[DISP_LAYER_0].fb.addr[0]       = 0;
	gSysDisp.layerParam[DISP_LAYER_0].fb.addr[1]       = 0;
	gSysDisp.layerParam[DISP_LAYER_0].fb.addr[2]       = 0; 
	gSysDisp.layerParam[DISP_LAYER_0].fb.size.width    = cfg[DISP_CH0].canvas.nWidth;
	gSysDisp.layerParam[DISP_LAYER_0].fb.size.height   = cfg[DISP_CH0].canvas.nHeight;
	gSysDisp.layerParam[DISP_LAYER_0].fb.mode          = gSysDisp.fb_mode[DISP_LAYER_0];
	gSysDisp.layerParam[DISP_LAYER_0].fb.format        = gSysDisp.fb_fmt[DISP_LAYER_0];
	gSysDisp.layerParam[DISP_LAYER_0].fb.br_swap       = 0;
	gSysDisp.layerParam[DISP_LAYER_0].fb.seq           = gSysDisp.fb_seq[DISP_LAYER_0];
	gSysDisp.layerParam[DISP_LAYER_0].ck_enable        = 0;
	gSysDisp.layerParam[DISP_LAYER_0].alpha_en         = 1; 
	gSysDisp.layerParam[DISP_LAYER_0].alpha_val        = 0xFF;
	gSysDisp.layerParam[DISP_LAYER_0].src_win.x        = 0;
	gSysDisp.layerParam[DISP_LAYER_0].src_win.y        = 0;
	gSysDisp.layerParam[DISP_LAYER_0].src_win.width    = cfg[DISP_CH0].canvas.nWidth;
	gSysDisp.layerParam[DISP_LAYER_0].src_win.height   = cfg[DISP_CH0].canvas.nHeight;
	gSysDisp.layerParam[DISP_LAYER_0].scn_win.x        = 0;
	gSysDisp.layerParam[DISP_LAYER_0].scn_win.y        = 0;
	gSysDisp.layerParam[DISP_LAYER_0].scn_win.width    = cfg[DISP_CH0].canvas.nWidth;
	gSysDisp.layerParam[DISP_LAYER_0].scn_win.height   = cfg[DISP_CH0].canvas.nHeight;
    
	disp_param[0] = SCREEN_0;
    disp_param[1] = gSysDisp.layerId[DISP_LAYER_0];
    disp_param[2] = (__u32)&gSysDisp.layerParam[DISP_LAYER_0];
    ioctl(gSysDisp.fd_dsp,DISP_CMD_LAYER_SET_PARA,(void*)disp_param);
    ioctl(gSysDisp.fd_dsp,DISP_CMD_LAYER_OPEN,(void*)disp_param);
    
	#if 1
	/* Set as registered fb layer to BOTTOM */
	ioctl(gSysDisp.fd_dsp, DISP_CMD_LAYER_BOTTOM, (void *)disp_param);
	#endif


	
	/* Initialize Frame buffer device driver */
	gSysDisp.fd_fb[DISP_CH0][DISP_LAYER_0] = open(FB0_DEVICE, O_RDWR);
	#if 0
	if (ioctl(gSysDisp.fd_fb[DISP_CH0][DISP_LAYER_0], FBIOGET_LAYER_HDL_0, &reqLayerId) == -1) {
		OSA_ERROR("get fb(%s) layer handel\n",FB0_DEVICE);	
		goto __error_close_layer_dev;
	}
	#endif
	
	/* Save frame buffer's length */
	gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_0].length = cfg[DISP_CH0].canvas.nWidth*cfg[DISP_CH0].canvas.nHeight*3;
	
	#if 0
	fb_tmpBuf = OSA_memAlloc(gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_0].length);
	memset(fb_tmpBuf, 0xff, gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_0].length);
	write(gSysDisp.fd_fb[DISP_CH0][DISP_LAYER_0], fb_tmpBuf, gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_0].length);

	OSA_printf("fb_layer hdl: %ld\n", fb_layer);
	
	if(fb_tmpBuf){
		OSA_memFree(fb_tmpBuf);
	}
	#endif
	
	
	





	/* Reset layer id for requesting new layer */
	reqLayerId = 0;

	/* Request New Layer */
	disp_param[0] = SCREEN_0;
    disp_param[1] = DISP_LAYER_WORK_MODE_SCALER;
    reqLayerId = ioctl(gSysDisp.fd_dsp, DISP_CMD_LAYER_REQUEST, (void*)disp_param);
    if (reqLayerId == 0) 
	{
		OSA_ERROR("Fail to request layer\n");
        goto __error_close_layer_dev;
	}
	OSA_printf("video layer id:%d\n", reqLayerId);

	/**! Assin layer 1 */
	gSysDisp.layerId[DISP_LAYER_1] = reqLayerId;

	gSysDisp.layerParam[DISP_LAYER_1].mode             = DISP_LAYER_WORK_MODE_SCALER; 
	gSysDisp.layerParam[DISP_LAYER_1].prio             = DISP_LAYER_1; //<< Priority 1
	gSysDisp.layerParam[DISP_LAYER_1].pipe             = 0; 
	gSysDisp.layerParam[DISP_LAYER_1].fb.addr[0]       = 0;
	gSysDisp.layerParam[DISP_LAYER_1].fb.addr[1]       = 0;
	gSysDisp.layerParam[DISP_LAYER_1].fb.addr[2]       = 0; 
	gSysDisp.layerParam[DISP_LAYER_1].fb.size.width    = SCREEN_720P_WIDTH;//cfg[DISP_CH0].canvas.nWidth;
	gSysDisp.layerParam[DISP_LAYER_1].fb.size.height   = SCREEN_720P_HEIGHT;//cfg[DISP_CH0].canvas.nHeight;
	gSysDisp.layerParam[DISP_LAYER_1].fb.mode          = gSysDisp.fb_mode[DISP_LAYER_1];
	gSysDisp.layerParam[DISP_LAYER_1].fb.format        = gSysDisp.fb_fmt[DISP_LAYER_1];
	gSysDisp.layerParam[DISP_LAYER_1].fb.br_swap       = 0;
	gSysDisp.layerParam[DISP_LAYER_1].fb.seq           = gSysDisp.fb_seq[DISP_LAYER_1];
	gSysDisp.layerParam[DISP_LAYER_1].ck_enable        = 1;
	gSysDisp.layerParam[DISP_LAYER_1].alpha_en         = 1; 
	gSysDisp.layerParam[DISP_LAYER_1].alpha_val        = 0x77;
	gSysDisp.layerParam[DISP_LAYER_1].src_win.x        = 0;
	gSysDisp.layerParam[DISP_LAYER_1].src_win.y        = 0;
	gSysDisp.layerParam[DISP_LAYER_1].src_win.width    = SCREEN_720P_WIDTH;//cfg[DISP_CH0].canvas.nWidth;
	gSysDisp.layerParam[DISP_LAYER_1].src_win.height   = SCREEN_720P_HEIGHT;//cfg[DISP_CH0].canvas.nHeight;
	gSysDisp.layerParam[DISP_LAYER_1].scn_win.x        = 0;
	gSysDisp.layerParam[DISP_LAYER_1].scn_win.y        = 0;
	gSysDisp.layerParam[DISP_LAYER_1].scn_win.width    = SCREEN_720P_WIDTH;//cfg[DISP_CH0].canvas.nWidth/2;
	gSysDisp.layerParam[DISP_LAYER_1].scn_win.height   = SCREEN_720P_HEIGHT;//cfg[DISP_CH0].canvas.nHeight/2;
    
	disp_param[0] = SCREEN_0;
    disp_param[1] = gSysDisp.layerId[DISP_LAYER_1];
    disp_param[2] = (__u32)&gSysDisp.layerParam[DISP_LAYER_1];
    ioctl(gSysDisp.fd_dsp,DISP_CMD_LAYER_SET_PARA,(void*)disp_param);
    ioctl(gSysDisp.fd_dsp,DISP_CMD_LAYER_OPEN,(void*)disp_param);
    
	#if 1
	/* Set as registered fb layer to TOP */
	ioctl(gSysDisp.fd_dsp, DISP_CMD_LAYER_TOP, (void *)disp_param);
	#endif
    

    {
		__disp_colorkey_t clrKeyParam;
		memset(&clrKeyParam, 0, sizeof(__disp_colorkey_t));
		clrKeyParam.ck_min.alpha = 0x00;
		clrKeyParam.ck_min.red = 0x00;
		clrKeyParam.ck_min.green = 0x00;
		clrKeyParam.ck_min.blue = 0x00;

		clrKeyParam.ck_max.alpha = 0xFF;
		clrKeyParam.ck_max.red = 0xFF;
		clrKeyParam.ck_max.green = 0xFF;
		clrKeyParam.ck_max.blue = 0xFF;

		clrKeyParam.red_match_rule = 2;
		clrKeyParam.green_match_rule = 2;
		clrKeyParam.blue_match_rule = 2;

		disp_param[0] = SCREEN_0;
		disp_param[1] = (unsigned long)&clrKeyParam;
		if( ioctl(gSysDisp.fd_dsp, DISP_CMD_SET_COLORKEY, (void*)disp_param)<0 )
		{
			OSA_ERROR("DISP_CMD_SET_COLORKEY Error\n");
		}
		OSA_printf("DISP_CMD_SET_COLORKEY Done\n");

		
		disp_param[0] = SCREEN_0;
		disp_param[1] = gSysDisp.layerId[DISP_LAYER_1];
		if( ioctl(gSysDisp.fd_dsp, DISP_CMD_LAYER_CK_ON, (void*)disp_param)<0 )
		{
			OSA_ERROR("DISP_CMD_LAYER_CK_ON Error\n");
		}
		OSA_printf("DISP_CMD_LAYER_CK_ON Done\n");

		#if 0
		if( ioctl(gSysDisp.fd_dsp, DISP_CMD_LAYER_ALPHA_ON, (void*)disp_param)<0 )
		{
			OSA_ERROR("DISP_CMD_LAYER_ALPHA_ON Error\n");
		}
		OSA_printf("DISP_CMD_LAYER_ALPHA_ON Done\n");
		#endif
		
	}

	/* Initialize Frame buffer device driver */
	gSysDisp.fd_fb[DISP_CH0][DISP_LAYER_1] = open(FB1_DEVICE, O_RDWR);
	#if 0
	if (ioctl(gSysDisp.fd_fb[DISP_CH0][DISP_LAYER_1], FBIOGET_LAYER_HDL_0, &reqLayerId) == -1) {
		OSA_ERROR("get fb(%s) layer handel\n",FB1_DEVICE);	
		goto __error_close_layer_dev;
	}
	#endif

	


	/* Physical memory allocation */
	phyFreeMemSize = ioctl(gSysDisp.fd_phymem, SUNXI_MEM_GET_REST_SZ, 0);
	OSA_printf("Available memory size[Before]: %d Mbyte\n", (phyFreeMemSize/(1024*1024)));

	/* Save frame buffer's length */
	//gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].length= (cfg[DISP_CH0].canvas.nWidth*cfg[DISP_CH0].canvas.nHeight)<<2;
	gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].length= (SCREEN_720P_WIDTH*SCREEN_720P_HEIGHT)<<2;

	/* Allocate phyical memory */
	if( phyFreeMemSize<gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].length ){
		OSA_ERROR("Not enough memory\n");
		goto __error_close_layer_dev;
	}else{
		gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].phyAddr = ioctl(gSysDisp.fd_phymem, 
																	SUNXI_MEM_ALLOC, 
																	&gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].length);
		if (0 == gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].phyAddr)
		{
			OSA_ERROR("can not request physical buffer, size: %d", 
						gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].length);
			goto __error_close_layer_dev;
		}

		gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].virAddr = (int)mmap(NULL, 
																	gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].length*3, 
																	PROT_READ | PROT_WRITE, MAP_SHARED, 
																	gSysDisp.fd_phymem, 
																	(int)gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].phyAddr);
		if ((int)MAP_FAILED == gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].virAddr)
		{
			ioctl(gSysDisp.fd_phymem, SUNXI_MEM_FREE, &gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].phyAddr);
			goto __error_close_layer_dev;
		}
		gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].isValid = TRUE;

		phyFreeMemSize = ioctl(gSysDisp.fd_phymem, SUNXI_MEM_GET_REST_SZ, 0);
		OSA_printf("Available memory size[After]: %d Mbyte\n", (phyFreeMemSize/(1024*1024)));
	}



	#if 0
	{
		__disp_video_fb_t  fb_addr;
		memset(&fb_addr, 0, sizeof(__disp_video_fb_t));
		fb_addr.interlace       = 0;
		fb_addr.top_field_first = 0;
		fb_addr.frame_rate      = 30;
		fb_addr.addr[0] = gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].phyAddr;

		fb_addr.id = 1;  //TODO

		disp_param[0] = SCREEN_0;
		disp_param[1] = gSysDisp.layerId[DISP_LAYER_1];
		disp_param[2] = (__u32)&fb_addr;
		ioctl(gSysDisp.fd_dsp, DISP_CMD_VIDEO_SET_FB, (void*)disp_param);

		memset(gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].virAddr,0x55,gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].length);
		{
			int i=0,j=0;
			unsigned int *p = NULL;

			#if 0
			for (i = 0; i < cfg[DISP_CH0].canvas.nHeight; i++)
		    {
		        p = (unsigned int *)(gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].virAddr + cfg[DISP_CH0].canvas.nWidth* i * 4);
		        for (j = 0; j < cfg[DISP_CH0].canvas.nWidth; j++)
		        {
		            int idx = (j << 3) / cfg[DISP_CH0].canvas.nWidth;
		            *p++ = colorbar[idx];
		        }
		    }
		    #else
		    for (i = 0; i < SCREEN_720P_HEIGHT; i++)
		    {
		        p = (unsigned int *)(gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].virAddr + SCREEN_720P_WIDTH* i * 4);
		        for (j = 0; j < SCREEN_720P_WIDTH; j++)
		        {
		            int idx = (j << 3) / SCREEN_720P_WIDTH;
		            *p++ = colorbar[idx];
		        }
		    }
		    #endif
	    }

		pmem_param[0] = (long)gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].virAddr;
		pmem_param[1] = pmem_param[0]+(gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].length<<2)-1;
	    //ioctl(gSysDisp.fd_phymem, SUNXI_MEM_FLUSH_CACHE, pmem_param);
	}
	#else
	{
		__disp_fb_t fbParam;
		memset(&fbParam, 0, sizeof(__disp_fb_t));
		fbParam.addr[0] = gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].phyAddr;
		fbParam.addr[1] = 0;
		fbParam.addr[2] = 0;
		fbParam.size.width = SCREEN_720P_WIDTH;//cfg[DISP_CH0].canvas.nWidth;
		fbParam.size.height = SCREEN_720P_HEIGHT;//cfg[DISP_CH0].canvas.nHeight;
		fbParam.format = gSysDisp.fb_fmt[DISP_LAYER_1];
		fbParam.seq = gSysDisp.fb_seq[DISP_LAYER_1];
		fbParam.mode = gSysDisp.fb_mode[DISP_LAYER_1];
		fbParam.cs_mode = DISP_BT601;

		disp_param[0] = SCREEN_0;
		disp_param[1] = gSysDisp.layerId[DISP_LAYER_1];
		disp_param[2] = (__u32)&fbParam;
		if( ioctl(gSysDisp.fd_dsp, DISP_CMD_LAYER_SET_FB, (void*)disp_param)<0 )
		{
			OSA_ERROR("DISP_CMD_VIDEO_SET_FB Error\n");
		}

		memset(gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].virAddr,0xFF,gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].length);

		{
			int i=0,j=0;
			unsigned int *p = NULL;

			#if 0
			for (i = 0; i < cfg[DISP_CH0].canvas.nHeight; i++)
		    {
		        p = (unsigned int *)(gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].virAddr + cfg[DISP_CH0].canvas.nWidth* i * 4);
		        for (j = 0; j < cfg[DISP_CH0].canvas.nWidth; j++)
		        {
		            int idx = (j << 3) / cfg[DISP_CH0].canvas.nWidth;
		            *p++ = colorbar[idx];
		        }
		    }
		    #else
		    for (i = 0; i < SCREEN_720P_HEIGHT; i++)
		    {
		        p = (unsigned int *)(gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].virAddr + SCREEN_720P_WIDTH* i * 4);
		        for (j = 0; j < SCREEN_720P_WIDTH; j++)
		        {
		            int idx = (j << 3) / SCREEN_720P_WIDTH;
		            *p++ = colorbar[idx];
		        }
		    }
		    #endif
	    }

		#if 1
		pmem_param[0] = (long)gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].virAddr;
		pmem_param[1] = pmem_param[0]+(gSysDisp.fb_bufPool[DISP_CH0][DISP_LAYER_1].length<<2)-1;
	    ioctl(gSysDisp.fd_phymem, SUNXI_MEM_FLUSH_CACHE, pmem_param);
	    #else
	    ioctl(gSysDisp.fd_phymem, SUNXI_MEM_FLUSH_CACHE_ALL, 0);
	    #endif
	}
	#endif


	




	

	#if 0
	gSysDisp.fd_fb[DISP_CH0][DISP_LAYER_1] = open(FB1_DEVICE, O_RDWR);
	if (ioctl(gSysDisp.fd_fb[DISP_CH0][DISP_LAYER_1], FBIOGET_LAYER_HDL_0, &fb_layer) == -1) {
		OSA_ERROR("get fb(%s) layer handel\n",FB1_DEVICE);	
		goto __error_close_layer_dev;
	}
	OSA_printf("fb_layer hdl: %ld\n", fb_layer);

	
	
	/* Save frame buffer's length */
	gSysDisp.fb_len[DISP_CH0][DISP_LAYER_1] = (cfg[DISP_CH0].canvas.nWidth*cfg[DISP_CH0].canvas.nHeight)<<2;

	/* Request Frame buffers */
	gSysDisp.fb_buffer[DISP_CH0][DISP_LAYER_1] = mmap( NULL, 
														gSysDisp.fb_len[DISP_CH0][DISP_LAYER_1]*3, 
														PROT_READ|PROT_WRITE, MAP_SHARED, 
														gSysDisp.fd_fb[DISP_CH0][DISP_LAYER_1], 
														0);
	memset(gSysDisp.fb_buffer[DISP_CH0][DISP_LAYER_1],0x00,gSysDisp.fb_len[DISP_CH0][DISP_LAYER_1]*3);
	#endif					
    



	OSA_printf("Display init: Done\n");
	
	return OSA_SOK;









__error_close_layer_dev:
	for (lIdx=0; lIdx<DISP_LAYER_MAXCNT; lIdx++)
	{
		if( gSysDisp.layerId[lIdx]>0 )
		{
		    disp_param[0] = SCREEN_0;
		    disp_param[1] = gSysDisp.layerId[lIdx];
		    ioctl(gSysDisp.fd_dsp, DISP_CMD_LAYER_CLOSE,  (void*)disp_param);

		    disp_param[0] = SCREEN_0;
		    disp_param[1] = gSysDisp.layerId[lIdx];
		    ioctl(gSysDisp.fd_dsp, DISP_CMD_LAYER_RELEASE,  (void*)disp_param);
	    }
    }

	for (chIdx=0; chIdx<DISP_CH_MAXCNT; chIdx++)
	{
		for (lIdx=0; lIdx<DISP_LAYER_MAXCNT; lIdx++)
		{
			if(gSysDisp.fd_fb[chIdx][lIdx])
			{
				if( gSysDisp.fb_bufPool[chIdx][lIdx].isValid == TRUE ){
					ioctl(gSysDisp.fd_phymem, SUNXI_MEM_FREE, &gSysDisp.fb_bufPool[chIdx][lIdx].phyAddr);
					munmap(gSysDisp.fb_bufPool[chIdx][lIdx].virAddr, gSysDisp.fb_bufPool[chIdx][lIdx].length);
				}
				
				close(gSysDisp.fd_fb[chIdx][lIdx]);
			}
		}
	}




__error_close_dsp_dev:
	/* Close display device driver */
	if(gSysDisp.fd_dsp){
		close(gSysDisp.fd_dsp);
		gSysDisp.fd_dsp = -1;
	}

	/* Close physical memory device driver */
	if(gSysDisp.fd_phymem>0){
		close(gSysDisp.fd_phymem);
	}
	return OSA_EFAIL;
}




int Display_deinit(void)
{
	int ret=0;
	int chIdx=0, lIdx=0;
	unsigned int disp_param[4]={0,};

	if(gSysDisp.if_type[DISP_CH0] == DISP_OIF_HDMI)
	{
		disp_param[0] = 0;
	    ret=ioctl(gSysDisp.fd_dsp, DISP_CMD_HDMI_OFF, (void*)disp_param);
    }

	for (lIdx=0; lIdx<DISP_LAYER_MAXCNT; lIdx++)
	{
		if( gSysDisp.layerId[lIdx]>0 )
		{
		    disp_param[0] = SCREEN_0;
		    disp_param[1] = gSysDisp.layerId[lIdx];
		    ret|=ioctl(gSysDisp.fd_dsp, DISP_CMD_LAYER_CLOSE,  (void*)disp_param);

		    disp_param[0] = SCREEN_0;
		    disp_param[1] = gSysDisp.layerId[lIdx];
		    ret|=ioctl(gSysDisp.fd_dsp, DISP_CMD_LAYER_RELEASE,  (void*)disp_param);
	    }
    }

	for (chIdx=0; chIdx<DISP_CH_MAXCNT; chIdx++)
	{
		for (lIdx=0; lIdx<DISP_LAYER_MAXCNT; lIdx++)
		{
			if(gSysDisp.fd_fb[chIdx][lIdx])
			{
				if( gSysDisp.fb_bufPool[chIdx][lIdx].isValid == TRUE ){
					ioctl(gSysDisp.fd_phymem, SUNXI_MEM_FREE, &gSysDisp.fb_bufPool[chIdx][lIdx].phyAddr);
					munmap(gSysDisp.fb_bufPool[chIdx][lIdx].virAddr, gSysDisp.fb_bufPool[chIdx][lIdx].length);
				}
				close(gSysDisp.fd_fb[chIdx][lIdx]);
			}
		}
	}

    /* Close display device driver */
    close (gSysDisp.fd_dsp);


	/* Close physical memory device driver */
    if(gSysDisp.fd_phymem>0){
		close(gSysDisp.fd_phymem);
	}

    OSA_printf("Display deinit: Done\n");

	return OSA_SOK;
}


void disp_start(void)
{
	int lIdx=0;
	unsigned int disp_param[4]={0,};

	for (lIdx=0; lIdx<DISP_LAYER_MAXCNT; lIdx++)
	{
		if( gSysDisp.layerId[lIdx]>0 )
		{
			disp_param[0] = SCREEN_0;
		    disp_param[1] = gSysDisp.layerId[lIdx];
		    ioctl(gSysDisp.fd_dsp, DISP_CMD_VIDEO_START,  (void*)disp_param);
		}
	}
}

void disp_stop(void)
{
	int lIdx=0;
	unsigned int disp_param[4]={0,};

	for (lIdx=0; lIdx<DISP_LAYER_MAXCNT; lIdx++)
	{
		if( gSysDisp.layerId[lIdx]>0 )
		{
			disp_param[0] = SCREEN_0;
		    disp_param[1] = gSysDisp.layerId[lIdx];
		    ioctl(gSysDisp.fd_dsp, DISP_CMD_VIDEO_STOP,  (void*)disp_param);
		}
	}
}



int disp_set_video_buffer(int pixFmt, int w,int h,int *phyAddr)
{
	unsigned int disp_param[4]={0,};

	__disp_video_fb_t  fb_addr;
	memset(&fb_addr, 0, sizeof(__disp_video_fb_t));

	fb_addr.interlace       = 0;
	fb_addr.top_field_first = 0;
	fb_addr.frame_rate      = 30;
	fb_addr.addr[0] = *phyAddr;


	switch(pixFmt){
		case V4L2_PIX_FMT_YUV422P:
		case V4L2_PIX_FMT_YUYV:
		case V4L2_PIX_FMT_YVYU:
		case V4L2_PIX_FMT_UYVY:
		case V4L2_PIX_FMT_VYUY:
			fb_addr.addr[1]       = *phyAddr+w*h; //your C address,modify this
			fb_addr.addr[2]       = *phyAddr+w*h*3/2;
			break;
		case V4L2_PIX_FMT_YVU420:
		case V4L2_PIX_FMT_YUV420:
			fb_addr.addr[1]       = *phyAddr+w*h; //your C address,modify this
			fb_addr.addr[2]       = *phyAddr+w*h;
			break;
		case V4L2_PIX_FMT_NV16:
		case V4L2_PIX_FMT_NV21:
		case V4L2_PIX_FMT_NV12:
		case V4L2_PIX_FMT_HM12:
			fb_addr.addr[1]       = *phyAddr+w*h; //your C address,modify this
			fb_addr.addr[2]       = gSysDisp.layerParam[DISP_LAYER_0].fb.addr[1];
			break;
		default:
			OSA_printf("gDisp_Format is not found!\n");
			break;
	}

	fb_addr.id = 0;  //TODO

	disp_param[0] = SCREEN_0;
	disp_param[1] = gSysDisp.layerId[DISP_LAYER_0];
	disp_param[2] = (__u32)&fb_addr;
	return ioctl(gSysDisp.fd_dsp, DISP_CMD_VIDEO_SET_FB, (void*)disp_param);
}



int disp_hdmi_on(void)
{
	unsigned int disp_param[4]={0,};
	int ret=0;

    /* set hdmi on */
	disp_param[0] = SCREEN_0;
	ret = ioctl(gSysDisp.fd_dsp, DISP_CMD_HDMI_ON, disp_param);
	if (ret < 0) {
		OSA_ERROR("hdmitester: set hdmi on failed(%d)\n", ret);
	}

	return ret;
}




int Display_thread_ch0(void* pParam)
{	
	int status = OSA_EFAIL;
	int nCapBufIdx = 0;
	OSA_TskHndl* pTaskHndl = (OSA_TskHndl*)pParam;


	/*< DISPLAY ON */
	disp_hdmi_on();
	OSA_printf("DISPLAY HDMI on\n\n");

	disp_start();
	OSA_printf("DISPLAY start\n\n");

	


	while( gSysDisp.exitThrDsp==FALSE )
	{
		int dspBufId=0;
		OSA_BufInfo *pBufInfo=NULL;
		
		if(pTaskHndl->buffer[0]==NULL){
			OSA_printf("Buffer is Invalid....\n");
		}else{
			if( OSA_bufGetFull(pTaskHndl->buffer[0], &dspBufId, OSA_TIMEOUT_NONE) == OSA_SOK )
			{
				pBufInfo = OSA_bufGetBufInfo(pTaskHndl->buffer[0], dspBufId);
				//OSA_printf("pBufInfo->physAddr=0x%x pBufInfo->virtAddr=0x%x ts=%lld\n",pBufInfo->physAddr, pBufInfo->virtAddr, pBufInfo->timestamp);

				disp_set_video_buffer(pBufInfo->pixFmt,pBufInfo->width,pBufInfo->height,pBufInfo->physAddr);

				OSA_bufPutEmpty(pTaskHndl->buffer[0], dspBufId);
			}
		}
		OSA_waitMsecs(1);
	}



	/*< DISPLAY STOP */
	disp_stop();



	OSA_printf("Display Finish...\n");

	return OSA_SOK;
}





/**
 * @name    SignalTraps()
 * @brief   signal trap function
 * @ingroup Group_FunctionTest
 * @param: None
 *
 * @retval: None
 *
 * Example Usage:
 * @code
 *  SignalTraps();
 * @endcode
 */
int TaskDisplay( struct OSA_TskHndl *pPrc, OSA_MsgHndl *pMsg, Uint32 curState )
{
	int retVal = OSA_SOK;
	int status = OSA_SOK;
	int curStatus = -1;
	Uint16 cmd = NULL;
	Config * pCfg = (Config*)pPrc->pParamCfg;

	/**! Loop operation */
	if (pMsg==NULL)
	{
		//OSA_waitMsecs(1000);
		//OSA_printf("Task[Display] Loop Operation...\n");
	}
	else
	{
		cmd = OSA_msgGetCmd(pMsg);

		switch(cmd)
		{
			case TASK_CMD_READY:
				OSA_printf("[DISPLAY TASK] Get READY command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_READY);
				break;

			case TASK_CMD_INIT:
				OSA_printf("[DISPLAY TASK] Get INIT command\n");

				curStatus = OSA_tskGetState(pPrc);
				
				if( curStatus==TASK_STATUS_READY )
				{
					if( Display_Init(pCfg->display)!=OSA_SOK ){
						OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
						OSA_tskSetState(pPrc,TASK_STATUS_ERROR);
					}else{				
						OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
						OSA_tskSetState(pPrc,TASK_STATUS_INIT);
					}
				}
				else
				{
					OSA_ERROR("Invalid cur status\n");	
					OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
				}
				break;

			case TASK_CMD_DEINIT:
				OSA_printf("[DISPLAY TASK] Get DEINIT command\n");
				curStatus = OSA_tskGetState(pPrc);

				if( curStatus==TASK_STATUS_STOP )
				{
					Display_deinit();
					OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
					OSA_tskSetState(pPrc,TASK_STATUS_DEINIT);
				}
				else
				{
					OSA_ERROR("Invalid cur status\n");	
					OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
				}
				break;
				
			case TASK_CMD_RUN:
				OSA_printf("[DISPLAY TASK] Get RUN command\n");

				curStatus = OSA_tskGetState(pPrc);
				
				if( curStatus==TASK_STATUS_INIT )
				{
					status = OSA_thrCreate(&gSysDisp.thrDsp, Display_thread_ch0, 0, 1024, pPrc);
				    if(status==OSA_SOK) {
				    	OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
						OSA_tskSetState(pPrc,TASK_STATUS_RUN);
				    }else{
				    	OSA_ERROR("Fail to create display thread for ch0\n");
				    	OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
				    	OSA_tskSetState(pPrc,TASK_STATUS_ERROR);
				    }
			    }else{
			    	OSA_ERROR("Invalid cur status\n");	
			    	OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
			    }
				break;
				
			case TASK_CMD_STOP:
				OSA_printf("[DISPLAY TASK] Get STOP command\n");

				curStatus = OSA_tskGetState(pPrc);

				if( curStatus!=TASK_STATUS_INIT && 
					curStatus!=TASK_STATUS_DEINIT &&
					curStatus!=TASK_STATUS_READY)
				{
					gSysDisp.exitThrDsp=TRUE;

					OSA_thrDelete(&gSysDisp.thrDsp);

					OSA_printf("Display ch0 thread deleted\n");

					OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
					OSA_tskSetState(pPrc,TASK_STATUS_STOP);
				}
				else
				{
					OSA_ERROR("Invalid cur status\n");	
					OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
				}
				break;
				
			case TASK_CMD_RESUME:
				OSA_printf("[DISPLAY TASK] Get RESUME command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_RESUME);
				break;
				
			case TASK_CMD_PAUSE:
				OSA_printf("[DISPLAY TASK] Get PAUSE command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_PAUSE);
				break;
				
			case TASK_CMD_RESET:
				OSA_printf("[DISPLAY TASK] Get RESET command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_RESET);
				break;
				
			default:
				break;
		}
	}

	return retVal;
}




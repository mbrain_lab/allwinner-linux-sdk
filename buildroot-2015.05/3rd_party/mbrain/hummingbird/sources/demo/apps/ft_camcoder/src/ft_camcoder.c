/**
 * @file   ft_hdmi.c
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.07.16
 * @brief  HDMI funnction test for Hummingbird A31
 *
 * Detailed description of file.
 *
 * @copyright
 * (c) Copyright MBRAIN & mBrain(2015).  All Rights Reserved.
 */

/*! [ 1]. File Directive (#include) */
#include <osa.h>
#include "task.h"
#include "capture.h"
#include "display.h"
#include "encoder.h"
#include "ft_camcoder.h"



/*! [ 2]. Local Macro (#define, enum) */
#define TASK_STACK (1*MB)


/*! [ 3]. Type definition or structure declaration (typedef or struct) */

/*! [ 4]. Local Function prototype declaration */
static void SignalTraps(void);
static void SignalHandler(int sig);

/*! [ 5]. Local Variable declaration */
/*! [ 6]. Global Variable declaration */
Config	gAppConfig;
Ctrl	gAppCtrl;







/**
 * @name    SignalTraps()
 * @brief   signal trap function
 * @ingroup Group_FunctionTest
 * @param: None
 *
 * @retval: None
 *
 * Example Usage:
 * @code
 *  SignalTraps();
 * @endcode
 */
void SignalTraps(void)
{
	OSA_attachSignalHandler(SIGTERM, SignalHandler);
	OSA_attachSignalHandler(SIGINT,  SignalHandler);
}


/**
 * @name    SignalHandler()
 * @brief   Callback function for signal trap
 * @ingroup Group_FunctionTest
 * @param
 *  sig: signal id
 *
 * @retval: None
 *
 * Example Usage:
 * @code
 *  void SignalTraps(void)
 *  {
 *      OSA_attachSignalHandler(SIGTERM, SignalHandler);
 *      OSA_attachSignalHandler(SIGINT,  SignalHandler);
 *  }
 * @endcode
 */
void SignalHandler(int sig)
{
	if ( (sig==SIGTERM)||(sig==SIGINT) ){
		OSA_printf("SIGTERM or SIGINT was captured\n");
		gAppCtrl.isExit=TRUE;
	}

}







int Task_subTaskCreate(void)
{
	int ret=OSA_EFAIL;
	int tsk_idx=0;
	
	/* Create Task */
	for( tsk_idx=TASK_CAPTURE; tsk_idx<TASK_MAXCNT; tsk_idx++ ){
		gAppCtrl.task[tsk_idx].pParamCfg = &gAppConfig;
	}

	ret=OSA_tskCreate(&gAppCtrl.task[TASK_CAPTURE], TaskCapture, 0, TASK_STACK, TASK_STATUS_READY, 1000);
	if (ret != OSA_SOK){
		OSA_ERROR("Fail to create Capture Task\n");
		return ret;
	}
	
	OSA_tskCreate(&gAppCtrl.task[TASK_DISPLAY], TaskDisplay, 0, TASK_STACK, TASK_STATUS_READY, 1000);
	if (ret != OSA_SOK){
		OSA_ERROR("Fail to create Display Task\n");
		return ret;
	}
	
	OSA_tskCreate(&gAppCtrl.task[TASK_ENCODER], TaskEncoder, 0, TASK_STACK, TASK_STATUS_READY, 1000);
	if (ret != OSA_SOK){
		OSA_ERROR("Fail to create Encoder Task\n");
		return ret;
	}

	return ret;
}


int Task_subTaskDelete(void)
{
	OSA_tskDelete(&gAppCtrl.task[TASK_ENCODER]);
	OSA_tskDelete(&gAppCtrl.task[TASK_DISPLAY]);
	OSA_tskDelete(&gAppCtrl.task[TASK_CAPTURE]);

	return OSA_SOK;
}



int Task_subTaskSetReady(void)
{
	int ret=OSA_SOK;
	int status=0;
	
	status = OSA_tskGetState(&gAppCtrl.task[TASK_ENCODER]);
	if (status!=TASK_STATUS_READY && gAppCtrl.task[TASK_ENCODER].taskValid==TRUE){
		OSA_printf("ENCODER Task is valid but it isn't READY status\n");
		ret=OSA_mbxSendMsg( &gAppCtrl.task[TASK_ENCODER].mbxHndl,  &gAppCtrl.task[TASK_MAIN].mbxHndl, TASK_CMD_READY, NULL, OSA_MBX_WAIT_ACK);
	}

	status = OSA_tskGetState(&gAppCtrl.task[TASK_DISPLAY]);
	if (status!=TASK_STATUS_READY && gAppCtrl.task[TASK_DISPLAY].taskValid==TRUE){
		OSA_printf("DISPLAY Task is valid but it isn't READY status\n");
		ret|=OSA_mbxSendMsg( &gAppCtrl.task[TASK_DISPLAY].mbxHndl,  &gAppCtrl.task[TASK_MAIN].mbxHndl, TASK_CMD_READY, NULL, OSA_MBX_WAIT_ACK);
	}

	status = OSA_tskGetState(&gAppCtrl.task[TASK_CAPTURE]);
	if (status!=TASK_STATUS_READY && gAppCtrl.task[TASK_CAPTURE].taskValid==TRUE){
		OSA_printf("CAPTURE Task is valid but it isn't READY status\n");
		ret|=OSA_mbxSendMsg( &gAppCtrl.task[TASK_CAPTURE].mbxHndl,  &gAppCtrl.task[TASK_MAIN].mbxHndl, TASK_CMD_READY, NULL, OSA_MBX_WAIT_ACK);
	}
	return ret;
}


int Task_subTaskSetInit(void)
{
	int ret=OSA_SOK;
	int status=0;

	ret=OSA_mbxSendMsg( &gAppCtrl.task[TASK_CAPTURE].mbxHndl,  &gAppCtrl.task[TASK_MAIN].mbxHndl, TASK_CMD_INIT, NULL, OSA_MBX_WAIT_ACK);
	if( ret != OSA_SOK ){
		OSA_ERROR("Fail to init CAPTURE TASK\n");
	}

	gAppCtrl.task[TASK_DISPLAY].buffer[0] = gAppCtrl.task[TASK_CAPTURE].buffer[0];
	gAppCtrl.task[TASK_DISPLAY].buffer[1] = gAppCtrl.task[TASK_CAPTURE].buffer[1];
	

	ret=OSA_mbxSendMsg( &gAppCtrl.task[TASK_DISPLAY].mbxHndl,  &gAppCtrl.task[TASK_MAIN].mbxHndl, TASK_CMD_INIT, NULL, OSA_MBX_WAIT_ACK);
	if( ret != OSA_SOK ){
		OSA_ERROR("Fail to init DISPLAY TASK\n");
	}


	ret=OSA_mbxSendMsg( &gAppCtrl.task[TASK_ENCODER].mbxHndl,  &gAppCtrl.task[TASK_MAIN].mbxHndl, TASK_CMD_INIT, NULL, OSA_MBX_WAIT_ACK);
	if( ret != OSA_SOK ){
		OSA_ERROR("Fail to init ENCODER TASK\n");
	}
	
	return ret;
}



int Task_subTaskSetDeinit(void)
{
	int ret=OSA_SOK;
	int status=0;

	ret=OSA_mbxSendMsg( &gAppCtrl.task[TASK_ENCODER].mbxHndl,  &gAppCtrl.task[TASK_MAIN].mbxHndl, TASK_CMD_DEINIT, NULL, OSA_MBX_WAIT_ACK);
	if( ret != OSA_SOK ){
		OSA_ERROR("Fail to init ENCODER TASK\n");
	}

	ret=OSA_mbxSendMsg( &gAppCtrl.task[TASK_DISPLAY].mbxHndl,  &gAppCtrl.task[TASK_MAIN].mbxHndl, TASK_CMD_DEINIT, NULL, OSA_MBX_WAIT_ACK);
	if( ret != OSA_SOK ){
		OSA_ERROR("Fail to init DISPLAY TASK\n");
	}

	ret=OSA_mbxSendMsg( &gAppCtrl.task[TASK_CAPTURE].mbxHndl,  &gAppCtrl.task[TASK_MAIN].mbxHndl, TASK_CMD_DEINIT, NULL, OSA_MBX_WAIT_ACK);
	if( ret != OSA_SOK ){
		OSA_ERROR("Fail to init CAPTURE TASK\n");
	}

	gAppCtrl.task[TASK_DISPLAY].buffer[0] = gAppCtrl.task[TASK_CAPTURE].buffer[0] = NULL;
	gAppCtrl.task[TASK_DISPLAY].buffer[1] = gAppCtrl.task[TASK_CAPTURE].buffer[1] = NULL;
	
	return ret;
}


int Task_subTaskSetRun(void)
{
	int ret=OSA_SOK;
	int status=0;

	ret=OSA_mbxSendMsg( &gAppCtrl.task[TASK_CAPTURE].mbxHndl,  &gAppCtrl.task[TASK_MAIN].mbxHndl, TASK_CMD_RUN, NULL, OSA_MBX_WAIT_ACK);
	if( ret != OSA_SOK ){
		OSA_ERROR("Fail to init CAPTURE TASK\n");
	}
	
	ret=OSA_mbxSendMsg( &gAppCtrl.task[TASK_DISPLAY].mbxHndl,  &gAppCtrl.task[TASK_MAIN].mbxHndl, TASK_CMD_RUN, NULL, OSA_MBX_WAIT_ACK);
	if( ret != OSA_SOK ){
		OSA_ERROR("Fail to init DISPLAY TASK\n");
	}


	ret=OSA_mbxSendMsg( &gAppCtrl.task[TASK_ENCODER].mbxHndl,  &gAppCtrl.task[TASK_MAIN].mbxHndl, TASK_CMD_RUN, NULL, OSA_MBX_WAIT_ACK);
	if( ret != OSA_SOK ){
		OSA_ERROR("Fail to init ENCODER TASK\n");
	}
	
	return ret;
}


int Task_subTaskSetStop(void)
{
	int ret=OSA_SOK;
	int status=0;

	ret=OSA_mbxSendMsg( &gAppCtrl.task[TASK_ENCODER].mbxHndl,  &gAppCtrl.task[TASK_MAIN].mbxHndl, TASK_CMD_STOP, NULL, OSA_MBX_WAIT_ACK);
	if( ret != OSA_SOK ){
		OSA_ERROR("Fail to init ENCODER TASK\n");
	}

	ret=OSA_mbxSendMsg( &gAppCtrl.task[TASK_DISPLAY].mbxHndl,  &gAppCtrl.task[TASK_MAIN].mbxHndl, TASK_CMD_STOP, NULL, OSA_MBX_WAIT_ACK);
	if( ret != OSA_SOK ){
		OSA_ERROR("Fail to init DISPLAY TASK\n");
	}

	ret=OSA_mbxSendMsg( &gAppCtrl.task[TASK_CAPTURE].mbxHndl,  &gAppCtrl.task[TASK_MAIN].mbxHndl, TASK_CMD_STOP, NULL, OSA_MBX_WAIT_ACK);
	if( ret != OSA_SOK ){
		OSA_ERROR("Fail to init CAPTURE TASK\n");
	}

	return ret;
}





/**
 * @name    Task_Main()
 * @brief   Main Task 
 * @ingroup Group_FunctionTest
 * @param: None
 *
 * @retval: None
 *
 * Example Usage:
 * @code
 *  SignalTraps();
 * @endcode
 */
int Task_Main( struct OSA_TskHndl *pPrc, OSA_MsgHndl *pMsg, Uint32 curState )
{
	int retVal = OSA_SOK;
	Uint16 cmd = 0;
	int curStatus = 0;

	/**! Loop operation */
	if (pMsg==NULL)
	{
		OSA_waitMsecs(10);
	}
	else
	{
		cmd = OSA_msgGetCmd(pMsg);

		switch(cmd)
		{
			case TASK_CMD_READY:
				OSA_printf("Get READY command\n");
				if(Task_subTaskSetReady()!=OSA_SOK){
					OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
					OSA_tskSetState(pPrc,TASK_STATUS_ERROR);
				}else{
					OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
					OSA_tskSetState(pPrc,TASK_STATUS_READY);
				}
				break;
				
			case TASK_CMD_CREATE:
				OSA_printf("Get CREATE command\n");
				if( Task_subTaskCreate()!=OSA_SOK ){
					OSA_ERROR("Sub task creation fail\n");
					OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
					OSA_tskSetState(pPrc,TASK_STATUS_ERROR);
				}else{
					OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
					OSA_tskSetState(pPrc,TASK_STATUS_READY);
				}
				break;
				
			case TASK_CMD_DELETE:
				OSA_printf("Get DELETE command\n");
				Task_subTaskDelete();
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_READY);
				break;
				
			case TASK_CMD_INIT:
				OSA_printf("Get INIT command\n");

				curStatus = OSA_tskGetState(pPrc);

				if( curStatus==TASK_STATUS_READY )
				{
					if(Task_subTaskSetInit()!=OSA_SOK){
						OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
						OSA_ERROR("Task_subTaskSetInit Fail\n");
						OSA_tskSetState(pPrc,TASK_STATUS_ERROR);
					}else{
						OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
						OSA_tskSetState(pPrc,TASK_STATUS_INIT);
					}
				}
				else
				{
					OSA_ERROR("Invalid cur status\n");	
					OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
				}
				break;

			case TASK_CMD_DEINIT:
				OSA_printf("Get DEINIT command\n");

				curStatus = OSA_tskGetState(pPrc);

				if( curStatus==TASK_STATUS_STOP )
				{				
					Task_subTaskSetDeinit();
					OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
					OSA_tskSetState(pPrc,TASK_STATUS_DEINIT);
				}
				else
				{
					OSA_ERROR("Invalid cur status\n");	
					OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
				}
				break;
				
			case TASK_CMD_RUN:
				OSA_printf("Get RUN command\n");

				curStatus = OSA_tskGetState(pPrc);

				if( curStatus==TASK_STATUS_INIT )
				{
					if(Task_subTaskSetRun()!=OSA_SOK){
						OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
						OSA_tskSetState(pPrc,TASK_STATUS_ERROR);
					}else{
						OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
						OSA_tskSetState(pPrc,TASK_STATUS_RUN);
					}
				}
				else
				{
					OSA_ERROR("Invalid cur status\n");	
					OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
				}
				break;

			case TASK_CMD_STOP:
				OSA_printf("Get STOP command\n");
				curStatus = OSA_tskGetState(pPrc);

				if( curStatus!=TASK_STATUS_READY && 
					curStatus!=TASK_STATUS_INIT &&
					curStatus!=TASK_STATUS_DEINIT &&
					curStatus!=TASK_STATUS_STOP)
				{	
					Task_subTaskSetStop();
					OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
					OSA_tskSetState(pPrc,TASK_STATUS_STOP);
				}
				else
				{
					OSA_ERROR("Invalid cur status\n");	
					OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
				}
				
				break;
				
			case TASK_CMD_RESUME:
				OSA_printf("Get RESUME command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_RESUME);
				break;
				
			case TASK_CMD_PAUSE:
				OSA_printf("Get PAUSE command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_PAUSE);
				break;
				
			case TASK_CMD_RESET:
				OSA_printf("Get RESET command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_RESET);
				break;
				
			default:
				OSA_printf("Get Unknown command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				break;
		}
	}

	return retVal;
}



/**
 * @name    main()
 * @brief   main function
 * @ingroup Group_FunctionTest
 * @param <Reserved>
 *
 * @retval <Reserved>
 *
 * Example Usage:
 * @code
 *  <reserved>
 * @endcode
 */
int main(int argc, char **argv)
{
	int ack=OSA_SOK;
	char cmd=0;
	Uint32 prm = 0;
	
	/* Init OSA library */
	OSA_init();

	/* Init global variable */
	memset(&gAppConfig,0x00,sizeof(Config));
	memset(&gAppCtrl,0x00,sizeof(Ctrl));
	
	gAppCtrl.isExit = FALSE;
	gAppCtrl.task_Syncker = OSA_memAlloc(sizeof(Rndez_Obj));
	

	/* Register signal */
	SignalTraps();

	OSA_printf("[%s] Start\n",__FILE__);

	/* Configuration */
	snprintf(gAppConfig.camera[CAM_CH0].szName,SZ_DESC_MAX,"CAM_CH0");
	gAppConfig.camera[CAM_CH0].isp_fmt = V4L2_PIX_FMT_NV12;
	gAppConfig.camera[CAM_CH0].sen_fmt = V4L2_PIX_FMT_SRGGB12;
	
	gAppConfig.camera[CAM_CH0].kBuf_cnt = CAM_CH0_KBUFCNT;
	gAppConfig.camera[CAM_CH0].win.nSX = 0;
	gAppConfig.camera[CAM_CH0].win.nSY = 0;
	gAppConfig.camera[CAM_CH0].win.nWidth = SCREEN_1080P_WIDTH;
	gAppConfig.camera[CAM_CH0].win.nHeight = SCREEN_1080P_HEIGHT;
	

	
	snprintf(gAppConfig.camera[CAM_CH1].szName,SZ_DESC_MAX,"CAM_CH1");
	gAppConfig.camera[CAM_CH1].isp_fmt = V4L2_PIX_FMT_NV12;
	gAppConfig.camera[CAM_CH1].sen_fmt = V4L2_PIX_FMT_SRGGB12;
	
	gAppConfig.camera[CAM_CH1].kBuf_cnt = CAM_CH0_KBUFCNT;
	gAppConfig.camera[CAM_CH1].win.nSX = 0;
	gAppConfig.camera[CAM_CH1].win.nSY = 0;
	gAppConfig.camera[CAM_CH1].win.nWidth = SCREEN_720P_WIDTH;
	gAppConfig.camera[CAM_CH1].win.nHeight = SCREEN_720P_HEIGHT;
	
	

	snprintf(gAppConfig.display[DISP_CH0].szName,SZ_DESC_MAX,"DISP_CH0");
	gAppConfig.display[DISP_CH0].win[CAM_CH0].nSX = 0;
	gAppConfig.display[DISP_CH0].win[CAM_CH0].nSY = 0;
	gAppConfig.display[DISP_CH0].win[CAM_CH0].nWidth = SCREEN_1080P_WIDTH/2;
	gAppConfig.display[DISP_CH0].win[CAM_CH0].nHeight = SCREEN_1080P_HEIGHT/2;
	
	gAppConfig.display[DISP_CH0].win[CAM_CH1].nSX = SCREEN_1080P_WIDTH/2;
	gAppConfig.display[DISP_CH0].win[CAM_CH1].nSY = SCREEN_1080P_HEIGHT/2;
	gAppConfig.display[DISP_CH0].win[CAM_CH1].nWidth = SCREEN_1080P_WIDTH/2;
	gAppConfig.display[DISP_CH0].win[CAM_CH1].nHeight = SCREEN_1080P_HEIGHT/2;
	
	gAppConfig.display[DISP_CH0].canvas.nSX = 0;
	gAppConfig.display[DISP_CH0].canvas.nSY = 0;
	gAppConfig.display[DISP_CH0].canvas.nWidth = SCREEN_1080P_WIDTH;
	gAppConfig.display[DISP_CH0].canvas.nHeight = SCREEN_1080P_HEIGHT;
	
	gAppConfig.display[DISP_CH0].nOutputIF = DISP_OIF_HDMI;
	gAppConfig.display[DISP_CH0].nOutputStd = DISP_TV_MOD_1080P_60HZ;
	gAppConfig.display[DISP_CH0].inPixFmt = DISP_FORMAT_YUV420;




	/* Create Task */
	gAppCtrl.task[TASK_MAIN].pParamCfg = &gAppCtrl;	
	OSA_mbxCreate(&gAppCtrl.task_Mbox);
	OSA_tskCreate(&gAppCtrl.task[TASK_MAIN], Task_Main, 0, TASK_STACK, TASK_STATUS_READY, 10000);



	/* Create Sub task */	
	ack=OSA_mbxSendMsg( &gAppCtrl.task[TASK_MAIN].mbxHndl, &gAppCtrl.task_Mbox, TASK_CMD_CREATE, &prm, OSA_MBX_WAIT_ACK);
	if(ack==OSA_EFAIL){
		gAppCtrl.isExit=TRUE;
		goto __exit_error;
	}

	/* Set ready to Sub task */	
	ack=OSA_mbxSendMsg( &gAppCtrl.task[TASK_MAIN].mbxHndl, &gAppCtrl.task_Mbox, TASK_CMD_READY, &prm, OSA_MBX_WAIT_ACK);
	if(ack==OSA_EFAIL){
		gAppCtrl.isExit=TRUE;
		goto __exit_error;
	}

	
	
	do
	{
		cmd = getchar();
		OSA_printf("\r\n");

		switch (cmd) 
		{

			case '1':
				ack=OSA_mbxSendMsg( &gAppCtrl.task[TASK_MAIN].mbxHndl, &gAppCtrl.task_Mbox, TASK_CMD_READY, &prm, OSA_MBX_WAIT_ACK);
				break;

			case '2':
				OSA_printf("INIT command iussued\n");
				ack=OSA_mbxSendMsg( &gAppCtrl.task[TASK_MAIN].mbxHndl, &gAppCtrl.task_Mbox, TASK_CMD_INIT, &prm, OSA_MBX_WAIT_ACK);
				break;

			case '3':
				OSA_printf("DEINIT command iussued\n");
				ack=OSA_mbxSendMsg( &gAppCtrl.task[TASK_MAIN].mbxHndl, &gAppCtrl.task_Mbox, TASK_CMD_DEINIT, &prm, OSA_MBX_WAIT_ACK);
				break;

			case '4':
				OSA_printf("RUN command iussued\n");
				OSA_mbxSendMsg( &gAppCtrl.task[TASK_MAIN].mbxHndl, &gAppCtrl.task_Mbox, TASK_CMD_RUN, &prm, OSA_MBX_WAIT_ACK);
				break;

			case '5':
				OSA_printf("STOP command iussued\n");
				OSA_mbxSendMsg( &gAppCtrl.task[TASK_MAIN].mbxHndl, &gAppCtrl.task_Mbox, TASK_CMD_STOP, &prm, OSA_MBX_WAIT_ACK);
				break;

			case '6':
				OSA_mbxSendMsg( &gAppCtrl.task[TASK_MAIN].mbxHndl, &gAppCtrl.task_Mbox, TASK_CMD_RESUME, &prm, OSA_MBX_WAIT_ACK);
				break;

			case '7':
				OSA_mbxSendMsg( &gAppCtrl.task[TASK_MAIN].mbxHndl, &gAppCtrl.task_Mbox, TASK_CMD_PAUSE, &prm, OSA_MBX_WAIT_ACK);
				break;

			case '8':
				OSA_mbxSendMsg( &gAppCtrl.task[TASK_MAIN].mbxHndl, &gAppCtrl.task_Mbox, TASK_CMD_RESET, &prm, OSA_MBX_WAIT_ACK);
				break;

			case '9':
				gAppCtrl.isExit=TRUE;
				break;
		}


	} while(gAppCtrl.isExit==FALSE);




__exit_error:
	/* Delete sub tasks */
	OSA_mbxSendMsg( &gAppCtrl.task[TASK_MAIN].mbxHndl, &gAppCtrl.task_Mbox, TASK_CMD_DELETE, &prm, OSA_MBX_WAIT_ACK);

	/* Delete Task */
	OSA_tskDelete(&gAppCtrl.task[TASK_MAIN]);
	OSA_mbxDelete(&gAppCtrl.task_Mbox);


	/*! Deinit OSA library */
	OSA_exit();

	if(gAppCtrl.task_Syncker){
		OSA_memFree(gAppCtrl.task_Syncker);
	}
	
	return OSA_SOK;
}

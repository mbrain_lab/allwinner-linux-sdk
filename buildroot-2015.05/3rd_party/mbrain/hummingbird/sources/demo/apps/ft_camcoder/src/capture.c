/**
 * @file   capture.c
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.08.02
 * @brief  Capture task
 *
 * Detailed description of file.
 *
 * @copyright
 * (c) Copyright MBRAIN & mBrain(2015).  All Rights Reserved.
 */

/*! [ 1]. File Directive (#include) */
#include <osa.h>
#include <osa_debug.h>
#include <osa_buf.h>
#include <osa_thr.h>

#include "capture.h"
#include "task.h"
#include "ft_camcoder.h"


/*! [ 2]. Local Macro (#define, enum) */
#define CAP_DEVICE_CH0			"/dev/video1"
#define CAP_CH0_FPS				30

/*! [ 3]. Type definition or structure declaration (typedef or struct) */
typedef struct {
	int                 fd_dev;
	int                 vfe_fmt;
	int                 vfe_type;
	int                 imgsen_fmt;
	int                 fps;
	int                 kBufCnt;
	enum v4l2_buf_type  capBufType;
	Uint32              capMode;
	OSA_BufHndl        *bufHndl;
	OSA_BufCreate 		bufCreatePrm;

	OSA_ThrHndl         thrCap;
	int                 exitThrCap;
} sys_cam;

/*! [ 4]. Local Function prototype declaration */

/*! [ 5]. Local Variable declaration */
/*! [ 6]. Global Variable declaration */
unsigned int gCapLoopCnt=0;
sys_cam gSysCam[CAM_CH_MAXCNT];





int Capture_init(_cam *cap_cfg)
{
	int status = OSA_EFAIL;
	int camIdx=0, nCapBufIdx = 0;
	struct v4l2_input inp;
	struct v4l2_format fmt;
	struct v4l2_pix_format sub_fmt;
	struct v4l2_streamparm parms;
	struct v4l2_requestbuffers capBuf;
	struct v4l2_buffer buf;
	

	memset(&gSysCam,0x00,sizeof(sys_cam)*CAM_CH_MAXCNT);
	gSysCam[CAM_CH0].bufHndl = NULL;
	gSysCam[CAM_CH1].bufHndl = NULL;
	
	gSysCam[CAM_CH0].exitThrCap = FALSE;
	gSysCam[CAM_CH1].exitThrCap = FALSE;
	
	gSysCam[CAM_CH0].vfe_fmt = cap_cfg[CAM_CH0].isp_fmt;
	gSysCam[CAM_CH0].imgsen_fmt = cap_cfg[CAM_CH0].sen_fmt;
	gSysCam[CAM_CH0].vfe_type = V4L2_INPUT_TYPE_CAMERA;
	gSysCam[CAM_CH0].fps = CAP_CH0_FPS;
	gSysCam[CAM_CH0].kBufCnt = cap_cfg[CAM_CH0].kBuf_cnt;
	gSysCam[CAM_CH0].capBufType = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	gSysCam[CAM_CH0].capMode = V4L2_MODE_VIDEO;


	

	for (camIdx=0; camIdx<CAM_CH_MAXCNT; camIdx++)
	{
		OSA_printf("CAM Name=%s\n", cap_cfg[camIdx].szName);
		OSA_printf("   sx=%d\n",cap_cfg[camIdx].win.nSX);
		OSA_printf("   sy=%d\n",cap_cfg[camIdx].win.nSY);
		OSA_printf("   width=%d\n",cap_cfg[camIdx].win.nWidth);
		OSA_printf("   height=%d\n",cap_cfg[camIdx].win.nHeight);
		OSA_printf("   capBufType=%x\n",gSysCam[CAM_CH0].capBufType);
		OSA_printf("   vfe_fmt=%x\n",gSysCam[CAM_CH0].vfe_fmt);
		OSA_printf("   imgsen_fmt=%x\n",gSysCam[CAM_CH0].imgsen_fmt);
	}

	
	gSysCam[CAM_CH0].fd_dev = open (CAP_DEVICE_CH0, O_RDWR /* required */ | O_NONBLOCK, 0);
	if (gSysCam[CAM_CH0].fd_dev<0){
		close(gSysCam[CAM_CH0].fd_dev);
		OSA_ERROR("Fail to open %s\n",CAP_DEVICE_CH0);
		return OSA_EFAIL;
	}

	inp.index = 0;
	inp.type = gSysCam[CAM_CH0].vfe_type;

	if (-1 == ioctl (gSysCam[CAM_CH0].fd_dev, VIDIOC_S_INPUT, &inp)){
		OSA_printf("VIDIOC_S_INPUT error!\n");
		goto __exitWithIoctlErr;
	}

	//Test VIDIOC_S_FMT
	OSA_memClean(fmt);
	fmt.type                = gSysCam[CAM_CH0].capBufType;
	fmt.fmt.pix.width       = cap_cfg[CAM_CH0].win.nWidth;
	fmt.fmt.pix.height      = cap_cfg[CAM_CH0].win.nHeight;
	fmt.fmt.pix.pixelformat = gSysCam[CAM_CH0].vfe_fmt;
	fmt.fmt.pix.field       = V4L2_FIELD_NONE;

	fmt.fmt.pix.subchannel = &sub_fmt;
	sub_fmt.width = cap_cfg[CAM_CH0].win.nWidth;
	sub_fmt.height = cap_cfg[CAM_CH0].win.nHeight;
	sub_fmt.pixelformat = gSysCam[CAM_CH0].imgsen_fmt;
	sub_fmt.field = V4L2_FIELD_NONE;

	if (-1 == ioctl (gSysCam[CAM_CH0].fd_dev, VIDIOC_S_FMT, &fmt))
	{
		OSA_ERROR("VIDIOC_S_FMT error!\n");
		goto __exitWithIoctlErr;
	}

	//Test VIDIOC_S_PARM
	parms.type = gSysCam[CAM_CH0].capBufType;
	parms.parm.capture.timeperframe.numerator = 1;
	parms.parm.capture.timeperframe.denominator = gSysCam[CAM_CH0].fps;
	parms.parm.capture.capturemode = gSysCam[CAM_CH0].capMode;
	
	if (-1 == ioctl (gSysCam[CAM_CH0].fd_dev, VIDIOC_S_PARM, &parms)){
		OSA_ERROR("VIDIOC_S_PARM error\n");
		goto __exitWithIoctlErr;
	}

	

	OSA_memClean(capBuf);
	OSA_memClean(gSysCam[CAM_CH0].bufCreatePrm);

	capBuf.count               = gSysCam[CAM_CH0].kBufCnt;
	capBuf.type                = gSysCam[CAM_CH0].capBufType;
	capBuf.memory              = V4L2_MEMORY_MMAP;
	
	if (-1 == ioctl (gSysCam[CAM_CH0].fd_dev, VIDIOC_REQBUFS, &capBuf)){
		OSA_ERROR("VIDIOC_REQBUFS error\n");
		goto __exitWithIoctlErr;
	}

	gSysCam[CAM_CH0].bufHndl = OSA_memAlloc(sizeof(OSA_BufHndl));
	if( gSysCam[CAM_CH0].bufHndl == NULL ){
		OSA_ERROR("Not enough memory\n");
		goto __exitWithIoctlErr;
	}else{
		OSA_printf("Buffer allocated at 0x%x\n",gSysCam[CAM_CH0].bufHndl);
	}

	gSysCam[CAM_CH0].bufCreatePrm.numBuf = gSysCam[CAM_CH0].kBufCnt;
	
	for (nCapBufIdx = 0; nCapBufIdx < capBuf.count; ++nCapBufIdx) 
	{
		OSA_memClean(buf);
		buf.type        = gSysCam[CAM_CH0].capBufType;
		buf.memory      = V4L2_MEMORY_MMAP;
		buf.index       = nCapBufIdx;

		if (-1 == ioctl (gSysCam[CAM_CH0].fd_dev, VIDIOC_QUERYBUF, &buf)){
			OSA_ERROR("VIDIOC_QUERYBUF error\n");
			goto __exitWithFreeMMAP; 
		}

		gSysCam[CAM_CH0].bufCreatePrm.nBufLen[nCapBufIdx] = buf.length;
		
		gSysCam[CAM_CH0].bufCreatePrm.bufPhysAddr[nCapBufIdx] = (void *)buf.m.offset;
		gSysCam[CAM_CH0].bufCreatePrm.bufVirtAddr[nCapBufIdx] = mmap ( NULL /* start anywhere */,
												buf.length,
												PROT_READ | PROT_WRITE /* required */,
												MAP_SHARED /* recommended */,
												gSysCam[CAM_CH0].fd_dev, buf.m.offset);

		OSA_printf("Buf Idx(%02d) Vir:0x%x\n",nCapBufIdx, gSysCam[CAM_CH0].bufCreatePrm.bufVirtAddr[nCapBufIdx]);
		if (MAP_FAILED == gSysCam[CAM_CH0].bufCreatePrm.bufVirtAddr[nCapBufIdx]){
			OSA_ERROR("mmap failed\n");
			goto __exitWithFreeMMAP; 
		}else{
			memset(gSysCam[CAM_CH0].bufCreatePrm.bufVirtAddr[nCapBufIdx],0x00,buf.length);
		}
	}

	if(gSysCam[CAM_CH0].bufCreatePrm.numBuf)
    {
		if( OSA_bufCreate(gSysCam[CAM_CH0].bufHndl, &gSysCam[CAM_CH0].bufCreatePrm)==OSA_SOK)
		{
			OSA_printf("Buffer create(0x%x)d\n",gSysCam[CAM_CH0].bufHndl);	
		}
    }

	OSA_printf("Capture init: Done\n");
    return OSA_SOK;



__exitWithFreeMMAP:
	for( camIdx=0; camIdx<CAM_CH_MAXCNT; camIdx++ ){
		for (nCapBufIdx = 0; nCapBufIdx < gSysCam[camIdx].kBufCnt; ++nCapBufIdx) {
			if( gSysCam[CAM_CH0].bufCreatePrm.bufVirtAddr[nCapBufIdx]!=NULL ){
				if (-1 == munmap (gSysCam[CAM_CH0].bufCreatePrm.bufVirtAddr[nCapBufIdx], gSysCam[CAM_CH0].bufCreatePrm.nBufLen[nCapBufIdx])) {
					OSA_ERROR("munmap error\n");
				}
			}
		}
	}


__exitWithIoctlErr:
	
	for( camIdx=0; camIdx<CAM_CH_MAXCNT; camIdx++ ){
		if(gSysCam[camIdx].fd_dev){
			close(gSysCam[camIdx].fd_dev);
			gSysCam[camIdx].fd_dev = 0;
		}
		
		if( gSysCam[camIdx].bufHndl ){
			OSA_bufDelete(gSysCam[camIdx].bufHndl);
			OSA_memFree(gSysCam[camIdx].bufHndl);
			gSysCam[camIdx].bufHndl = NULL;
		}
	}


	return status;
}




int Capture_deinit(void)
{
	int camIdx=0, nCapBufIdx = 0;
	OSA_BufInfo *pBufInfo=NULL;
	//struct v4l2_requestbuffers capBuf;
	struct v4l2_buffer buf;

	
	for( camIdx=0; camIdx<CAM_CH_MAXCNT; camIdx++ ){
		if(gSysCam[camIdx].fd_dev){
			if (-1 == ioctl (gSysCam[camIdx].fd_dev, VIDIOC_STREAMOFF, &gSysCam[camIdx].capBufType)){
				OSA_ERROR("VIDIOC_STREAMOFF failed\n");
			}else{
				OSA_printf("VIDIOC_STREAMOFF ok\n");
			}
		}
	
		for (nCapBufIdx = 0; nCapBufIdx < gSysCam[camIdx].kBufCnt; ++nCapBufIdx) {
			if(gSysCam[camIdx].fd_dev){
				OSA_memClean(buf);
				buf.type = gSysCam[camIdx].capBufType;
				buf.memory = V4L2_MEMORY_MMAP;
			
				ioctl (gSysCam[camIdx].fd_dev, VIDIOC_DQBUF, &buf);
			}

			
			pBufInfo = OSA_bufGetBufInfo(gSysCam[camIdx].bufHndl, nCapBufIdx);

			if (pBufInfo!=NULL){
				//OSA_printf("Buffer Index(%d): VirAddr=%p len=%d\n",nCapBufIdx, pBufInfo->virtAddr, pBufInfo->size);
				if( pBufInfo->virtAddr!=NULL ){
					if (-1 == munmap (pBufInfo->virtAddr, pBufInfo->size)) {
						OSA_ERROR("munmap error\n");
					}
				}
			}else{
				OSA_ERROR("Buffer Index(%d) has NULL\n",nCapBufIdx);
			}
		}
		
		if(gSysCam[camIdx].fd_dev){
			close(gSysCam[camIdx].fd_dev);
			gSysCam[camIdx].fd_dev = 0;
		}
		
		if( gSysCam[camIdx].bufHndl ){
			OSA_bufDelete(gSysCam[camIdx].bufHndl);
			OSA_memFree(gSysCam[camIdx].bufHndl);
			gSysCam[camIdx].bufHndl = NULL;
		}
	}

	OSA_printf("Capture deinit: Done\n");
	return OSA_SOK;
}





int Capture_thread_ch0(void* pParam)
{	
	int status = OSA_EFAIL;
	int nCapBufIdx = 0;
	struct v4l2_buffer buf;
	unsigned long long initail_ts=0,cur_ts=0;
	
	OSA_TskHndl* pTaskHndl = (OSA_TskHndl*)pParam;
	Config * pCfg = (Config*)pTaskHndl->pParamCfg;


	for (nCapBufIdx = 0; nCapBufIdx < gSysCam[CAM_CH0].kBufCnt; ++nCapBufIdx) 
	{
		OSA_memClean(buf);

		buf.type        = gSysCam[CAM_CH0].capBufType;
		buf.memory      = V4L2_MEMORY_MMAP;
		buf.index       = nCapBufIdx;

		if (-1 == ioctl (gSysCam[CAM_CH0].fd_dev, VIDIOC_QBUF, &buf)) {
			OSA_ERROR("VIDIOC_QBUF failed\n");
			gSysCam[CAM_CH0].exitThrCap=TRUE;
			break;
		}
	}

	/*< STREAM ON */
	if (-1 == ioctl (gSysCam[CAM_CH0].fd_dev, VIDIOC_STREAMON, &gSysCam[CAM_CH0].capBufType)) {
		OSA_ERROR("VIDIOC_STREAMON failed\n");
		gSysCam[CAM_CH0].exitThrCap=TRUE;
	}
	OSA_printf("VIDIOC_STREAMON ok\n\n");

	


	while( gSysCam[CAM_CH0].exitThrCap==FALSE )
	{
		fd_set fds;
		struct timeval tv;
		int r;

		FD_ZERO (&fds);
		FD_SET (gSysCam[CAM_CH0].fd_dev, &fds);

		/* Timeout. */
		tv.tv_sec = 1;
		tv.tv_usec = 0;

		r = select (gSysCam[CAM_CH0].fd_dev + 1, &fds, NULL, NULL, &tv);

		if (-1 == r) {
			if (EINTR == errno){
				continue;
			}
			OSA_ERROR("select err\n");
		}else if (0 == r) {
			OSA_ERROR("select timeout\n");
			gSysCam[CAM_CH0].exitThrCap=TRUE;
			continue;
		}


	
		OSA_memClean(buf);
		buf.type = gSysCam[CAM_CH0].capBufType;
		buf.memory = V4L2_MEMORY_MMAP;

		if(ioctl(gSysCam[CAM_CH0].fd_dev, VIDIOC_DQBUF, &buf)<0)
		{
			OSA_ERROR("VIDIOC_DQBUF Fail\n");
		}
		else
		{
			OSA_BufInfo *pBufInfo=NULL;
			
			if( initail_ts==0){
				initail_ts = OSA_getTvalInMsec(buf.timestamp);
			}
			cur_ts = OSA_getTvalInMsec(buf.timestamp)-initail_ts;
			
			OSA_assert(buf.index < gSysCam[CAM_CH0].kBufCnt);
			//OSA_printf("IDX: %02d SEQ:%08d Msec:%8lld \n", buf.index, buf.sequence, cur_ts);

			
			if( OSA_bufGetEmpty(gSysCam[CAM_CH0].bufHndl, &nCapBufIdx, OSA_TIMEOUT_NONE) == OSA_SOK )
			{
				pBufInfo = OSA_bufGetBufInfo(gSysCam[CAM_CH0].bufHndl, nCapBufIdx);

				OSA_assert(pBufInfo!=NULL);

				

				pBufInfo->physAddr = (int*)&buf.m.offset;
				pBufInfo->virtAddr = gSysCam[CAM_CH0].bufCreatePrm.bufVirtAddr[nCapBufIdx];
				pBufInfo->width = pCfg->camera[CAM_CH0].win.nWidth;
				pBufInfo->height = pCfg->camera[CAM_CH0].win.nHeight;
				pBufInfo->timestamp = cur_ts;
				pBufInfo->pixFmt = gSysCam[CAM_CH0].vfe_fmt;

				//OSA_printf("CAPTURE: PUT BUFFER(%d)\n",nCapBufIdx);

				OSA_bufPutFull(gSysCam[CAM_CH0].bufHndl, nCapBufIdx);
			}
			else
			{
				OSA_ERROR("Fail to get empty buffer\n");
			}
			
			ioctl (gSysCam[CAM_CH0].fd_dev, VIDIOC_QBUF, &buf);
		}

		
		//disp_set_addr(gDspFrame_size.width, gDspFrame_size.height,(int*)&buf.m.offset);
		
	}



	/*< STREAM OFF */
	if (-1 == ioctl (gSysCam[CAM_CH0].fd_dev, VIDIOC_STREAMOFF, &gSysCam[CAM_CH0].capBufType)){
		OSA_ERROR("VIDIOC_STREAMOFF failed\n");
	}else{
		OSA_printf("VIDIOC_STREAMOFF ok\n");
	}



	OSA_printf("Capture Finish...\n");

	return OSA_SOK;
}
















/**
 * @name    SignalTraps()
 * @brief   signal trap function
 * @ingroup Group_FunctionTest
 * @param: None
 *
 * @retval: None
 *
 * Example Usage:
 * @code
 *  SignalTraps();
 * @endcode
 */
int TaskCapture( struct OSA_TskHndl *pPrc, OSA_MsgHndl *pMsg, Uint32 curState )
{
	int retVal = OSA_SOK;
	int status = OSA_SOK;
	int curStatus = -1;
	Uint16 cmd = NULL;
	Config * pCfg = (Config*)pPrc->pParamCfg;

	/**! Loop operation */
	if (pMsg==NULL)
	{
		gCapLoopCnt++;
		if(gCapLoopCnt%10000==0){
			//OSA_printf("Task[Capture] Loop Operation...\n");
			gCapLoopCnt = 0;
		}
	}
	else
	{
		cmd = OSA_msgGetCmd(pMsg);

		switch(cmd)
		{
			case TASK_CMD_READY:
				OSA_printf("[CAPTURE TASK] Get READY command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_READY);
				break;
				
			case TASK_CMD_INIT:
				OSA_printf("[CAPTURE TASK] Get INIT command\n");

				curStatus = OSA_tskGetState(pPrc);
				
				if( curStatus==TASK_STATUS_READY )
				{
					if( Capture_init(pCfg->camera)!=OSA_SOK ){
						OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
						OSA_tskSetState(pPrc,TASK_STATUS_ERROR);
					}else{
						pPrc->buffer[0] = gSysCam[CAM_CH0].bufHndl;
						pPrc->buffer[1] = gSysCam[CAM_CH1].bufHndl;
						
						OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
						OSA_tskSetState(pPrc,TASK_STATUS_INIT);
					}
				}
				else
				{
					OSA_ERROR("Invalid cur status\n");	
					OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
				}
				break;

			case TASK_CMD_DEINIT:
				OSA_printf("[CAPTURE TASK] Get DEINIT command\n");
				
				curStatus = OSA_tskGetState(pPrc);

				if( curStatus==TASK_STATUS_STOP )
				{
					Capture_deinit();
					
					OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
					OSA_tskSetState(pPrc,TASK_STATUS_DEINIT);
				}
				else
				{
					OSA_ERROR("Invalid cur status\n");	
					OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
				}
				break;
				
			case TASK_CMD_RUN:
				OSA_printf("[CAPTURE TASK] Get RUN command\n");

				curStatus = OSA_tskGetState(pPrc);

				if( curStatus==TASK_STATUS_INIT )
				{
					status = OSA_thrCreate(&gSysCam[CAM_CH0].thrCap, Capture_thread_ch0, 0, 1024, pPrc);
				    if(status==OSA_SOK) {
				    	OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
						OSA_tskSetState(pPrc,TASK_STATUS_RUN);
				    }else{
				    	OSA_ERROR("Fail to create caputre thread for ch0\n");
				    	OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
				    	OSA_tskSetState(pPrc,TASK_STATUS_ERROR);
				    }
			    }else{
			    	OSA_ERROR("Invalid cur status\n");	
			    	OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
			    }
				break;
				
			case TASK_CMD_STOP:
				OSA_printf("[CAPTURE TASK] Get STOP command\n");

				curStatus = OSA_tskGetState(pPrc);

				if( curStatus!=TASK_STATUS_INIT && 
					curStatus!=TASK_STATUS_DEINIT &&
					curStatus!=TASK_STATUS_READY)
				{				
					gSysCam[CAM_CH0].exitThrCap=TRUE;

					OSA_thrDelete(&gSysCam[CAM_CH0].thrCap);

					OSA_printf("Capture ch0 thread deleted\n");

					OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
					OSA_tskSetState(pPrc,TASK_STATUS_STOP);
				}
				else
				{
					OSA_ERROR("Invalid cur status\n");	
					OSA_tskAckOrFreeMsg(pMsg, OSA_EFAIL);
				}
				break;
				
			case TASK_CMD_RESUME:
				OSA_printf("[CAPTURE TASK] Get RESUME command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_RESUME);
				break;
				
			case TASK_CMD_PAUSE:
				OSA_printf("[CAPTURE TASK] Get PAUSE command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_PAUSE);
				break;
				
			case TASK_CMD_RESET:
				OSA_printf("[CAPTURE TASK] Get RESET command\n");
				OSA_tskAckOrFreeMsg(pMsg, OSA_SOK);
				OSA_tskSetState(pPrc,TASK_STATUS_RESET);
				break;
				
			default:
				break;
		}
	}

	return retVal;
}


#!/bin/bash

#insmod /lib/modules/3.3.0/kernel/drivers/media/video/videobuf-core.ko
#insmod /lib/modules/3.3.0/kernel/drivers/media/video/videobuf-dma-contig.ko
#insmod /lib/modules/3.3.0/kernel/drivers/media/video/videobuf2-core.ko
#insmod /lib/modules/3.3.0/kernel/drivers/media/video/videobuf2-memops.ko
#insmod /lib/modules/3.3.0/kernel/drivers/media/video/videobuf2-vmalloc.ko
#insmod /lib/modules/3.3.0/kernel/drivers/media/video/sunxi-vfe/vfe_os.ko
#insmod /lib/modules/3.3.0/kernel/drivers/media/video/sunxi-vfe/vfe_subdev.ko
#insmod /lib/modules/3.3.0/kernel/drivers/media/video/sunxi-vfe/device/imx122.ko
#insmod /lib/modules/3.3.0/kernel/drivers/media/video/sunxi-vfe/actuator/actuator.ko
#insmod /lib/modules/3.3.0/kernel/drivers/media/video/sunxi-vfe/camera_detector/cam_detect.ko
#insmod /lib/modules/3.3.0/kernel/drivers/media/video/sunxi-vfe/vfe_v4l2.ko

#insmod /lib/modules/3.3.0/kernel/drivers/video/sun6i/disp/disp.ko
#insmod /lib/modules/3.3.0/kernel/drivers/video/sun6i/lcd/lcd.ko
#insmod /lib/modules/3.3.0/kernel/drivers/video/sun6i/hdmi/hdmi.ko


#drv_list=("videobuf-core.ko" "videobuf-dma-contig.ko" "videobuf2-core.ko" "videobuf2-memops.ko" "videobuf2-vmalloc.ko" "vfe_os.ko" "vfe_subdev.ko" "imx122.ko" "actuator.ko" "cam_detect.ko" "vfe_v4l2.ko" "disp.ko" "lcd.ko" "hdmi.ko")

drv_list=("videobuf-core.ko" "videobuf-dma-contig.ko" "videobuf2-core.ko" "videobuf2-memops.ko" "videobuf2-vmalloc.ko" "vfe_os.ko" "vfe_subdev.ko" "imx122.ko" "actuator.ko" "cam_detect.ko" "vfe_v4l2.ko")

for drv in "${drv_list[@]}"; do
	#echo $drv
	file_path=$(find /lib/modules/ -name ${drv})
	if [[ -z "$file_path" ]]; then
		echo "Cannot find device driver[${drv}]"
	else
		echo "Install device driver[${drv}]"
		if [ "$drv" == "videobuf-core.ko" ]; then
			echo "videobuf2-core.ko detected"
			insmod $file_path debug=3
		else
			insmod $file_path
		fi
		sleep 0.1
	fi
done

sleep 1

echo "1" > /sys/devices/platform/sunxi_vfe.1/video4linux/video1/attr/vfe_dbg_en
echo "3" > /sys/devices/platform/sunxi_vfe.1/video4linux/video1/attr/vfe_dbg_lv
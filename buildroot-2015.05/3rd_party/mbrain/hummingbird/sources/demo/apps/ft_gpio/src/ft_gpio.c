/**
 * @file   ft_hdmi.c
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.07.07
 * @brief  HDMI funnction test for Hummingbird A31
 *
 * Detailed description of file.
 *
 * @copyright
 * (c) Copyright MBRAIN & mBrain(2015).  All Rights Reserved.
 */

/*! [ 1]. File Directive (#include) */
#include <stdio.h>
#include <osa.h>
#include <drv_gpio.h>

/*! [ 2]. Local Macro (#define, enum) */
/*! [ 3]. Type definition or structure declaration (typedef or struct) */
/*! [ 4]. Local Function prototype declaration */
/*! [ 5]. Local Variable declaration */
int gIsExitMainTask=FALSE;

static void SignalTraps(void);
static void SignalHandler(int sig);

void SignalTraps(void)
{
	OSA_attachSignalHandler(SIGTERM, SignalHandler);
	OSA_attachSignalHandler(SIGINT,  SignalHandler);
}

void SignalHandler(int sig)
{
	if ( (sig==SIGTERM)||(sig==SIGINT) ){
		OSA_printf("gIsExitRecvThr was setted with 1\n");
		gIsExitMainTask=TRUE;
	}

}



int show_Usage(char *str)
{
	OSA_printf(" \n");
	OSA_printf(" GPIO Test Utility, \r\n");
	OSA_printf(" Usage: <GROUP> <PINNUM> <VALUE>\r\n");
	OSA_printf("      -GROUP: 0~10\r\n");
	OSA_printf("              0:PORTA\r\n");
	OSA_printf("              1:PORTB\r\n");
	OSA_printf("              2:PORTC ...\r\n");
	OSA_printf("      -PINNUM: integer\r\n");
	OSA_printf("      -VAL: 0:Low, 1:High\r\n");
	OSA_printf(" Example: PH9 set to high\r\n");
	OSA_printf("          ft_gpio.out 7 9 0\r\n");
	OSA_printf(" \n");
	return 0;
}



/**
 * @name    main()
 * @brief   main function
 * @ingroup Group_FunctionTest
 * @param <Reserved>
 *
 * @retval <Reserved>
 *
 * Example Usage:
 * @code
 *  ft_gpio.out 7 9 0
 * @endcode
 */
int main(int argc, char **argv)
{
	int nPort, nPinNum, nPinVal;

	/* Init OSA library */
	OSA_init();

	/* Register signal */
	SignalTraps();


	OSA_printf("[%s] Start\n",__FILE__);

	if(argc!=4) {
		show_Usage(argv[0]);
		return -1;
	}

	nPort = atoi(argv[1]);
	nPinNum = atoi(argv[2]);
	nPinVal = atoi(argv[3]);

	OSA_printf("PORT=%d PINNUM=%d PINVAL=%d\n",nPort, nPinNum, nPinVal);

	DRV_GpioSetVal(nPort, nPinNum, nPinVal);

	/*! Deinit OSA library */
	OSA_exit();
	return OSA_SOK;
}

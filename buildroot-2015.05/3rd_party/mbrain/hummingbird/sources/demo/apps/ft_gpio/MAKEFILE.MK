# (c) MidmeSoft
include $(DEMO_BUILD_DIR)/COMMON_HEADER.MK


libs:
	make -fMAKEFILE.MK -C./src MODULE=ft_gpio $(MAKE_TARGET)


exe:
	make -fMAKEFILE.MK -C./src MODULE=ft_gpio exe

all:
	make -fMAKEFILE.MK depend
	make -fMAKEFILE.MK clean
	make -fMAKEFILE.MK libs

clean:
	make -fMAKEFILE.MK libs MAKE_TARGET=clean

depend:
	make -fMAKEFILE.MK libs MAKE_TARGET=depend

install:
	make -fMAKEFILE.MK libs MAKE_TARGET=install
ifneq ($(DEMO_INSTALL_DIR),)
	mkdir -p $(DEMO_INSTALL_DIR)/$(MODULE) 2>/dev/null
	cp -a $(EXE_BASE_DIR)/$(DEMO_CONFIG)/$(MODULE).out $(DEMO_INSTALL_DIR)/$(MODULE)
endif

.PHONY : install clean depend all libs


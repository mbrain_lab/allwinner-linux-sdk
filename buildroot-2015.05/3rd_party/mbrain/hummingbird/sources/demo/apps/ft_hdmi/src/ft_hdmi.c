/**
 * @file   ft_hdmi.c
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.07.07
 * @brief  HDMI funnction test for Hummingbird A31 
 * 
 * Detailed description of file.
 *
 * @copyright
 * (c) Copyright MBRAIN & mBrain(2015).  All Rights Reserved.
 */
 
/*! [ 1]. File Directive (#include) */
#include <stdio.h>
#include <osa.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/unistd.h>
#include <sys/mman.h>


#include <fcntl.h>
#include <linux/drv_display.h>

#include "ft_hdmi.h"

/*! [ 2]. Local Macro (#define, enum) */
#define DEV_NAME	"/dev/disp"

#define TYPE_COUNT                      11


#define COLOR_WHITE                     0xffffffff
#define COLOR_YELLOW                    0xffffff00
#define COLOR_GREEN                     0xff00ff00
#define COLOR_CYAN                      0xff00ffff
#define COLOR_MAGENTA                   0xffff00ff
#define COLOR_RED                       0xffff0000
#define COLOR_BLUE                      0xff0000ff
#define COLOR_BLACK                     0xff000000


/*! [ 3]. Type definition or structure declaration (typedef or struct) */
static unsigned int colorbar[8] = 
{
    COLOR_WHITE,
    COLOR_YELLOW,
    COLOR_CYAN,
    COLOR_GREEN,
    COLOR_MAGENTA,
    COLOR_RED,
    COLOR_BLUE,
    COLOR_BLACK
};

struct hdmi_output_type
{
    int mode;
    int width;
    int height;
    char name[16];
};


static struct hdmi_output_type type_list[TYPE_COUNT] = {
    {DISP_TV_MOD_480I,       720,  480,  "480I"},       /* 0 */
    {DISP_TV_MOD_576I,       720,  576,  "576I"},       /* 1 */ 
    {DISP_TV_MOD_480P,       720,  480,  "480P"},       /* 2 */
    {DISP_TV_MOD_576P,       720,  576,  "576P"},       /* 3 */
    {DISP_TV_MOD_720P_50HZ,  1280, 720,  "720P 50HZ"},  /* 4 */
    {DISP_TV_MOD_720P_60HZ,  1280, 720,  "720P 60HZ"},  /* 5 */
    {DISP_TV_MOD_1080I_50HZ, 1920, 1080, "1080I 50HZ"}, /* 6 */
    {DISP_TV_MOD_1080I_60HZ, 1920, 1080, "1080I 60HZ"}, /* 7 */
    {DISP_TV_MOD_1080P_24HZ, 1920, 1080, "1080P 24HZ"}, /* 8 */
    {DISP_TV_MOD_1080P_50HZ, 1920, 1080, "1080P 50HZ"}, /* 9 */
    {DISP_TV_MOD_1080P_60HZ, 1920, 1080, "1080P 60HZ"}  /* 10 */
};


/*! [ 4]. Local Function prototype declaration */
/*! [ 5]. Local Variable declaration */
static unsigned char *gFbBuff = NULL;
int           gIsExitMainTask=FALSE;

static void SignalTraps(void);
static void SignalHandler(int sig);

void SignalTraps(void)
{
  OSA_attachSignalHandler(SIGTERM, SignalHandler);
  OSA_attachSignalHandler(SIGINT,  SignalHandler);
}

void SignalHandler(int sig)
{
  if ( (sig==SIGTERM)||(sig==SIGINT) ){
    OSA_printf("gIsExitRecvThr was setted with 1\n");
    gIsExitMainTask=TRUE;
  }
  
}


int fill_colorbar(int devfd, int nWidth, int nHeight)
{
    unsigned int args[4];
    __disp_fb_create_para_t fb_para;
    int ret;
    int i, j;
    int fb;
    unsigned int *p = NULL;
    

    memset(&fb_para, 0, sizeof(__disp_fb_create_para_t));
    fb_para.fb_mode = FB_MODE_SCREEN1;
    fb_para.mode = DISP_LAYER_WORK_MODE_NORMAL;
    fb_para.buffer_num = 2;
    fb_para.width = nWidth;
    fb_para.height = nHeight;

    args[0] = 1;
    args[1] = (unsigned int)&fb_para;
    ret = ioctl(devfd, DISP_CMD_FB_REQUEST, args);
    if (ret < 0) {
        OSA_ERROR("request framebuffer failed\n");
        return -1;
    }

    fb = open("/dev/fb1", O_RDWR);
    gFbBuff = mmap(NULL, nWidth * nHeight * 4, PROT_READ|PROT_WRITE, MAP_SHARED, fb, 0);
    for (i = 0; i < nHeight; i++)
    {
        p = (unsigned int *)(gFbBuff + nWidth * i * 4);
        for (j = 0; j < nWidth; j++)
        {
            int idx = (j << 3) / nWidth;
            *p++ = colorbar[idx];
        }
    }
    close(fb);

    return 0;
}



int detect_output_mode(int devfd, int* pOutputMode, int* pScrrenWidth, int* pScreenHeight )
{
	unsigned int ioctl_param[4]={0,};
	int nIdx=TYPE_COUNT-1;
	int ret;
	int support_mode;

	for (nIdx=(TYPE_COUNT-1); nIdx>0; nIdx--)
	{
		ioctl_param[0] = 1;
		ioctl_param[1] = type_list[nIdx].mode;
		ret = ioctl(devfd, DISP_CMD_HDMI_SUPPORT_MODE, ioctl_param);
		if (ret == 1) 
		{
			OSA_printf("hdmitester: your device support %s\n",type_list[nIdx].name);
			*pOutputMode = type_list[nIdx].mode;
			*pScrrenWidth  = type_list[nIdx].width;
			*pScreenHeight = type_list[nIdx].height;
			break;
		}	
	}

	if (nIdx==0){
		OSA_ERROR("Fail to findout output mode\n");
		return -1;
	}
	return 0;
}



/**
 * @name    main()
 * @brief   main function
 * @ingroup Group_FunctionTest
 * @param <Reserved>
 *
 * @retval <Reserved>
 *
 * Example Usage:
 * @code
 *  1. insmode device drivers
 *     insmod /lib/modules/3.3.0/kernel/drivers/video/sun6i/disp/disp.ko
 *     insmod /lib/modules/3.3.0/kernel/drivers/video/sun6i/lcd/lcd.ko 
 *     insmod /lib/modules/3.3.0/kernel/drivers/video/sun6i/hdmi/hdmi.ko
 * @endcode
 */
int main(int argc, char **argv)
{
	int ret;
	int flags = 0;
	int retry = 0;
	int status = 0;
	int fd_disp = -1;
	int output_mode;
	int screen_width = 0,screen_height = 0;
	unsigned int disp_param[4]={0,};

	__disp_output_type_t disp_output_type;
	
    /* Init OSA library */
    OSA_init();

	/* Register signal */
	SignalTraps();
    

    OSA_printf("[%s] Start\n",__FILE__);


	fd_disp = open("/dev/disp", O_RDWR);
    if (fd_disp == -1) {
		OSA_ERROR("open %s failed(%s)\n", DEV_NAME, strerror(errno));
		goto err;
    }
	disp_param[0] = 0;
	disp_output_type = (__disp_output_type_t)ioctl(fd_disp, DISP_CMD_GET_OUTPUT_TYPE,(void*)disp_param);
	OSA_printf("DISPSEL[%d]'s disp_output_type=%d\n",disp_param[0],disp_output_type);   

	

	while(gIsExitMainTask==FALSE)
	{
		if (status == 1) {
			sleep(1);
			continue;
        }

            
		if(disp_output_type==DISP_OUTPUT_TYPE_LCD)
			disp_param[0] = 1;
		else
			disp_param[0] = 0;

		ret = ioctl(fd_disp, DISP_CMD_HDMI_GET_HPD_STATUS, disp_param);
		OSA_printf("ret=%d\n",ret);
		if (ret == 1) 
		{
			flags = 0;

			/* Select output mode */
            if (detect_output_mode(fd_disp,&output_mode, &screen_width, &screen_height)<0)
            {
            	break;
            }
            OSA_printf("Detect: mode=%d width=%d height=%d\n",output_mode, screen_width, screen_height);
            OSA_waitMsecs(10);

            
			disp_param[0] = 1;
			disp_param[1] = output_mode;
			ret = ioctl(fd_disp, DISP_CMD_HDMI_SET_MODE, disp_param);
			if (ret < 0) {
				OSA_ERROR("hdmitester: set hdmi output mode failed(%d)\n", ret);
				break;
			}

            /* Init FB driver and fill colorbar */
            if (fill_colorbar(fd_disp, screen_width, screen_height)<0){
            	break;
            }

            /* set hdmi on */
			disp_param[0] = 1;
			ret = ioctl(fd_disp, DISP_CMD_HDMI_ON, disp_param);
			if (ret < 0) {
				OSA_ERROR("hdmitester: set hdmi on failed(%d)\n", ret);
				break;
			}

			status = 1;

			__disp_init_t disp_init_info;
			memset(&disp_init_info, 0x00, sizeof(disp_init_info));
			disp_param[0] = (unsigned int)&disp_init_info;
			ret = ioctl(fd_disp, DISP_CMD_GET_DISP_INIT_PARA, disp_param);
			if (ret < 0) {
				OSA_ERROR("hdmitester: get disp initialize parameter information(%d)\n", ret);
			}else{
				OSA_printf("DISP INIT INFO: b_init=%d\n",disp_init_info.b_init);
				OSA_printf("DISP INIT INFO: disp_mode=%d\n",disp_init_info.disp_mode);
				
				OSA_printf("DISP INIT INFO: output_type[0]=%d\n",disp_init_info.output_type[0]);
				OSA_printf("DISP INIT INFO: output_type[1]=%d\n",disp_init_info.output_type[1]);
				
				OSA_printf("DISP INIT INFO: tv_mode[0]=%d\n",disp_init_info.tv_mode[0]);
				OSA_printf("DISP INIT INFO: tv_mode[1]=%d\n",disp_init_info.tv_mode[1]);
				
				OSA_printf("DISP INIT INFO: vga_mode[0]=%d\n",disp_init_info.vga_mode[0]);
				OSA_printf("DISP INIT INFO: vga_mode[1]=%d\n",disp_init_info.vga_mode[1]);
				
				OSA_printf("DISP INIT INFO: buffer_num[0]=%d\n",disp_init_info.buffer_num[0]);
				OSA_printf("DISP INIT INFO: buffer_num[1]=%d\n",disp_init_info.buffer_num[1]);
				
				OSA_printf("DISP INIT INFO: scaler_mode[0]=%d\n",disp_init_info.scaler_mode[0]);
				OSA_printf("DISP INIT INFO: scaler_mode[1]=%d\n",disp_init_info.scaler_mode[1]);
				
				OSA_printf("DISP INIT INFO: format[0]=0x%02x\n",disp_init_info.format[0]);
				OSA_printf("DISP INIT INFO: format[1]=0x%02x\n",disp_init_info.format[1]);

				OSA_printf("DISP INIT INFO: seq[0]=0x%02x\n",disp_init_info.seq[0]);
				OSA_printf("DISP INIT INFO: seq[1]=0x%02x\n",disp_init_info.seq[1]);
				
				OSA_printf("DISP INIT INFO: br_swap[0]=%d\n",disp_init_info.br_swap[0]);
				OSA_printf("DISP INIT INFO: br_swap[1]=%d\n",disp_init_info.br_swap[1]);
				
				OSA_printf("DISP INIT INFO: fb_width[0]=%d\n",disp_init_info.fb_width[0]);
				OSA_printf("DISP INIT INFO: fb_height[0]=%d\n",disp_init_info.fb_height[0]);
				
				OSA_printf("DISP INIT INFO: fb_width[1]=%d\n",disp_init_info.fb_width[1]);
				OSA_printf("DISP INIT INFO: fb_height[1]=%d\n",disp_init_info.fb_height[1]);
			}
			
		}
		else
		{
			sleep(1);
			continue;
		}
	}


err:
	if (gFbBuff != NULL)
	{
		munmap(gFbBuff, screen_width * screen_height * 4);
	    disp_param[0] = 1;
	    ioctl(fd_disp, DISP_CMD_FB_RELEASE, disp_param);
	}
	
	/* Close DISP driver */
	close(fd_disp);
	
    /*! Deinit OSA library */
    OSA_exit();
    return OSA_SOK;
}

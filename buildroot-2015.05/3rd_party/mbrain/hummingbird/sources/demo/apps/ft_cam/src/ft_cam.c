/**
 * @file   ft_hdmi.c
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.07.16
 * @brief  HDMI funnction test for Hummingbird A31
 *
 * Detailed description of file.
 *
 * @copyright
 * (c) Copyright MBRAIN & mBrain(2015).  All Rights Reserved.
 */

/*! [ 1]. File Directive (#include) */
#include <osa.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <getopt.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <asm/types.h>
#include <linux/videodev2_new.h>

#include <time.h>
#include <linux/fb.h>
#include <linux/drv_display.h>
#include <linux/sunxi_physmem.h>


/*! [ 2]. Local Macro (#define, enum) */
//#define WR_FILE	1

#define CAP_KERNEL_QUE_CNT		5
#define CANVAS_1080P_WIDTH		1920
#define CANVAS_1080P_HEIGHT		1080

#define CANVAS_720P_WIDTH		1280
#define CANVAS_720P_HEIGHT		720



#define NAME_CAP_DEVICE			"/dev/video1"
#define NAME_PMEM_DEVICE		"/dev/sunxi_mem"
#define NAME_DISP_DEVICE		"/dev/disp"
#define NAME_FB0				"/dev/fb0"

#define SCREEN_0	0

/*! [ 3]. Type definition or structure declaration (typedef or struct) */
struct buffer {
	void *                  start;
	size_t                  length;
};


struct size{
	int width;
	int height;
};


/*! [ 4]. Local Function prototype declaration */
static void SignalTraps(void);
static void SignalHandler(int sig);


/*! [ 5]. Local Variable declaration */
int gIsExitMainTask=FALSE;

int gVfe_KBufCnt = CAP_KERNEL_QUE_CNT;           //<< Kernel VFE buffer count
int gVfe_Format;
int gVfe_ImgSenFormat;

struct size gCapFrame_size = {CANVAS_1080P_WIDTH,CANVAS_1080P_HEIGHT};
unsigned int gCapDeviceType = V4L2_INPUT_TYPE_CAMERA;


struct size gDspFrame_size = {CANVAS_1080P_WIDTH,CANVAS_1080P_HEIGHT};
__disp_pixel_fmt_t  gDisp_Format;
__disp_pixel_mod_t  gDisp_mode;
__disp_pixel_seq_t	gDisp_seq;

static int fd_phyMemDev = -1;
static int fd_captureDev = -1;
static int fd_display = -1;
static int fd_fb = -1;
struct buffer*		gCapBuffers = NULL;

int nCapFrameSize = 0;
int nDspFrameSize = 0;



int	 gRead_RrameCnt = 100000;
int  gCurFrameCnt = 0;
int  gFrameRate = 30;

__disp_layer_info_t gLayer_para;
unsigned int gHLayer=0;



void SignalTraps(void)
{
	OSA_attachSignalHandler(SIGTERM, SignalHandler);
	OSA_attachSignalHandler(SIGINT,  SignalHandler);
}

void SignalHandler(int sig)
{
	if ( (sig==SIGTERM)||(sig==SIGINT) ){
		OSA_printf("SIGTERM or SIGINT was captured\n");
		gIsExitMainTask=TRUE;
	}

}








int disp_init(void)
{
	int ret=0;
	unsigned int disp_param[4]={0,};
	__disp_output_type_t disp_output_type;

	if((fd_display = open(NAME_DISP_DEVICE,O_RDWR)) == -1)
	{
		OSA_ERROR("Fail to open %s\n",NAME_DISP_DEVICE);
		return OSA_EFAIL;
	}

	disp_param[0] = 0;
	disp_output_type = (__disp_output_type_t)ioctl(fd_display, DISP_CMD_GET_OUTPUT_TYPE,(void*)disp_param);
	OSA_printf("DISPSEL[%d]'s disp_output_type=%d\n",disp_param[0],disp_output_type);

	disp_param[0] = 0;
	ret = ioctl(fd_display, DISP_CMD_HDMI_GET_HPD_STATUS, disp_param);
	OSA_printf("ret=%d\n",ret);


	disp_param[0] = SCREEN_0;
	disp_param[1] = DISP_TV_MOD_1080P_60HZ;
	ret = ioctl(fd_display, DISP_CMD_HDMI_SET_MODE, disp_param);
	if (ret < 0) {
		OSA_ERROR("hdmitester: set hdmi output mode failed(%d)\n", ret);
		goto __error_close_dsp_dev;
	}

	disp_param[0] = 0;
    disp_param[1] = DISP_LAYER_WORK_MODE_SCALER;
    gHLayer = ioctl(fd_display, DISP_CMD_LAYER_REQUEST, (void*)disp_param);
	OSA_printf("ret=%d\n",ret);
	if (gHLayer == 0)
	{
		OSA_ERROR("Fail to request layer0 \n");
        goto __error_close_dsp_dev;
	}
	OSA_printf("video layer hdl:%d\n", gHLayer);


    gLayer_para.mode = DISP_LAYER_WORK_MODE_SCALER;
    gLayer_para.pipe = 0;
    gLayer_para.fb.addr[0]       = 0;	//<< your Y address,modify this
    gLayer_para.fb.addr[1]       = 0;	//<< your C address,modify this
    gLayer_para.fb.addr[2]       = 0;
    gLayer_para.fb.size.width    = gDspFrame_size.width;
    gLayer_para.fb.size.height   = gDspFrame_size.height;
    gLayer_para.fb.mode          = gDisp_mode;
    gLayer_para.fb.format        = gDisp_Format;
    gLayer_para.fb.br_swap       = 0;
    gLayer_para.fb.seq           = gDisp_seq;
    gLayer_para.ck_enable        = 0;
    gLayer_para.alpha_en         = 1;
    gLayer_para.alpha_val        = 0xFF;
    gLayer_para.src_win.x        = 0;
    gLayer_para.src_win.y        = 0;
    gLayer_para.src_win.width    = gDspFrame_size.width;
    gLayer_para.src_win.height   = gDspFrame_size.height;
    gLayer_para.scn_win.x        = 0;
    gLayer_para.scn_win.y        = 0;
    gLayer_para.scn_win.width    = gDspFrame_size.width;
    gLayer_para.scn_win.height   = gDspFrame_size.height;

	disp_param[0] = SCREEN_0;		//<< which screen 0/1
    disp_param[1] = gHLayer;
    disp_param[2] = (__u32)&gLayer_para;
    ioctl(fd_display,DISP_CMD_LAYER_SET_PARA,(void*)disp_param);

    disp_param[0] = SCREEN_0;
    disp_param[1] = gHLayer;
    ioctl(fd_display,DISP_CMD_LAYER_OPEN,(void*)disp_param);


	unsigned long fb_layer;
	void *addr = NULL;
	fd_fb = open(NAME_FB0, O_RDWR);
	if (ioctl(fd_fb, FBIOGET_LAYER_HDL_0, &fb_layer) == -1) {
		OSA_ERROR("get fb layer handel\n");
		goto __error_close_dsp_dev;
	}

	addr = malloc(gDspFrame_size.width*gDspFrame_size.height*3);
	memset(addr, 0xff, gDspFrame_size.width*gDspFrame_size.height*3);
	write(fd_fb, addr, gDspFrame_size.width*gDspFrame_size.height*3);
	//memset(addr, 0x12, 800*480*3);

	OSA_printf("fb_layer hdl: %ld\n", fb_layer);
	close(fd_fb);

	disp_param[0] = 0;
	disp_param[1] = fb_layer;
	ioctl(fd_display, DISP_CMD_LAYER_BOTTOM, (void *)disp_param);
	return OSA_SOK;


__error_close_dsp_dev:
	if(fd_display){
		close(fd_display);
		fd_display = -1;
	}
	return OSA_EFAIL;
}


void disp_start(void)
{
	unsigned int disp_param[4]={0,};

	disp_param[0] = SCREEN_0;
    disp_param[1] = gHLayer;
    ioctl(fd_display, DISP_CMD_VIDEO_START,  (void*)disp_param);
}

void disp_stop(void)
{
	unsigned int disp_param[4]={0,};

	disp_param[0] = SCREEN_0;
    disp_param[1] = gHLayer;
    ioctl(fd_display, DISP_CMD_VIDEO_STOP,  (void*)disp_param);
}


int disp_on(void)
{
	unsigned int disp_param[4]={0,};
	int ret=0;

    /* set hdmi on */
	disp_param[0] = SCREEN_0;
	ret = ioctl(fd_display, DISP_CMD_HDMI_ON, disp_param);
	if (ret < 0) {
		OSA_ERROR("hdmitester: set hdmi on failed(%d)\n", ret);
	}

	return ret;
}




int disp_set_addr(int w,int h,int *addr)
{
	unsigned int disp_param[4]={0,};

	__disp_video_fb_t  fb_addr;
	memset(&fb_addr, 0, sizeof(__disp_video_fb_t));

	fb_addr.interlace       = 0;
	fb_addr.top_field_first = 0;
	fb_addr.frame_rate      = 25;
	fb_addr.addr[0] = *addr;


	switch(gVfe_Format){
		case V4L2_PIX_FMT_YUV422P:
		case V4L2_PIX_FMT_YUYV:
		case V4L2_PIX_FMT_YVYU:
		case V4L2_PIX_FMT_UYVY:
		case V4L2_PIX_FMT_VYUY:
			fb_addr.addr[1]       = *addr+w*h; //your C address,modify this
			fb_addr.addr[2]       = *addr+w*h*3/2;
			break;
		case V4L2_PIX_FMT_YVU420:
		case V4L2_PIX_FMT_YUV420:
			fb_addr.addr[1]       = *addr+w*h; //your C address,modify this
			fb_addr.addr[2]       = *addr+w*h;
			break;
		case V4L2_PIX_FMT_NV16:
		case V4L2_PIX_FMT_NV21:
		case V4L2_PIX_FMT_NV12:
		case V4L2_PIX_FMT_HM12:
			fb_addr.addr[1]       = *addr+w*h; //your C address,modify this
			fb_addr.addr[2]       = gLayer_para.fb.addr[1];
			break;
		default:
			OSA_printf("gDisp_Format is not found!\n");
			break;
	}

	fb_addr.id = 0;  //TODO

	disp_param[0] = SCREEN_0;
	disp_param[1] = gHLayer;
	disp_param[2] = (__u32)&fb_addr;
	return ioctl(fd_display, DISP_CMD_VIDEO_SET_FB, (void*)disp_param);
}


int disp_quit(void)
{
	int ret=0;
	unsigned int disp_param[4]={0,};

	disp_param[0] = 0;
    ret=ioctl(fd_display, DISP_CMD_HDMI_OFF, (void*)disp_param);

    disp_param[0] = SCREEN_0;
    disp_param[1] = gHLayer;
    ret|=ioctl(fd_display, DISP_CMD_LAYER_CLOSE,  (void*)disp_param);

    disp_param[0] = SCREEN_0;
    disp_param[1] = gHLayer;
    ret|=ioctl(fd_display, DISP_CMD_LAYER_RELEASE,  (void*)disp_param);
    close (fd_display);

    return 0;
}


static int read_frame (void)
{
	struct v4l2_buffer buf;
#ifdef WR_FILE
	char szFileName[128]={0,};
#endif

	OSA_memClean(buf);
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;

	ioctl (fd_captureDev, VIDIOC_DQBUF, &buf); //出列采集的帧缓冲

	OSA_assert(buf.index < gVfe_KBufCnt);
	disp_set_addr(gDspFrame_size.width, gDspFrame_size.height,(int*)&buf.m.offset);

#ifdef WR_FILE
	if (gCurFrameCnt<gRead_RrameCnt)
	{
		if(gCurFrameCnt%100==0)
		{
			snprintf(szFileName,128,"/tmp/dump_%04d.yuv",gCurFrameCnt);
			OSA_printf("file[%04d] length = %d\n",buf.index, gCapBuffers[buf.index].length);
			OSA_printf("file[%04d] start = 0x%x\n",buf.index, gCapBuffers[buf.index].start);

			file_fd = fopen(szFileName,"wb");
			fwrite(gCapBuffers[buf.index].start, nCapFrameSize, 1, file_fd);
			if(file_fd){
				fclose(file_fd);
			}
		}
	}
#endif
	ioctl (fd_captureDev, VIDIOC_QBUF, &buf);

	return 1;
}


int capture_test(void)
{
	struct v4l2_input inp;
	struct v4l2_format fmt;
	struct v4l2_pix_format sub_fmt;
	struct v4l2_streamparm parms;

	struct v4l2_requestbuffers capBuf;

	int nCapBufIdx = 0;


	int i = 0;

	int ret = -1;

	OSA_printf("gVfe_Format=%x\n",gVfe_Format);
	OSA_printf("gVfe_ImgSenFormat=%x\n",gVfe_ImgSenFormat);


	fd_phyMemDev = open(NAME_PMEM_DEVICE, O_RDWR /* required */ | O_NONBLOCK, 0);
	if (fd_phyMemDev<0){
		OSA_ERROR("Fail to open %s\n",NAME_PMEM_DEVICE);
		return OSA_EFAIL;
	}

	fd_captureDev = open (NAME_CAP_DEVICE, O_RDWR /* required */ | O_NONBLOCK, 0);
	if (fd_captureDev<0){
		close(fd_phyMemDev);
		OSA_ERROR("Fail to open %s\n",NAME_CAP_DEVICE);
		return OSA_EFAIL;
	}

	inp.index = 0;
	inp.type = gCapDeviceType;

	if (inp.type == V4L2_INPUT_TYPE_CAMERA)
		OSA_printf("Capture Device type is V4L2_INPUT_TYPE_CAMERA!\n");

	if (-1 == ioctl (fd_captureDev, VIDIOC_S_INPUT, &inp)){
		OSA_printf("VIDIOC_S_INPUT error!\n");
		goto __exitWithIoctlErr;
	}

	//Test VIDIOC_S_FMT
	OSA_memClean(fmt);
	fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	fmt.fmt.pix.width       = gCapFrame_size.width;
	fmt.fmt.pix.height      = gCapFrame_size.height;
	fmt.fmt.pix.pixelformat = gVfe_Format;
	fmt.fmt.pix.field       = V4L2_FIELD_NONE;


	fmt.fmt.pix.subchannel = &sub_fmt;
	sub_fmt.width = gCapFrame_size.width;
	sub_fmt.height = gCapFrame_size.height;
	sub_fmt.pixelformat = gVfe_ImgSenFormat;
	sub_fmt.field = V4L2_FIELD_NONE;

	if (-1 == ioctl (fd_captureDev, VIDIOC_S_FMT, &fmt))
	{
		OSA_ERROR("VIDIOC_S_FMT error!\n");
		goto __exitWithIoctlErr;
	}

	//Test VIDIOC_S_PARM
	parms.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	parms.parm.capture.timeperframe.numerator = 1;
	parms.parm.capture.timeperframe.denominator = gFrameRate;
	parms.parm.capture.capturemode = V4L2_MODE_VIDEO; //V4L2_MODE_IMAGE

	if (-1 == ioctl (fd_captureDev, VIDIOC_S_PARM, &parms)){
		OSA_ERROR("VIDIOC_S_PARM error\n");
	}



	OSA_memClean(capBuf);

	capBuf.count               = gVfe_KBufCnt;
	capBuf.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	capBuf.memory              = V4L2_MEMORY_MMAP;

	ioctl (fd_captureDev, VIDIOC_REQBUFS, &capBuf);

	gCapBuffers = calloc (capBuf.count, sizeof (*gCapBuffers));
	for (nCapBufIdx = 0; nCapBufIdx < capBuf.count; ++nCapBufIdx)
	{
		struct v4l2_buffer buf;
		OSA_memClean(buf);
		buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory      = V4L2_MEMORY_MMAP;
		buf.index       = nCapBufIdx;

		if (-1 == ioctl (fd_captureDev, VIDIOC_QUERYBUF, &buf)){
			OSA_ERROR("VIDIOC_QUERYBUF error\n");
		}

		if (nCapFrameSize<=0){
			nCapFrameSize = buf.length;
		}

		gCapBuffers[nCapBufIdx].length = buf.length;
		gCapBuffers[nCapBufIdx].start  = mmap ( NULL /* start anywhere */,
												buf.length,
												PROT_READ | PROT_WRITE /* required */,
												MAP_SHARED /* recommended */,
												fd_captureDev, buf.m.offset);

		if (MAP_FAILED == gCapBuffers[nCapBufIdx].start){
			OSA_ERROR("mmap failed\n");
			goto __exitWithFreeMMAP;
		}else{
			memset(gCapBuffers[nCapBufIdx].start,0x00,buf.length);
		}
	}

	for (i = 0; i < nCapBufIdx; ++i)
	{
		struct v4l2_buffer buf;
		OSA_memClean(buf);

		buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory      = V4L2_MEMORY_MMAP;
		buf.index       = i;

		if (-1 == ioctl (fd_captureDev, VIDIOC_QBUF, &buf)) {
			OSA_ERROR("VIDIOC_QBUF failed\n");
		}
	}


	disp_init();
	disp_start();
	disp_on();


	/*< STREAM ON */
	if (-1 == ioctl (fd_captureDev, VIDIOC_STREAMON, &capBuf.type)) {//开始捕捉图像数据
		OSA_ERROR("VIDIOC_STREAMON failed\n");
		goto __exitWithFreeMMAP;
	}
	OSA_printf("VIDIOC_STREAMON ok\n\n");


	while(gIsExitMainTask==FALSE)
	{
		for(;;)
		{
			fd_set fds;
			struct timeval tv;
			int r;

			FD_ZERO (&fds);
			FD_SET (fd_captureDev, &fds);

			/* Timeout. */
			tv.tv_sec = 1;
			tv.tv_usec = 0;

			r = select (fd_captureDev + 1, &fds, NULL, NULL, &tv);//判断是否可读（即摄像头是否准备好），tv是定时

			if (-1 == r) {
				if (EINTR == errno){
					continue;
				}
				OSA_printf("select err\n");
			}

			if (0 == r) {
				OSA_ERROR("select timeout\n");
				exit (EXIT_FAILURE);
			}

			if (read_frame ())
			{
				gCurFrameCnt++;
				if (gCurFrameCnt>=gRead_RrameCnt)
				{
					gIsExitMainTask = TRUE;
				}
				break;
			}
		}
	}



	if (-1 == ioctl (fd_captureDev, VIDIOC_STREAMOFF, &capBuf.type)){
		OSA_ERROR("VIDIOC_STREAMOFF failed\n");
	}else{
		OSA_printf("VIDIOC_STREAMOFF ok\n");
	}


__exitWithFreeMMAP:
	if(gCapBuffers){
		for (nCapBufIdx = 0; nCapBufIdx < gVfe_KBufCnt; ++nCapBufIdx) {
			if( gCapBuffers[nCapBufIdx].start!=NULL ){
				if (-1 == munmap (gCapBuffers[nCapBufIdx].start, gCapBuffers[nCapBufIdx].length)) {
					OSA_ERROR("munmap error\n");
				}
			}
		}
	}

__exitWithIoctlErr:
	if(fd_captureDev>0){
		close(fd_captureDev);
	}

	if(fd_phyMemDev>0){
		close(fd_phyMemDev);
	}

	disp_stop();
	disp_quit();


	return ret;
}









/**
 * @name    main()
 * @brief   main function
 * @ingroup Group_FunctionTest
 * @param <Reserved>
 *
 * @retval <Reserved>
 *
 * Example Usage:
 * @code
 *  <reserved>
 * @endcode
 */
int main(int argc, char **argv)
{
	/* Init OSA library */
	OSA_init();

	/* Register signal */
	SignalTraps();

	OSA_printf("[%s] Start\n",__FILE__);


	gCapFrame_size.width = CANVAS_1080P_WIDTH;
	gCapFrame_size.height = CANVAS_1080P_HEIGHT;

	gDspFrame_size.width = CANVAS_1080P_WIDTH;
	gDspFrame_size.height  = CANVAS_1080P_HEIGHT;

	OSA_printf("Capture width = %d height = %d\n",gCapFrame_size.width,gCapFrame_size.height);
	OSA_printf("Display width = %d height = %d\n",gDspFrame_size.width,gDspFrame_size.height);


	/*! Candiate formats
	 *	V4L2_PIX_FMT_SBGGR12
	 *	V4L2_PIX_FMT_SGBRG12
	 *	V4L2_PIX_FMT_SGRBG12
	 *	V4L2_PIX_FMT_SRGGB12
	 */

	gVfe_Format = V4L2_PIX_FMT_NV12;//V4L2_PIX_FMT_NV12;
	gVfe_ImgSenFormat = V4L2_PIX_FMT_SRGGB12;
	gCapDeviceType = V4L2_INPUT_TYPE_CAMERA;


	gDisp_Format= DISP_FORMAT_YUV420;
	gDisp_mode  = DISP_MOD_NON_MB_UV_COMBINED;
	gDisp_seq   = DISP_SEQ_UVUV;

	capture_test();


	/*! Deinit OSA library */
	OSA_exit();
	return OSA_SOK;
}

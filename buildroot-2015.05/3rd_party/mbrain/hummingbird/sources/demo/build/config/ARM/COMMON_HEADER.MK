# (c) MidmeSoft

ifndef $(COMMON_HEADER_MK)
COMMON_HEADER_MK = 1

#
# Compiler Class define
#
CC=$(TARGET_CC)gcc
AR=$(TARGET_CC)ar
LD=$(TARGET_CC)gcc
AS=$(TARGET_CC)as
CPP=$(CC) ?�E
NM=$(TARGET_CC)nm
STRIP=$(TARGET_CC)strip
OBJCOPY=$(TARGET_CC)objcopy
OBJDUMP=$(TARGET_CC)objdump


LIB_BASE_DIR=$(DEMO_BUILD_DIR)/lib/$(DEMO_PLATFORM)
OBJ_BASE_DIR=$(DEMO_BUILD_DIR)/lib/obj/$(DEMO_PLATFORM)
EXE_BASE_DIR=$(DEMO_ROOT_DIR)/bin/$(DEMO_PLATFORM)

$(info $(LIB_BASE_DIR))

LIB_DIR=$(LIB_BASE_DIR)/$(DEMO_CONFIG)

CC_OPTS=-c -Wall -fPIC

ifeq ($(DEMO_CONFIG), debug)
#debug compiler options
CC_OPTS+=-g
OPTI_OPTS=
DEFINE=-DDEBUG
else
#release compiler options
CC_OPTS+=
OPTI_OPTS=-O3
DEFINE=
endif



#
# Define preprocessors
#
#DEFINE+=-DTESSST




AR_OPTS=-rc
LD_OPTS=-lpthread -lm

FILES=$(subst ./, , $(foreach dir,.,$(wildcard $(dir)/*.c)) )

vpath %.a $(LIB_DIR)

include $(DEMO_BUILD_DIR)/INCLUDES.MK

INCLUDE=
INCLUDE+=$(COMMON_INC)
INCLUDE+=$(USER_INC)
INCLUDE+=$(OSA_INC)


endif # ifndef $(COMMON_HEADER_MK)


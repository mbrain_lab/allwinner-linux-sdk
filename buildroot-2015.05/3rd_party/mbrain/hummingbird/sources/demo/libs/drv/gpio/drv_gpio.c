/**
 * @file   drv_gpio.c
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.07.12
 * @brief  GPIO user space driver
 * 
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain & MidmeSoft
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain & MidmeSoft
 * 
 * (c) Copyright mBrain & MidmeSoft(2015).  All Rights Reserved.
 */


/*! [ 1]. File Directive (#include) */
#include <osa.h>

/* Standard Header files */
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <linux/types.h>

#include <drv_gpio.h>


/*! [ 2]. Local Macro (#define, enum) */
/*! [ 3]. Type definition or structure declaration (typedef or struct */
/*! [ 4]. Local Function prototype declaration */

typedef struct{
	int		nPort;
	int		nStartPinNum;
}__drv_gpio_cfg;

__drv_gpio_cfg gGioCfg[PORT_MAXCNT] = {
	{PORT_A,     0 },
	{PORT_B,    30 },
	{PORT_C,    40 },
	{PORT_D,    70 },
	{PORT_E,   100 },
	{PORT_F,   119 },
	{PORT_G,   127 },
	{PORT_H,   148 },
	{PORT_L,   181 },
	{PORT_M,   192 },
	{PORT_AXP, 202 },
};

/**
 * @name    DRV_GpioSetVal()
 * @brief   This function set gio value with given pin number
 * @ingroup Group_LibHwPeripheral
 *
 * This API provides certain actions as an example.
 *
 * @param [bPinNum] target pin number
 * @param [bVal] target pin value(1: high, 0:low)
 *
 * @retval  0 Success
 * @retval -1 Failure
 *
 * Example Usage:
 * @code
 *    char nPinNum=54;
 *    char nPinVal=1;
 *    LIBHWPERI_SetGioPinRead(nPinNum,nPinVal);
 * @endcode
 */
int DRV_GpioSetVal(int portGroup, int nPinNum, int bVal)
{
	FILE *sysfs_handle;
	int isNeedExportation = FALSE;
	char szCmd[128]={0,};
	char szPathDrv[128]={0,};
	
	if (portGroup<PORT_A || portGroup>=PORT_MAXCNT){
		OSA_ERROR("Invalid Port Group");
		return OSA_EFAIL;
	}

	
	snprintf(szPathDrv, 128, "/sys/class/gpio/gpio%d",gGioCfg[portGroup].nStartPinNum+nPinNum);
	

	if( access( szPathDrv, F_OK ) == -1 ) {
		OSA_printf("Given path[%s] doesn't exist\n",szPathDrv);
		isNeedExportation = TRUE;
	}
	

	if (isNeedExportation==TRUE) 
	{	
		if ((sysfs_handle = fopen("/sys/class/gpio/export", "w")) != NULL)
		{	
			snprintf(szCmd, 128, "%d", gGioCfg[portGroup].nStartPinNum+nPinNum);
			fwrite(szCmd, sizeof(char), strlen(szCmd), sysfs_handle);
			fclose(sysfs_handle);
		}
		else
		{
			OSA_ERROR("Cannot open /sys/class/gpio/export \n");
			return OSA_EFAIL;
		}
	}

	memset(szPathDrv,0x00,128);
	snprintf(szPathDrv, 128, "/sys/class/gpio/gpio%d/direction",gGioCfg[portGroup].nStartPinNum+nPinNum);

	if (( sysfs_handle = fopen(szPathDrv,"w")) != NULL)
	{
		fwrite("out", sizeof(char), 4, sysfs_handle);
		fclose(sysfs_handle);
	}


	memset(szPathDrv,0x00,128);
	snprintf(szPathDrv, 128, "/sys/class/gpio/gpio%d/value",gGioCfg[portGroup].nStartPinNum+nPinNum);

	if (( sysfs_handle = fopen(szPathDrv,"w")) != NULL)
	{
		if ( bVal==1 ){
			fwrite("1", sizeof(char), 2, sysfs_handle);
		}else{
			fwrite("0", sizeof(char), 2, sysfs_handle);
		}
		fclose(sysfs_handle);
	}

	
	return OSA_SOK;
}






/**
 * @file   drv_spi.h
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.07.12
 * @brief  SPI user space driver
 * 
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain & MidmeSoft
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain & MidmeSoft
 * 
 * (c) Copyright mBrain & MidmeSoft(2015).  All Rights Reserved.
 */


/*! [ 1]. File classification */

#ifndef _DRV_SPI_H_
#define _DRV_SPI_H_

#  ifdef __cplusplus
extern "C" {
#  endif /* __cplusplus */

/*! [ 2]. File Directive (#include) */
#include <osa.h>
#include <osa_mutex.h>

#include <linux/types.h>
#include <linux/spi/spidev.h>


/*! [ 3]. Global macro and enum */
#define SPI_DRV_BO_MSBFIRST (0)
#define SPI_DRV_BO_LSBFIRST (1)

#define SPI_DRV_8BIT	(8)
#define SPI_DRV_16BIT	(16)



/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
typedef struct {
	Int8        dev[128];
    Uint8       mode;
    Uint8       bits;
    Uint32      speed;
    Uint16      delay;
    Uint8       byteOrder;
} DRV_SPICreatParam;


typedef struct {
  int           fd;
  int			isExternalLock;
  OSA_MutexHndl spiBusLock;
  DRV_SPICreatParam cParam;
} DRV_SpiHndl;

/*! [ 5]. Global Function prototype declaration */
extern DRV_SpiHndl* DRV_spiInit(DRV_SPICreatParam* pParam, OSA_MutexHndl* pLock);
extern int DRV_spiRead8(DRV_SpiHndl *hndl, Uint8 *buf,  Uint32 count, Uint8 *Obuf);
extern int DRV_spiWrite8(DRV_SpiHndl *hndl, Uint8 *buf, Uint32 count);
extern int DRV_spiRead16(DRV_SpiHndl *hndl, Uint16 *buf,  Uint32 count, Uint8 *Obuf);
extern int DRV_spiWrite16(DRV_SpiHndl *hndl, Uint16 *buf, Uint32 count);
extern int DRV_spiDeinit(DRV_SpiHndl *hndl);


/*! [ 6]. Global Variable declaration  */





#  ifdef __cplusplus
    }
#  endif /* __cplusplus */
#endif //_DRV_SPI_H_


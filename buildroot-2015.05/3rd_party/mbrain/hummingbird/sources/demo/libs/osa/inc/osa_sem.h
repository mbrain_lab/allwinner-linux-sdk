/**
 * @file		  osa_sem.h
 * @Author 	      Joel,Kim (Joel.Kim@mbrain.org)
 * @date		  2015.07.12
 * @brief		  Semaphore helper APIs
 * 
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain & MidmeSoft
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain & MidmeSoft
 * 
 * (c) Copyright mBrain & MidmeSoft(2015).		  All Rights Reserved.
 */

/*! [ 1]. File classification */
#ifndef _OSA_SEM_H_
#define _OSA_SEM_H_

/*! [ 2]. File Directive (#include) */
#include <osa.h>


/*! [ 3]. Global macro and enum */


/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
typedef struct {

	Uint32 count;
	Uint32 maxCount;
	pthread_mutex_t lock;
	pthread_cond_t	cond;

} OSA_SemHndl;


/*! [ 5]. Global Function prototype declaration */
int OSA_semCreate(OSA_SemHndl *hndl, Uint32 maxCount, Uint32 initVal);
int OSA_semWait(OSA_SemHndl *hndl, Uint32 timeout);
int OSA_semSignal(OSA_SemHndl *hndl);
int OSA_semDelete(OSA_SemHndl *hndl);


/*! [ 6]. Global Variable declaration  */

#endif /* _OSA_FLG_H_ */


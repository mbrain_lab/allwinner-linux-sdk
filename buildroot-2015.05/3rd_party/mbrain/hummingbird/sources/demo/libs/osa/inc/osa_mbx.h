/**
 * @file		osa_mbx.h
 * @Author     Joel,Kim (Joel.Kim@mbrain.org)
 * @date	   2015.07.12
 * @brief	   Messagebox helper APIs
 *
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain & MidmeSoft
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain & MidmeSoft
 * 
 * (c) Copyright mBrain & MidmeSoft(2015).	   All Rights Reserved.
 */

/*! [ 1]. File classification */
#ifndef _OSA_MBX_H_
#define _OSA_MBX_H_

/*! [ 2]. File Directive (#include) */
#include <osa_msgq.h>


/*! [ 3]. Global macro and enum */
#define OSA_MBX_WAIT_ACK            0x0002   ///< Message Flag : Wait for ACK
#define OSA_MBX_FREE_PRM            0x0004   ///< Message Flag : Message parameters are malloc'ed and need to be free'ed
#define OSA_MBX_BROADCAST_MAX       10       ///< Max message queues/PRC's that can be broadcast to


/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
/*
 * @brief Mailbox handle
*/
typedef struct {

	OSA_MsgqHndl rcvMbx;        ///< Receive mailbox
	OSA_MsgqHndl ackMbx;        ///< ACK mailbox

} OSA_MbxHndl;


/*! [ 5]. Global Function prototype declaration */
int OSA_mbxCreate(OSA_MbxHndl *pHndl);
int OSA_mbxDelete(OSA_MbxHndl *pHndl);
int OSA_mbxSendMsg(OSA_MbxHndl *pTo, OSA_MbxHndl *pFrom, Uint16 cmd, void *pPrm, Uint16 flags);
int OSA_mbxBroadcastMsg(OSA_MbxHndl *pToList[], OSA_MbxHndl *pFrom, Uint16 cmd, void *pPrm, Uint16 flags);
int OSA_mbxAckOrFreeMsg(OSA_MsgHndl *pMsg, int ackRetVal);
int OSA_mbxWaitMsg(OSA_MbxHndl *pHndl, OSA_MsgHndl **pMsg);
int OSA_mbxCheckMsg(OSA_MbxHndl *pHndl, OSA_MsgHndl **pMsg);
int OSA_mbxWaitCmd(OSA_MbxHndl *pHndl, OSA_MsgHndl **pMsg, Uint16 waitCmd);
int OSA_mbxFlush(OSA_MbxHndl *pHndl);


/*! [ 6]. Global Variable declaration  */


#endif /* _OSA_MBX_H_ */


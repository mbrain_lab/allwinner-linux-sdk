#include "osa_ini.h"

/* Standard Header files */
#include <stdio.h>
#include <unistd.h>

/** Maximum value size for integers and doubles. */
#define MAXVALSZ	1024

/** Minimal allocated number of entries in a OSA_iniDic */
#define DICTMINSZ	128

/** Invalid key token */
#define DICT_INVALID_KEY		((char*)-1)


/*******************************************************************************
	 Private functions
 ******************************************************************************/
/* Doubles the allocated size associated to a pointer */
/* 'size' is the current allocated size. */
static void * mem_double(void * ptr, int size)
{
	void * newptr ;

	newptr = calloc(2*size, 1);
	if (newptr==NULL) {
		return NULL ;
	}
	memcpy(newptr, ptr, size);
	free(ptr);
	return newptr ;
}



/*
 ******************************************************************************
 SOURCE CODE - xstrdup() routine BEGINS HERE
 ******************************************************************************
 // Function Name: xstrdup()
 // Argument:
				a) Input: 	s String to duplicate
				b) Output:	Pointer to a newly allocated string, to be freed with free()
 // Description  :
				Duplicate a string

				This is a replacement for strdup(). This implementation is provided
				for systems that do not have it.
 *****************************************************************************
*/
static char * xstrdup(char * s)
{
	char * t ;
	if (!s)
		return NULL ;
	t = malloc(strlen(s)+1) ;
	if (t) {
			strcpy(t,s);
	}
	return t ;
}





/*
 ******************************************************************************
 SOURCE CODE - dictionary_hash() routine BEGINS HERE
 ******************************************************************************
 // Function Name: dictionary_hash()
 // Argument:
	a) Input: 	key		Character string to use for key.
	b) Output:	1 unsigned int on at least 32 bits.
 // Description  :
	Compute the hash key for a string.

	This hash function has been taken from an Article in Dr Dobbs Journal.
	This is normally a collision-free function, distributing keys evenly.
	The key is stored anyway in the struct so that collision can be avoided
	by comparing the key itself in last resort.
 *****************************************************************************
*/
unsigned dictionary_hash(char * key)
{
	int			len ;
	unsigned	hash ;
	int			i ;

	len = strlen(key);
	for (hash=0, i=0 ; i<len ; i++) {
		hash += (unsigned)key[i] ;
		hash += (hash<<10);
		hash ^= (hash>>6) ;
	}
	hash += (hash <<3);
	hash ^= (hash >>11);
	hash += (hash <<15);
	return hash ;
}



/*
 ******************************************************************************
 SOURCE CODE - dictionary_new() routine BEGINS HERE
 ******************************************************************************
 // Function Name: dictionary_new()
 // Argument:
	a) Input: 	size	Optional initial size of the OSA_iniDic.
	b) Output:	1 newly allocated OSA_iniDic objet.
 // Description  :
	Create a new OSA_iniDic object.

	This function allocates a new OSA_iniDic object of given size and returns
	it. If you do not know in advance (roughly) the number of entries in the
	OSA_iniDic, give size=0.
 *****************************************************************************
*/
OSA_iniDic * dictionary_new(int size)
{
	OSA_iniDic	*	d ;

	/* If no size was specified, allocate space for DICTMINSZ */
	if (size<DICTMINSZ) size=DICTMINSZ ;

	if (!(d = (OSA_iniDic *)calloc(1, sizeof(OSA_iniDic)))) {
		return NULL;
	}
	d->size = size ;
	d->val	= (char **)calloc(size, sizeof(char*));
	d->key	= (char **)calloc(size, sizeof(char*));
	d->hash = (unsigned int *)calloc(size, sizeof(unsigned));
	return d ;
}


/*
 ******************************************************************************
 SOURCE CODE - dictionary_del() routine BEGINS HERE
 ******************************************************************************
 // Function Name: dictionary_del()
 // Argument:
				a) Input: 	d	OSA_iniDic object to deallocate.
				b) Output:	void
 // Description  :
				Deallocate a OSA_iniDic object and all memory associated to it.
 *****************************************************************************
*/
void dictionary_del(OSA_iniDic * d)
{
	int		i ;

	if (d==NULL) return ;
	for (i=0 ; i<d->size ; i++) {
		if (d->key[i]!=NULL)
			free(d->key[i]);
		if (d->val[i]!=NULL)
			free(d->val[i]);
	}
	free(d->val);
	free(d->key);
	free(d->hash);
	free(d);
	return ;
}


/*
 ******************************************************************************
 SOURCE CODE - dictionary_get() routine BEGINS HERE
 ******************************************************************************
 // Function Name: dictionary_get()
 // Argument:
     a) Input: 	d		OSA_iniDic object to search.
				key		Key to look for in the OSA_iniDic.
				def 		Default value to return if key not found.
     b) Output:	1 pointer to internally allocated character string.
 // Description  :
     Get a value from a OSA_iniDic.
     
     This function locates a key in a OSA_iniDic and returns a pointer to its
     value, or the passed 'def' pointer if no such key can be found in
     OSA_iniDic. The returned character pointer points to data internal to the
     OSA_iniDic object, you should not try to free it or modify it.
 *****************************************************************************
*/
char * dictionary_get(OSA_iniDic * d, char * key, char * def)
{
	unsigned	hash ;
	int			i ;

	hash = dictionary_hash(key);
	for (i=0 ; i<d->size ; i++) {
		if (d->key[i]==NULL)
				continue ;
		/* Compare hash */
		if (hash==d->hash[i]) {
			/* Compare string, to avoid hash collisions */
			if (!strcmp(key, d->key[i])) {
				return d->val[i] ;
			}
		}
	}
	return def ;
}



/*
 ******************************************************************************
 SOURCE CODE - dictionary_set() routine BEGINS HERE
 ******************************************************************************
 // Function Name: dictionary_set()
 // Argument:
    a) Input:  d    OSA_iniDic object to modify.
               key  Key to modify or add.
               val  Value to add.
    b) Output:	int
              0 if Ok, anything else otherwise
 // Description  :
    Set a value in a OSA_iniDic.
    
    If the given key is found in the OSA_iniDic, the associated value is
    replaced by the provided one. If the key cannot be found in the
    OSA_iniDic, it is added to it.
    
    It is Ok to provide a NULL value for val, but NULL values for the OSA_iniDic
    or the key are considered as errors: the function will return immediately
    in such a case.
    
    Notice that if you dictionary_set a variable to NULL, a call to
    dictionary_get will return a NULL value: the variable will be found, and
    its value (NULL) is returned. In other words, setting the variable
    content to NULL is equivalent to deleting the variable from the
    OSA_iniDic. It is not possible (in this implementation) to have a key in
    the OSA_iniDic without value.
    
    This function returns non-zero in case of failure.
 *****************************************************************************
*/
int dictionary_set(OSA_iniDic * d, char * key, char * val)
{
	int			i ;
	unsigned	hash ;

	if (d==NULL || key==NULL) return -1 ;

	/* Compute hash for this key */
	hash = dictionary_hash(key) ;
	/* Find if value is already in OSA_iniDic */
	if (d->n>0) {
		for (i=0 ; i<d->size ; i++) {
			if (d->key[i]==NULL)
				continue ;
			if (hash==d->hash[i]) { /* Same hash value */
				if (!strcmp(key, d->key[i])) {	 /* Same key */
					/* Found a value: modify and return */
					if (d->val[i]!=NULL)
						free(d->val[i]);
					d->val[i] = val ? xstrdup(val) : NULL ;
					/* Value has been modified: return */
					return 0 ;
				}
			}
		}
	}
	/* Add a new value */
	/* See if OSA_iniDic needs to grow */
	if (d->n==d->size) {

		/* Reached maximum size: reallocate OSA_iniDic */
		d->val	= (char **)mem_double(d->val,  d->size * sizeof(char*)) ;
		d->key	= (char **)mem_double(d->key,  d->size * sizeof(char*)) ;
		d->hash = (unsigned int *)mem_double(d->hash, d->size * sizeof(unsigned)) ;
		if ((d->val==NULL) || (d->key==NULL) || (d->hash==NULL)) {
			/* Cannot grow OSA_iniDic */
			return -1 ;
		}
		/* Double size */
		d->size *= 2 ;
	}

	/* Insert key in the first empty slot */
	for (i=0 ; i<d->size ; i++) {
		if (d->key[i]==NULL) {
			/* Add key here */
			break ;
		}
	}
	/* Copy key */
	d->key[i]  = xstrdup(key);
	d->val[i]  = val ? xstrdup(val) : NULL ;
	d->hash[i] = hash;
	d->n ++ ;
	return 0 ;
}



/*
 ******************************************************************************
 SOURCE CODE - dictionary_unset() routine BEGINS HERE
 ******************************************************************************
 // Function Name: dictionary_unset()
 // Argument:
    a) Input: d    OSA_iniDic object to modify.
    		  key  Key to remove.
    
    b) Output:	void
 // Description  :
    Delete a key in a OSA_iniDic
    
    This function deletes a key in a OSA_iniDic. Nothing is done if the
    key cannot be found.
 *****************************************************************************
*/
void dictionary_unset(OSA_iniDic * d, char * key)
{
	unsigned	hash ;
	int			i ;

	if (key == NULL) {
		return;
	}

	hash = dictionary_hash(key);
	for (i=0 ; i<d->size ; i++) {
		if (d->key[i]==NULL)
			continue ;
				/* Compare hash */
		if (hash==d->hash[i]) {
				/* Compare string, to avoid hash collisions */
				if (!strcmp(key, d->key[i])) {
					/* Found key */
					break ;
			}
		}
	}
		if (i>=d->size)
				/* Key not found */
				return ;

		free(d->key[i]);
		d->key[i] = NULL ;
		if (d->val[i]!=NULL) {
				free(d->val[i]);
				d->val[i] = NULL ;
		}
		d->hash[i] = 0 ;
		d->n -- ;
		return ;
}



/*
 ******************************************************************************
 SOURCE CODE - dictionary_dump() routine BEGINS HERE
 ******************************************************************************
 // Function Name: dictionary_dump()
 // Argument:
    a) Input:d Dictionary to dump
             f Opened file pointer.
    
    b) Output:	void
 // Description  :
    Dump a OSA_iniDic to an opened file pointer.
    
    Dumps a OSA_iniDic onto an opened file pointer. Key pairs are printed out
    as @c [Key]=[Value], one per line. It is Ok to provide stdout or stderr as
    output file pointers.
 *****************************************************************************
*/
void dictionary_dump(OSA_iniDic * d, FILE * out)
{
	int		i ;

	if (d==NULL || out==NULL) return ;
	if (d->n<1) {
		fprintf(out, "empty OSA_iniDic\n");
		return ;
	}
	for (i=0 ; i<d->size ; i++) {
		if (d->key[i]) {
			fprintf(out, "%20s\t[%s]\n",
					d->key[i],
					d->val[i] ? d->val[i] : "UNDEF");
		}
	}
	return ;
}

#if 0
/* Test code */
#ifdef TESTDIC
#define NVALS 20000
int main(int argc, char *argv[])
{
	OSA_iniDic	*	d ;
	char	*	val ;
	int			i ;
	char		cval[90] ;

	/* Allocate OSA_iniDic */
	printf("allocating...\n");
	d = dictionary_new(0);

	/* Set values in OSA_iniDic */
	printf("setting %d values...\n", NVALS);
	for (i=0 ; i<NVALS ; i++) {
		sprintf(cval, "%04d", i);
		dictionary_set(d, cval, "salut");
	}
	printf("getting %d values...\n", NVALS);
	for (i=0 ; i<NVALS ; i++) {
		sprintf(cval, "%04d", i);
		val = dictionary_get(d, cval, DICT_INVALID_KEY);
		if (val==DICT_INVALID_KEY) {
			printf("cannot get value for key [%s]\n", cval);
		}
	}
		printf("unsetting %d values...\n", NVALS);
	for (i=0 ; i<NVALS ; i++) {
		sprintf(cval, "%04d", i);
		dictionary_unset(d, cval);
	}
		if (d->n != 0) {
				printf("error deleting values\n");
		}
	printf("deallocating...\n");
	dictionary_del(d);
	return 0 ;
}
#endif
/* vim: set ts=4 et sw=4 tw=75 */
#endif



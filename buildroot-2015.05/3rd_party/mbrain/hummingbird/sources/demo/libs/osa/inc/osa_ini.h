/**
 * @file		osa_ini.h
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date	 2015.07.12
 * @brief	 INI helper APIs
 *
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain & MidmeSoft
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain & MidmeSoft
 * 
 * (c) Copyright mBrain & MidmeSoft(2015).	 All Rights Reserved.
 */

/*! [ 1]. File classification */
#ifndef _OSA_INI_PARSER_H_
#define _OSA_INI_PARSER_H_

/*! [ 2]. File Directive (#include) */
#include <osa.h>
#include <stdio.h>

/*! [ 3]. Global macro and enum */
/*< For backwards compatibility only */
#define OSA_iniPsr_getstr(d, k)  	OSA_iniPsr_getstring(d, k, NULL)
#define OSA_iniPsr_setstr 			 OSA_iniPsr_setstring

/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
/*
 *@brief	Dictionary object
 *
 *This object contains a list of string/string associations. Each
 *association is identified by a unique string key. Looking up values
 *in the OSA_iniDic is speeded up by the use of a (hopefully collision-free)
 *hash function.
 */

typedef struct _dictionary_ {
	int				n ;		/** Number of entries in OSA_iniDic */
	int				size ;	/** Storage size */
	char		**	val ;	/** List of string values */
	char		**	key ;	/** List of string keys */
	unsigned	 *	hash ;	/** List of hash values for keys */
} OSA_iniDic ;

/*! [ 5]. Global Function prototype declaration */
unsigned dictionary_hash(char * key);
OSA_iniDic * dictionary_new(int size);
void dictionary_del(OSA_iniDic * vd);
char * dictionary_get(OSA_iniDic * d, char * key, char * def);
int dictionary_set(OSA_iniDic * vd, char * key, char * val);
void dictionary_unset(OSA_iniDic * d, char * key);
void dictionary_dump(OSA_iniDic * d, FILE * out);

int OSA_iniPsr_getnsec(OSA_iniDic * d);
char * OSA_iniPsr_getsecname(OSA_iniDic * d, int n);
void OSA_iniPsr_dump_ini(OSA_iniDic * d, FILE * f);
void OSA_iniPsr_dump(OSA_iniDic * d, FILE * f);
char * OSA_iniPsr_getstring(OSA_iniDic * d, const char * key, char * def);
int OSA_iniPsr_getint(OSA_iniDic * d, const char * key, int notfound);
double OSA_iniPsr_getdouble(OSA_iniDic * d, char * key, double notfound);
int OSA_iniPsr_getboolean(OSA_iniDic * d, const char * key, int notfound);
int OSA_iniPsr_setstring(OSA_iniDic * ini, char * entry, char * val);
void OSA_iniPsr_unset(OSA_iniDic * ini, char * entry);
int OSA_iniPsr_find_entry(OSA_iniDic * ini, char * entry) ;
OSA_iniDic * OSA_iniPsr_load(const char * ininame);
void OSA_iniPsr_freedict(OSA_iniDic * d);


/*! [ 6]. Global Variable declaration  */

#endif //_OSA_INI_PARSER_H_

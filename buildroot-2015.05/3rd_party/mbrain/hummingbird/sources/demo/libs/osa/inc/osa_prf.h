/**
 * @file		osa_prf.h
 * @Author 	    Joel,Kim (Joel.Kim@mbrain.org)
 * @date		2015.07.12
 * @brief		Profiling helper APIs
 * 
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain & MidmeSoft
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain & MidmeSoft
 * 
 * (c) Copyright mBrain & MidmeSoft(2015).		All Rights Reserved.
 */

/*! [ 1]. File classification */
#ifndef _OSA_PRF_H_
#define _OSA_PRF_H_

/*! [ 2]. File Directive (#include) */
#include <osa.h>


/*! [ 3]. Global macro and enum */
#define OSA_PRF_PRINT_DEFAULT 		(OSA_PRF_PRINT_TIME|OSA_PRF_PRINT_VALUE)

#define OSA_PRF_PRINT_ALL 			(0xFFFF)

#define OSA_PRF_PRINT_TIME			(0x0001)
#define OSA_PRF_PRINT_VALUE 		(0x0002)
#define OSA_PRF_PRINT_MIN_MAX 		(0x0004)
#define OSA_PRF_PRINT_COUNT 		(0x0008)


/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */
/*
 * @brief: Profile info	: <name>
 * ======================
 * Iterations		:
 * Avg Time (ms) :
 * Max Time (ms) :
 * Min Time (ms) :
 * Avg Value 		:
 * Avg Value/sec :
 * Max Value 		:
 * Min Value 		:
 */
#ifdef OSA_PRF_ENABLE
typedef struct {

	Uint32 totalTime;
	Uint32 maxTime;
	Uint32 minTime;

	Uint32 totalValue;
	Uint32 maxValue;
	Uint32 minValue;

	Uint32 count;
	Uint32 curTime;
	Uint32 curValue;

} OSA_PrfHndl;
#endif


/*! [ 5]. Global Function prototype declaration */
#ifdef OSA_PRF_ENABLE
	int OSA_prfBegin(OSA_PrfHndl *hndl);
	int OSA_prfEnd(OSA_PrfHndl *hndl, Uint32 value);
	int OSA_prfReset(OSA_PrfHndl *hndl);
	int OSA_prfPrint(OSA_PrfHndl *hndl, char *name, Uint32 flags);
#else
#define OSA_prfBegin(hndl)
#define OSA_prfEnd(hndl, value)
#define OSA_prfReset(hndl)
#define OSA_prfPrint(hndl, name, flags)
#endif


/*! [ 6]. Global Variable declaration  */



#endif /* _OSA_PRF_H_ */


/**
 * @file		osa.h
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.07.12
 * @brief  OSAL main header file
 *
 * Detailed description of file.
 * <Reserved>
 *
 * @copyright
 * mBrain CONFIDENTIAL AND PROPRIETARY
 *
 * This source is the sole property of mBrain & MidmeSoft
 * Reproduction or utilization of this source in whole or in part is forbi-
 * dden without the written consent of mBrain & MidmeSoft
 * 
 * (c) Copyright mBrain & MidmeSoft(2015).  All Rights Reserved.
 */


/*! [ 1]. File classification */
#ifndef _OSA_H_
#define _OSA_H_

/*! [ 2]. File Directive (#include) */
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>



/*! [ 3]. Global macro and enum */

#define OSA_DEBUG_MODE // enable OSA_printf, OSA_assert
#define OSA_DEBUG_FILE // enable printf's during OSA_fileXxxx
#define OSA_PRF_ENABLE // enable profiling APIs

#define OSA_SOK 		 0	///< Status : OK
#define OSA_EFAIL 	-1	///< Status : Generic error

#ifndef _3RDPTY_STD_TYPES
#define _3RDPTY_STD_TYPES

#ifndef TRUE

typedef short 	Bool; 						///< Boolean type

#define TRUE		((Bool) 1)				///< Boolean value : TRUE
#define FALSE		((Bool) 0)				///< Boolean value : FALSE

#endif

/* unsigned quantities */
typedef unsigned long long Uint64;			///< Unsigned 64-bit integer
typedef unsigned int Uint32;				///< Unsigned 32-bit integer
typedef unsigned short Uint16;				///< Unsigned 16-bit integer
typedef unsigned char Uint8;				///< Unsigned  8-bit integer

/* signed quantities */
typedef long long Int64;					///< Signed 64-bit integer
typedef int Int32;							///< Signed 32-bit integer
typedef short Int16;						///< Signed 16-bit integer
typedef char Int8;							///< Signed	8-bit integer

typedef signed char 			__s8;
typedef unsigned char 			__u8;
typedef signed short			__s16;
typedef unsigned short			__u16;
typedef signed int				__s32;
typedef unsigned int			__u32;
typedef signed long long		__s64;
typedef unsigned long long		__u64;


#endif /* _3RDPTY_STD_TYPES */



#ifndef KB
#define KB ((Uint32)1024)
#endif

#ifndef MB
#define MB (KB*KB)
#endif


#define PRINTF_GRAY_CLR             "\033[1;30m"  
#define PRINTF_NORMAL_CLR           "\033[0m"  
#define PRINTF_RED_CLR              "\033[1;31m"  
#define PRINTF_GREEN_CLR            "\033[1;32m"  
#define PRINTF_MAGENTA_CLR          "\033[1;35m"  
#define PRINTF_YELLOW_CLR           "\033[1;33m"  
#define PRINTF_BLUE_CLR             "\033[1;34m"  
#define PRINTF_DBLUE_CLR            "\033[0;34m"  
#define PRINTF_WHITE_CLR            "\033[1;37m"  
#define PRINTF_COLORERR_CLR         "\033[1;37;41m"  
#define PRINTF_COLORWRN_CLR         "\033[0;31m"  
#define PRINTF_BROWN_CLR            "\033[0;40;33m"  
#define PRINTF_CYAN_CLR             "\033[0;40;36m"  
#define PRINTF_LIGHTGRAY_CLR        "\033[1;40;37m"  
#define PRINTF_BRIGHTRED_CLR        "\033[0;40;31m"  
#define PRINTF_BRIGHTBLUE_CLR       "\033[0;40;34m"  
#define PRINTF_BRIGHTMAGENTA_CLR    "\033[0;40;35m"  
#define PRINTF_BRIGHTCYAN_CLR       "\033[0;40;36m"



#define OSA_TIMEOUT_NONE			((Uint32) 0)	// no timeout
#define OSA_TIMEOUT_FOREVER 		((Uint32)-1)	// wait forever

#define OSA_memAlloc(size)			(void*)malloc((size))
#define OSA_memFree(ptr)			free(ptr)
#define OSA_memClean(buf)			memset(&(buf), 0, sizeof (buf))

#define OSA_align(value, align) 	((( (value) + ( (align) - 1 ) ) / (align) ) * (align) )

#define OSA_floor(value, align) 	(( (value) / (align) ) * (align) )
#define OSA_ceil(value, align)		OSA_align(value, align)

#define OSA_max(a, b) 	 ( (a) > (b) ? (a) : (b) )
#define OSA_min(a, b) 	 ( (a) < (b) ? (a) : (b) )

#include <osa_debug.h>
#include <osa_prf.h>


/*! [ 4]. Global Type definition or structure declaration (typedef or struct) */


/*! [ 5]. Global Function prototype declaration */

unsigned long long OSA_getCurTimeInMsec(void);
unsigned long long OSA_getTvalInMsec(struct timeval tv);
void	 OSA_waitUsecs(Uint32 usecs);
void	 OSA_waitMsecs(Uint32 msecs);
int 	 OSA_attachSignalHandler(int sigId, void (*handler)(int ) );
int 	 OSA_getHostName(char *hostname, int maxHostNameLen);



int xstrtoi(char *hex);

int OSA_init();
int OSA_exit();


/*! [ 6]. Global Variable declaration  */


#endif //_OSA_H_


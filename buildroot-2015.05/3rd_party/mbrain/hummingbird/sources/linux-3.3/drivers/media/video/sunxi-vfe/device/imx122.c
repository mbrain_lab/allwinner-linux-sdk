/**
 * @file   imx122.c
 * @Author Joel,Kim (Joel.Kim@mbrain.org)
 * @date   2015.07.14
 * @brief  V4L2 driver for IMX122 raw image sensor
 *
 * Detailed description of file.
 *
 * @copyright
 * (c) Copyright MBRAIN(2015).  All Rights Reserved.
 */

/*! [ 1]. File Directive (#include) */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/videodev2.h>
#include <linux/clk.h>
#include <media/v4l2-device.h>
#include <media/v4l2-chip-ident.h>
#include <media/v4l2-mediabus.h>
#include <linux/io.h>

#include "camera.h"


/*! [ 2]. Local Macro (#define, enum) */
MODULE_AUTHOR("JoelKim");
MODULE_DESCRIPTION("A low-level driver for IMX122 Raw sensors");
MODULE_LICENSE("GPL");

/*! for internel driver debug */
#define DEV_DBG_EN      1 
#if(DEV_DBG_EN == 1)    
#define vfe_dev_dbg(x,arg...) printk("[IMX122 Raw]"x,##arg)
#else
#define vfe_dev_dbg(x,arg...) 
#endif
#define vfe_dev_err(x,arg...) printk("[IMX122 Raw]"x,##arg)
#define vfe_dev_print(x,arg...) printk("[IMX122 Raw]"x,##arg)

#define LOG_ERR_RET(x)  { \
                          int ret;  \
                          ret = x; \
                          if(ret < 0) {\
                            vfe_dev_err("error at %s\n",__func__);  \
                            return ret; \
                          } \
                        }

/*! define module timing */
#define MCLK              (24*1000*1000)
#define VREF_POL          V4L2_MBUS_VSYNC_ACTIVE_HIGH
#define HREF_POL          V4L2_MBUS_HSYNC_ACTIVE_HIGH
#define CLK_POL           V4L2_MBUS_PCLK_SAMPLE_RISING
#define V4L2_IDENT_SENSOR  0x0122

/*! define the voltage level of control signal */
#define CSI_STBY_ON     1
#define CSI_STBY_OFF    0
#define CSI_RST_ON      0
#define CSI_RST_OFF     1
#define CSI_PWR_ON      1
#define CSI_PWR_OFF     0
#define CSI_AF_PWR_ON   1
#define CSI_AF_PWR_OFF  0

#define CSI_SPI_CS_HIGH 1
#define CSI_SPI_CS_LOW  0


#define REG_TERM 0xfffe
#define VAL_TERM 0xfe
#define REG_DLY  0xffff

/*! Our nominal (default) frame rate. */
#ifdef FPGA
#define SENSOR_FRAME_RATE 15
#else
#define SENSOR_FRAME_RATE 30
#endif


/*! The ov2710 i2c address */
#define IMX122_WRITE_ADDR (0x6c)
#define IMX_READ_ADDR  (0x6d)


/*! [ 3]. Type definition or structure declaration (typedef or struct) */
/**!
  * Sony IMX122/222/322 type register arrary format
  */
struct ImxX22Reg{
	u8 devid;
	u8 reg_addr;
	u8 reg_val;
};


typedef struct {
	int reg;
	int db;
} imxX22GainTbl;


/**! 
  * Private SPI bus driver 
  */
struct spi_device *glb_spiBus;



/**! [ 4]. Local Function prototype declaration */
/**! [ 5]. Local Variable declaration */
static struct v4l2_subdev *glb_sd;

/**! Information we maintain about a known sensor. */
struct sensor_format_struct;  /* coming later */



static inline struct sensor_info *to_state(struct v4l2_subdev *sd)
{
  return container_of(sd, struct sensor_info, sd);
}

#define SENIF_TEST
/**!
  * The default register settings
  */
static struct ImxX22Reg sensor_default_regs[] = 
{
	{0x02, 0x00, 0x31},
	{0x02, 0x01, 0x00},
	{0x02, 0x02, 0x0f},
	{0x02, 0x03, 0x4c},
	{0x02, 0x04, 0x04},
	{0x02, 0x05, 0x65},
	{0x02, 0x06, 0x04},
	{0x02, 0x07, 0x00},
	#ifdef SENIF_TEST
	{0x02, 0x08, 0x19},  /**< 8.3 ms */
	{0x02, 0x09, 0x01},
	#else
	{0x02, 0x08, 0x00},
	{0x02, 0x09, 0x00},
	#endif
	{0x02, 0x0a, 0x00},
	{0x02, 0x0b, 0x00},
	{0x02, 0x0c, 0x00},
	{0x02, 0x0d, 0x00},
	{0x02, 0x0e, 0x00},
	{0x02, 0x0f, 0x00},
	{0x02, 0x10, 0x00},
	{0x02, 0x11, 0x00},
	{0x02, 0x12, 0x82},
	{0x02, 0x13, 0x40},
	{0x02, 0x14, 0x00},
	{0x02, 0x15, 0x01},
	{0x02, 0x16, 0x3c},
	{0x02, 0x17, 0x00},
	{0x02, 0x18, 0xc0},
	{0x02, 0x19, 0x07},
	{0x02, 0x1a, 0x51},
	{0x02, 0x1b, 0x04},
	{0x02, 0x1c, 0x50},
	{0x02, 0x1d, 0x00},
	#ifdef SENIF_TEST
	{0x02, 0x1e, 0x28},	/**< 12 dB */
	#else
	{0x02, 0x1e, 0x00},
	#endif
	{0x02, 0x1f, 0x31},
	{0x02, 0x20, 0x00},	/**< Black level: 0x4C, 0x3C */
	{0x02, 0x21, 0x20},
	{0x02, 0x22, 0x02},
	{0x02, 0x23, 0x08},
	{0x02, 0x24, 0x30},
	{0x02, 0x25, 0x00},
	{0x02, 0x26, 0x80},
	{0x02, 0x27, 0x20},
	{0x02, 0x28, 0x34},
	{0x02, 0x29, 0x63},
	{0x02, 0x2a, 0x00},
	{0x02, 0x2b, 0x00},
	{0x02, 0x2c, 0x00},
	{0x02, 0x2d, 0x40},
	{0x02, 0x2e, 0x00},
	{0x02, 0x2f, 0x02},
	{0x02, 0x30, 0x30},
	{0x02, 0x31, 0x20},
	{0x02, 0x32, 0x00},
	{0x02, 0x33, 0x14},
	{0x02, 0x34, 0x20},
	{0x02, 0x35, 0x60},
	{0x02, 0x36, 0x00},
	{0x02, 0x37, 0x23},
	{0x02, 0x38, 0x01},
	{0x02, 0x39, 0x00},
	{0x02, 0x3a, 0xa8},
	{0x02, 0x3b, 0xe0},
	{0x02, 0x3c, 0x06},
	{0x02, 0x3d, 0x00},
	{0x02, 0x3e, 0x10},
	{0x02, 0x3f, 0x00},
	{0x02, 0x40, 0x42},
	{0x02, 0x41, 0x23},
	{0x02, 0x42, 0x3c},
	{0x02, 0x43, 0x01},
	{0x02, 0x44, 0x00},
	{0x02, 0x45, 0x00},
	{0x02, 0x46, 0x00},
	{0x02, 0x47, 0x00},
	{0x02, 0x48, 0x00},
	{0x02, 0x49, 0x00},
	{0x02, 0x4a, 0x00},
	{0x02, 0x4b, 0x00},
	{0x02, 0x4c, 0x01},
	{0x02, 0x4d, 0x00},
	{0x02, 0x4e, 0x01},
	{0x02, 0x4f, 0x07},
	{0x02, 0x50, 0x10},
	{0x02, 0x51, 0x18},
	{0x02, 0x52, 0x12},
	{0x02, 0x53, 0x00},
	{0x02, 0x54, 0x00},
	{0x02, 0x55, 0x00},
	{0x02, 0x56, 0x00},
	{0x02, 0x57, 0x00},
	{0x02, 0x58, 0xe0},
	{0x02, 0x59, 0x01},
	{0x02, 0x5a, 0xe0},
	{0x02, 0x5b, 0x01},
	{0x02, 0x5c, 0x00},
	{0x02, 0x5d, 0x00},
	{0x02, 0x5e, 0x00},
	{0x02, 0x5f, 0x00},
	{0x02, 0x60, 0x00},
	{0x02, 0x61, 0x00},
	{0x02, 0x62, 0x76},
	{0x02, 0x63, 0x00},
	{0x02, 0x64, 0x01},
	{0x02, 0x65, 0x00},
	{0x02, 0x66, 0x00},
	{0x02, 0x67, 0x00},
	{0x02, 0x68, 0x00},
	{0x02, 0x69, 0x00},
	{0x02, 0x6a, 0x00},
	{0x02, 0x6b, 0x00},
	{0x02, 0x6c, 0x00},
	{0x02, 0x6d, 0x00},
	{0x02, 0x6e, 0x00},
	{0x02, 0x6f, 0x00},
	{0x02, 0x70, 0x00},
	{0x02, 0x71, 0x00},
	{0x02, 0x72, 0x00},
	{0x02, 0x73, 0x01},
	{0x02, 0x74, 0x06},
	{0x02, 0x75, 0x07},
	{0x02, 0x76, 0x80},
	{0x02, 0x77, 0x00},
	{0x02, 0x78, 0x40},
	{0x02, 0x79, 0x08},
	{0x02, 0x7a, 0x00},
	{0x02, 0x7b, 0x00},
	{0x02, 0x7c, 0x10},
	{0x02, 0x7d, 0x00},
	{0x02, 0x7e, 0x00},
	{0x02, 0x7f, 0x00},
	{0x02, 0x80, 0x06},
	{0x02, 0x81, 0x19},
	{0x02, 0x82, 0x00},
	{0x02, 0x83, 0x64},
	{0x02, 0x84, 0x00},
	{0x02, 0x85, 0x01},
	{0x02, 0x86, 0x00},
	{0x02, 0x87, 0x00},
	{0x02, 0x88, 0x00},
	{0x02, 0x89, 0x00},
	{0x02, 0x8a, 0x00},
	{0x02, 0x8b, 0x00},
	{0x02, 0x8c, 0x00},
	{0x02, 0x8d, 0x00},
	{0x02, 0x8e, 0x00},
	{0x02, 0x8f, 0x00},
	{0x02, 0x90, 0x00},
	{0x02, 0x91, 0x00},
	{0x02, 0x92, 0x01},
	{0x02, 0x93, 0x01},
	{0x02, 0x94, 0x00},
	{0x02, 0x95, 0xff},
	{0x02, 0x96, 0x0f},
	{0x02, 0x97, 0x00},
	{0x02, 0x98, 0x26},
	{0x02, 0x99, 0x02},
	{0x02, 0x9a, 0x26},
	{0x02, 0x9b, 0x02},
	{0x02, 0x9c, 0x9c},
	{0x02, 0x9d, 0x01},
	{0x02, 0x9e, 0x39},
	{0x02, 0x9f, 0x03},
	{0x02, 0xa0, 0x01},
	{0x02, 0xa1, 0x05},
	{0x02, 0xa2, 0xd0},
	{0x02, 0xa3, 0x07},
	{0x02, 0xa4, 0x00},
	{0x02, 0xa5, 0x02},
	{0x02, 0xa6, 0x0b},
	{0x02, 0xa7, 0x0f},
	{0x02, 0xa8, 0x24},
	{0x02, 0xa9, 0x00},
	{0x02, 0xaa, 0x28},
	{0x02, 0xab, 0x00},
	{0x02, 0xac, 0xe8},
	{0x02, 0xad, 0x04},
	{0x02, 0xae, 0xec},
	{0x02, 0xaf, 0x04},
	{0x02, 0xb0, 0x00},
	{0x02, 0xb1, 0x00},
	{0x02, 0xb2, 0x03},
	{0x02, 0xb3, 0x05},
	{0x02, 0xb4, 0x00},
	{0x02, 0xb5, 0x0f},
	{0x02, 0xb6, 0x10},
	{0x02, 0xb7, 0x00},
	{0x02, 0xb8, 0x28},
	{0x02, 0xb9, 0x00},
	{0x02, 0xba, 0xbf},
	{0x02, 0xbb, 0x07},
	{0x02, 0xbc, 0xcf},
	{0x02, 0xbd, 0x07},
	{0x02, 0xbe, 0xcf},
	{0x02, 0xbf, 0x07},
	{0x02, 0xc0, 0xcf},
	{0x02, 0xc1, 0x07},
	{0x02, 0xc2, 0xd0},
	{0x02, 0xc3, 0x07},
	{0x02, 0xc4, 0x01},
	{0x02, 0xc5, 0x02},
	{0x02, 0xc6, 0x03},
	{0x02, 0xc7, 0x04},
	{0x02, 0xc8, 0x05},
	{0x02, 0xc9, 0x06},
	{0x02, 0xca, 0x07},
	{0x02, 0xcb, 0x08},
	{0x02, 0xcc, 0x01},
	{0x02, 0xcd, 0x03},
	{0x02, 0xce, 0x16},
	{0x02, 0xcf, 0x82},
	{0x02, 0xd0, 0x00},
	{0x02, 0xd1, 0x00},
	{0x02, 0xd2, 0x00},
	{0x02, 0xd3, 0x00},
	{0x02, 0xd4, 0x00},
	{0x02, 0xd5, 0x00},
	{0x02, 0xd6, 0x00},
	{0x02, 0xd7, 0x00},
	{0x02, 0xd8, 0x00},
	{0x02, 0xd9, 0x00},
	{0x02, 0xda, 0x00},
	{0x02, 0xdb, 0x00},
	{0x02, 0xdc, 0x00},
	{0x02, 0xdd, 0x00},
	{0x02, 0xde, 0x00},
	{0x02, 0xdf, 0x00},
	{0x02, 0xe0, 0x00},
	{0x02, 0xe1, 0x00},
	{0x02, 0xe2, 0x00},
	{0x02, 0xe3, 0x00},
	{0x02, 0xe4, 0x00},
	{0x02, 0xe5, 0x00},
	{0x02, 0xe6, 0x00},
	{0x02, 0xe7, 0x00},
	{0x02, 0xe8, 0x00},
	{0x02, 0xe9, 0x00},
	{0x02, 0xea, 0x00},
	{0x02, 0xeb, 0x00},
	{0x02, 0xec, 0x00},
	{0x02, 0xed, 0x00},
	{0x02, 0xee, 0x00},
	{0x02, 0xef, 0x00},
	{0x02, 0xf0, 0x00},
	{0x02, 0xf1, 0x00},
	{0x02, 0xf2, 0x00},
	{0x02, 0xf3, 0x00},
	{0x02, 0xf4, 0x00},
	{0x02, 0xf5, 0x00},
	{0x02, 0xf6, 0x00},
	{0x02, 0xf7, 0x00},
	{0x02, 0xf8, 0x00},
	{0x02, 0xf9, 0x00},
	{0x02, 0xfa, 0x00},
	{0x02, 0xfb, 0x00},
	{0x02, 0xfc, 0x00},
	{0x02, 0xfd, 0x00},
	{0x02, 0xfe, 0x00},
	{0x02, 0xff, 0x00},
	{0x03, 0x00, 0x00},
	{0x03, 0x01, 0x00},
	{0x03, 0x02, 0x00},
	{0x03, 0x03, 0x00},
	{0x03, 0x04, 0x00},
	{0x03, 0x05, 0x00},
	{0x03, 0x06, 0x00},
	{0x03, 0x07, 0xfa},
	{0x03, 0x08, 0xfa},
	{0x03, 0x09, 0x41},
	{0x03, 0x0a, 0x31},
	{0x03, 0x0b, 0x38},
	{0x03, 0x0c, 0x04},
	{0x03, 0x0d, 0x00},
	{0x03, 0x0e, 0x1a},
	{0x03, 0x0f, 0x10},
	{0x03, 0x10, 0x00},
	{0x03, 0x11, 0x00},
	{0x03, 0x12, 0x10},
	{0x03, 0x13, 0x00},
	{0x03, 0x14, 0x00},
	{0x03, 0x15, 0x06},
	{0x03, 0x16, 0x33},
	{0x03, 0x17, 0x0d},
	{0x03, 0x18, 0x00},
	{0x03, 0x19, 0x00},
	{0x03, 0x1a, 0x00},
	{0x03, 0x1b, 0x00},
	{0x03, 0x1c, 0x00},
	{0x03, 0x1d, 0x00},
	{0x03, 0x1e, 0x00},
	{0x03, 0x1f, 0x00},
	{0x03, 0x20, 0x00},
	{0x03, 0x21, 0x80},
	{0x03, 0x22, 0x0c},
	{0x03, 0x23, 0x00},
	{0x03, 0x24, 0x00},
	{0x03, 0x25, 0x00},
	{0x03, 0x26, 0x00},
	{0x03, 0x27, 0x00},
	{0x03, 0x28, 0x05},
	{0x03, 0x29, 0x80},
	{0x03, 0x2a, 0x00},
	{0x03, 0x2b, 0x00},
	{0x03, 0x2c, 0x04},
	{0x03, 0x2d, 0x04},
	{0x03, 0x2e, 0x00},
	{0x03, 0x2f, 0x00},
	{0x03, 0x30, 0x9b},
	{0x03, 0x31, 0x71},
	{0x03, 0x32, 0x33},
	{0x03, 0x33, 0x37},
	{0x03, 0x34, 0xb3},
	{0x03, 0x35, 0x19},
	{0x03, 0x36, 0x97},
	{0x03, 0x37, 0xb1},
	{0x03, 0x38, 0x19},
	{0x03, 0x39, 0x01},
	{0x03, 0x3a, 0x50},
	{0x03, 0x3b, 0x00},
	{0x03, 0x3c, 0x35},
	{0x03, 0x3d, 0xb0},
	{0x03, 0x3e, 0x03},
	{0x03, 0x3f, 0xd1},
	{0x03, 0x40, 0x71},
	{0x03, 0x41, 0x1d},
	{0x03, 0x42, 0x00},
	{0x03, 0x43, 0x00},
	{0x03, 0x44, 0x00},
	{0x03, 0x45, 0x00},
	{0x03, 0x46, 0x02},
	{0x03, 0x47, 0x30},
	{0x03, 0x48, 0x00},
	{0x03, 0x49, 0x00},
	{0x03, 0x4a, 0x00},
	{0x03, 0x4b, 0x03},
	{0x03, 0x4c, 0x00},
	{0x03, 0x4d, 0x02},
	{0x03, 0x4e, 0x10},
	{0x03, 0x4f, 0xa0},
	{0x03, 0x50, 0x00},
	{0x03, 0x51, 0x07},
	{0x03, 0x52, 0x40},
	{0x03, 0x53, 0x80},
	{0x03, 0x54, 0x00},
	{0x03, 0x55, 0x02},
	{0x03, 0x56, 0x50},
	{0x03, 0x57, 0x02},
	{0x03, 0x58, 0x23},
	{0x03, 0x59, 0xe4},
	{0x03, 0x5a, 0x45},
	{0x03, 0x5b, 0x33},
	{0x03, 0x5c, 0x79},
	{0x03, 0x5d, 0xd1},
	{0x03, 0x5e, 0xcc},
	{0x03, 0x5f, 0x2f},
	{0x03, 0x60, 0xb6},
	{0x03, 0x61, 0xa1},
	{0x03, 0x62, 0x17},
	{0x03, 0x63, 0xcb},
	{0x03, 0x64, 0xe8},
	{0x03, 0x65, 0xc5},
	{0x03, 0x66, 0x32},
	{0x03, 0x67, 0xc0},
	{0x03, 0x68, 0xa8},
	{0x03, 0x69, 0xc6},
	{0x03, 0x6a, 0x5e},
	{0x03, 0x6b, 0x20},
	{0x03, 0x6c, 0x63},
	{0x03, 0x6d, 0x0d},
	{0x03, 0x6e, 0x6d},
	{0x03, 0x6f, 0x44},
	{0x03, 0x70, 0xa6},
	{0x03, 0x71, 0x32},
	{0x03, 0x72, 0x24},
	{0x03, 0x73, 0x50},
	{0x03, 0x74, 0xc4},
	{0x03, 0x75, 0x2f},
	{0x03, 0x76, 0xf4},
	{0x03, 0x77, 0x42},
	{0x03, 0x78, 0x82},
	{0x03, 0x79, 0x13},
	{0x03, 0x7a, 0x90},
	{0x03, 0x7b, 0x00},
	{0x03, 0x7c, 0x10},
	{0x03, 0x7d, 0x8a},
	{0x03, 0x7e, 0x60},
	{0x03, 0x7f, 0xc4},
	{0x03, 0x80, 0x2f},
	{0x03, 0x81, 0x84},
	{0x03, 0x82, 0xf1},
	{0x03, 0x83, 0x0b},
	{0x03, 0x84, 0xcd},
	{0x03, 0x85, 0x70},
	{0x03, 0x86, 0x42},
	{0x03, 0x87, 0x16},
	{0x03, 0x88, 0x00},
	{0x03, 0x89, 0x61},
	{0x03, 0x8a, 0x0b},
	{0x03, 0x8b, 0x29},
	{0x03, 0x8c, 0x74},
	{0x03, 0x8d, 0x81},
	{0x03, 0x8e, 0x10},
	{0x03, 0x8f, 0xba},
	{0x03, 0x90, 0x18},
	{0x03, 0x91, 0x22},
	{0x03, 0x92, 0x11},
	{0x03, 0x93, 0xe9},
	{0x03, 0x94, 0x60},
	{0x03, 0x95, 0x07},
	{0x03, 0x96, 0x09},
	{0x03, 0x97, 0xf6},
	{0x03, 0x98, 0x40},
	{0x03, 0x99, 0x02},
	{0x03, 0x9a, 0x3c},
	{0x03, 0x9b, 0x00},
	{0x03, 0x9c, 0x00},
	{0x03, 0x9d, 0x00},
	{0x03, 0x9e, 0x00},
	{0x03, 0x9f, 0x00},
	{0x03, 0xa0, 0x80},
	{0x03, 0xa1, 0x0b},
	{0x03, 0xa2, 0x64},
	{0x03, 0xa3, 0x90},
	{0x03, 0xa4, 0x8d},
	{0x03, 0xa5, 0x6e},
	{0x03, 0xa6, 0x98},
	{0x03, 0xa7, 0x40},
	{0x03, 0xa8, 0x05},
	{0x03, 0xa9, 0xd1},
	{0x03, 0xaa, 0xa8},
	{0x03, 0xab, 0x86},
	{0x03, 0xac, 0x09},
	{0x03, 0xad, 0x54},
	{0x03, 0xae, 0x10},
	{0x03, 0xaf, 0x8d},
	{0x03, 0xb0, 0x6a},
	{0x03, 0xb1, 0xe8},
	{0x03, 0xb2, 0x82},
	{0x03, 0xb3, 0x17},
	{0x03, 0xb4, 0x1c},
	{0x03, 0xb5, 0x60},
	{0x03, 0xb6, 0xc1},
	{0x03, 0xb7, 0x31},
	{0x03, 0xb8, 0xae},
	{0x03, 0xb9, 0xd1},
	{0x03, 0xba, 0x81},
	{0x03, 0xbb, 0x16},
	{0x03, 0xbc, 0x20},
	{0x03, 0xbd, 0x03},
	{0x03, 0xbe, 0x1b},
	{0x03, 0xbf, 0x24},
	{0x03, 0xc0, 0xe0},
	{0x03, 0xc1, 0xc1},
	{0x03, 0xc2, 0x33},
	{0x03, 0xc3, 0xbe},
	{0x03, 0xc4, 0x51},
	{0x03, 0xc5, 0x82},
	{0x03, 0xc6, 0x1e},
	{0x03, 0xc7, 0x40},
	{0x03, 0xc8, 0x03},
	{0x03, 0xc9, 0x1c},
	{0x03, 0xca, 0x34},
	{0x03, 0xcb, 0xd0},
	{0x03, 0xcc, 0x81},
	{0x03, 0xcd, 0x02},
	{0x03, 0xce, 0x16},
	{0x03, 0xcf, 0x00},
	{0x03, 0xd0, 0x02},
	{0x03, 0xd1, 0x04},
	{0x03, 0xd2, 0x00},
	{0x03, 0xd3, 0x00},
	{0x03, 0xd4, 0x00},
	{0x03, 0xd5, 0x80},
	{0x03, 0xd6, 0x00},
	{0x03, 0xd7, 0x00},
	{0x03, 0xd8, 0x23},
	{0x03, 0xd9, 0x01},
	{0x03, 0xda, 0x03},
	{0x03, 0xdb, 0x02},
	{0x03, 0xdc, 0x00},
	{0x03, 0xdd, 0x00},
	{0x03, 0xde, 0x00},
	{0x03, 0xdf, 0x00},
	{0x03, 0xe0, 0x22},
	{0x03, 0xe1, 0x00},
	{0x03, 0xe2, 0x00},
	{0x03, 0xe3, 0x00},
	{0x03, 0xe4, 0x3f},
	{0x03, 0xe5, 0x17},
	{0x03, 0xe6, 0x15},
	{0x03, 0xe7, 0x00},
	{0x03, 0xe8, 0x00},
	{0x03, 0xe9, 0x00},
	{0x03, 0xea, 0x00},
	{0x03, 0xeb, 0x00},
	{0x03, 0xec, 0x00},
	{0x03, 0xed, 0x00},
	{0x03, 0xee, 0x00},
	{0x03, 0xef, 0x00},
	{0x03, 0xf0, 0x00},
	{0x03, 0xf1, 0x00},
	{0x03, 0xf2, 0x00},
	{0x03, 0xf3, 0x00},
	{0x03, 0xf4, 0x00},
	{0x03, 0xf5, 0x00},
	{0x03, 0xf6, 0x00},
	{0x03, 0xf7, 0x00},
	{0x03, 0xf8, 0x00},
	{0x03, 0xf9, 0x00},
	{0x03, 0xfa, 0x00},
	{0x03, 0xfb, 0x00},
	{0x03, 0xfc, 0x00},
	{0x03, 0xfd, 0x00},
	{0x03, 0xfe, 0x00},
	{0x03, 0xff, 0x00},
	
	{0x02, 0x00, 0x30},
};

#define IMXX22_SGAIN_TBLSIZE	141

static imxX22GainTbl g_ImxX22SenGainTbl[IMXX22_SGAIN_TBLSIZE] = {
  {   0x00	  ,   16000   },  //0dB
  {   0x01	  ,   19542   },  //0.3dB
  {   0x02	  ,   23084   },  //0.6dB
  {   0x03	  ,   26626   },  //0.9dB
  {   0x04	  ,   30168   },  //1.2dB
  {   0x05	  ,   33710   },  //1.5dB
  {   0x06	  ,   37252   },  //1.8dB
  {   0x07	  ,   40794   },  //2.1dB
  {   0x08	  ,   44336   },  //2.4dB
  {   0x09	  ,   47878   },  //2.7dB
  {   0x0A	  ,   51420   },  //3dB
  {   0x0B	  ,   54962   },  //3.3dB
  {   0x0C	  ,   58504   },  //3.6dB
  {   0x0D	  ,   62046   },  //3.9dB
  {   0x0E	  ,   65588   },  //4.2dB
  {   0x0F	  ,   69130   },  //4.5dB
  {   0x10	  ,   72672   },  //4.8dB
  {   0x11	  ,   76214   },  //5.1dB
  {   0x12	  ,   79756   },  //5.4dB
  {   0x13	  ,   83298   },  //5.7dB
  {   0x14	  ,   86840   },  //6dB
  {   0x15	  ,   90382   },  //6.3dB
  {   0x16	  ,   93924   },  //6.6dB
  {   0x17	  ,   97466   },  //6.9dB
  {   0x18	  ,   101008  },  //7.2dB
  {   0x19	  ,   104550  },  //7.5dB
  {   0x1A	  ,   108092  },  //7.8dB
  {   0x1B	  ,   111634  },  //8.1dB
  {   0x1C	  ,   115176  },  //8.4dB
  {   0x1D	  ,   118718  },  //8.7dB
  {   0x1E	  ,   122260  },  //9dB
  {   0x1F	  ,   125802  },  //9.3dB
  {   0x20	  ,   129344  },  //9.6dB
  {   0x21	  ,   132886  },  //9.9dB
  {   0x22	  ,   136428  },  //10.2dB
  {   0x23	  ,   139970  },  //10.5dB
  {   0x24	  ,   143512  },  //10.8dB
  {   0x25	  ,   147054  },  //11.1dB
  {   0x26	  ,   150596  },  //11.4dB
  {   0x27	  ,   154138  },  //11.7dB
  {   0x28	  ,   157680  },  //12dB
  {   0x29	  ,   161222  },  //12.3dB
  {   0x2A	  ,   164764  },  //12.6dB
  {   0x2B	  ,   168306  },  //12.9dB
  {   0x2C	  ,   171848  },  //13.2dB
  {   0x2D	  ,   175390  },  //13.5dB
  {   0x2E	  ,   178932  },  //13.8dB
  {   0x2F	  ,   182474  },  //14.1dB
  {   0x30	  ,   186016  },  //14.4dB
  {   0x31	  ,   189558  },  //14.7dB
  {   0x32	  ,   193100  },  //15dB
  {   0x33	  ,   196642  },  //15.3dB
  {   0x34	  ,   200184  },  //15.6dB
  {   0x35	  ,   203726  },  //15.9dB
  {   0x36	  ,   207268  },  //16.2dB
  {   0x37	  ,   210810  },  //16.5dB
  {   0x38	  ,   214352  },  //16.8dB
  {   0x39	  ,   217894  },  //17.1dB
  {   0x3A	  ,   221436  },  //17.4dB
  {   0x3B	  ,   224978  },  //17.7dB
  {   0x3C	  ,   228520  },  //18dB
  {   0x3D	  ,   232062  },  //18.3dB
  {   0x3E	  ,   235604  },  //18.6dB
  {   0x3F	  ,   239146  },  //18.9dB
  {   0x40	  ,   242688  },  //19.2dB
  {   0x41	  ,   246230  },  //19.5dB
  {   0x42	  ,   249772  },  //19.8dB
  {   0x43	  ,   253314  },  //20.1dB
  {   0x44	  ,   256856  },  //20.4dB
  {   0x45	  ,   260398  },  //20.7dB
  {   0x46	  ,   263940  },  //21dB
  {   0x47	  ,   267482  },  //21.3dB
  {   0x48	  ,   271024  },  //21.6dB
  {   0x49	  ,   274566  },  //21.9dB
  {   0x4A	  ,   278108  },  //22.2dB
  {   0x4B	  ,   281650  },  //22.5dB
  {   0x4C	  ,   285192  },  //22.8dB
  {   0x4D	  ,   288734  },  //23.1dB
  {   0x4E	  ,   292276  },  //23.4dB
  {   0x4F	  ,   295818  },  //23.7dB
  {   0x50	  ,   299360  },  //24dB
  {   0x51	  ,   302902  },  //24.3dB
  {   0x52	  ,   306444  },  //24.6dB
  {   0x53	  ,   309986  },  //24.9dB
  {   0x54	  ,   313528  },  //25.2dB
  {   0x55	  ,   317070  },  //25.5dB
  {   0x56	  ,   320612  },  //25.8dB
  {   0x57	  ,   324154  },  //26.1dB
  {   0x58	  ,   327696  },  //26.4dB
  {   0x59	  ,   331238  },  //26.7dB
  {   0x5A	  ,   334780  },  //27dB
  {   0x5B	  ,   338322  },  //27.3dB
  {   0x5C	  ,   341864  },  //27.6dB
  {   0x5D	  ,   345406  },  //27.9dB
  {   0x5E	  ,   348948  },  //28.2dB
  {   0x5F	  ,   352490  },  //28.5dB
  {   0x60	  ,   356032  },  //28.8dB
  {   0x61	  ,   359574  },  //29.1dB
  {   0x62	  ,   363116  },  //29.4dB
  {   0x63	  ,   366658  },  //29.7dB
  {   0x64	  ,   370200  },  //30dB
  {   0x65	  ,   373742  },  //30.4dB
  {   0x66	  ,   377284  },  //30.7dB
  {   0x67	  ,   380826  },  //31dB
  {   0x68	  ,   384368  },  //31.3dB
  {   0x69	  ,   387910  },  //31.6dB
  {   0x6A	  ,   391452  },  //31.9dB
  {   0x6B	  ,   394994  },  //32.2dB
  {   0x6C	  ,   398536  },  //32.4dB
  {   0x6D	  ,   402078  },  //32.7dB
  {   0x6E	  ,   405620  },  //33dB
  {   0x6F	  ,   409162  },  //33.3dB
  {   0x70	  ,   412704  },  //33.6dB
  {   0x71	  ,   416246  },  //33.9dB
  {   0x72	  ,   419788  },  //34.2dB
  {   0x73	  ,   423330  },  //34.5dB
  {   0x74	  ,   426872  },  //34.8dB
  {   0x75	  ,   430414  },  //35.1dB
  {   0x76	  ,   433956  },  //35.4dB
  {   0x77	  ,   437498  },  //35.7dB
  {   0x78	  ,   441040  },  //36dB
  {   0x79	  ,   444582  },  //36.3dB
  {   0x7A	  ,   448124  },  //36.6dB
  {   0x7B	  ,   451666  },  //36.9dB
  {   0x7C	  ,   455208  },  //37.2dB
  {   0x7D	  ,   458750  },  //37.5dB
  {   0x7E	  ,   462292  },  //37.8dB
  {   0x7F	  ,   465834  },  //38.1dB
  {   0x80	  ,   469376  },  //38.4dB
  {   0x81	  ,   472918  },  //38.7dB
  {   0x82	  ,   476460  },  //39dB
  {   0x83	  ,   480002  },  //39.3dB
  {   0x84	  ,   483544  },  //39.6dB
  {   0x85	  ,   487086  },  //39.9dB
  {   0x86	  ,   490628  },  //40.2dB
  {   0x87	  ,   494170  },  //40.5dB
  {   0x88	  ,   497712  },  //40.8dB
  {   0x89	  ,   501254  },  //41.1dB
  {   0x8A	  ,   504796  },  //41.4dB
  {   0x8B	  ,   508338  },  //41.7dB
  {   0x8C	  ,   512000  },  //42dB
};



/**! 1080: 1920*1080@30fps */
static struct ImxX22Reg sensor_1080p_regs[] = {
	{0x02, 0x00, 0x31},
};

/**! 720: 1280*720@60fps */
static struct ImxX22Reg sensor_720p_regs[] = {
	{0x02, 0x00, 0x31},
};

/**! VGA:  640*480 */
static struct ImxX22Reg sensor_vga_regs[] = {
	{0x02, 0x00, 0x31},
};


/**! 
  * Here we'll try to encapsulate the changes for just the output
  * video format.
  */
static struct ImxX22Reg sensor_fmt_raw[] = {
	{0x02, 0x00, 0x31},
};


static struct ImxX22Reg sensor_dumy[] = {
	{0x02, 0x00, 0x00},
};






/**!
  * Low-level register I/O.
  */
static int sensor_read(struct v4l2_subdev *sd, u8 dvid, u8 reg, u8 *value)
{
	return 0;
}

static int reg_val_show(struct v4l2_subdev *sd, u8 dvid,u8 reg)
{
	return 0;
}

static int sensor_write(struct v4l2_subdev *sd, u8 dvid, u8 reg, u8 value)
{
	int ret=0;
	u8 tmpBuf[3]={0,};

	vfe_gpio_write(sd,SPI_CS,CSI_SPI_CS_LOW);
    usleep_range(20,30);
	tmpBuf[0] = dvid;
	tmpBuf[1] = reg;
	tmpBuf[2] = value;
	ret=spi_write(glb_spiBus,tmpBuf,sizeof(tmpBuf));
    usleep_range(20,30);
	vfe_gpio_write(sd,SPI_CS,CSI_SPI_CS_HIGH);
	return ret;
}

/**!
  * Write a list of register settings;
  */
static int sensor_write_array(struct v4l2_subdev *sd, struct ImxX22Reg *regs, int array_size)
{
	int aryIdx = 0; 
	u8 tmpBuf[3]={0,};
	
	if( array_size<0||sd==NULL||regs==NULL){
		return -EINVAL;
	}

	for (aryIdx=0; aryIdx<array_size; aryIdx++)
	{
		vfe_gpio_write(sd,SPI_CS,CSI_SPI_CS_LOW);
		usleep_range(20,30);
		tmpBuf[0] = regs[aryIdx].devid;
		tmpBuf[1] = regs[aryIdx].reg_addr;
		tmpBuf[2] = regs[aryIdx].reg_val;
		if( spi_write(glb_spiBus,tmpBuf,sizeof(tmpBuf)) < 0){
			vfe_dev_err("Cannot write[%dth] to 0x%02x:0x%02x with Reg:0x%02x\n",
						aryIdx,tmpBuf[0],tmpBuf[1],tmpBuf[2]);
			break;
		}
		//vfe_dev_dbg("Witten[0x%02x:0x%02x] = 0x%02x\n",tmpBuf[0],tmpBuf[1],tmpBuf[2]);
		usleep_range(20,30);
		vfe_gpio_write(sd,SPI_CS,CSI_SPI_CS_HIGH);		
		usleep_range(20,30);
	}
	vfe_gpio_write(sd,SPI_CS,CSI_SPI_CS_HIGH);		
	return 0;
}




/* 
 * Code for dealing with controls.
 * fill with different sensor module
 * different sensor module has different settings here
 * if not support the follow function ,retrun -EINVAL
 */

/* *********************************************begin of ******************************************** */
/*
static int sensor_g_hflip(struct v4l2_subdev *sd, __s32 *value)
{
	return 0;
}

static int sensor_s_hflip(struct v4l2_subdev *sd, int value)
{
	return 0;
}

static int sensor_g_vflip(struct v4l2_subdev *sd, __s32 *value)
{
	return 0;
}

static int sensor_s_vflip(struct v4l2_subdev *sd, int value)
{
	return 0;
}

static int sensor_g_pclk(struct v4l2_subdev *sd, unsigned int *value)
{
	return 0;
}*/

static int sensor_g_exp(struct v4l2_subdev *sd, __s32 *value)
{
	struct sensor_info *info = to_state(sd);
	*value = info->exp;
	vfe_dev_dbg("sensor_get_exposure = %d\n", info->exp);
	return 0;
}

static int sensor_s_exp(struct v4l2_subdev *sd, unsigned int exp_val)
{
	unsigned char explow,expmid;
	unsigned int exptime;
	struct sensor_info *info = to_state(sd);

	if(exp_val>0x1fffff){
		exp_val=0x1fffff;
	}

	exptime = 1125 - (exp_val>>4);

	expmid  = (unsigned char) ((0x000ff00&exptime)>>8);
	explow  = (unsigned char) ((0x00000ff&exptime)   ) ;

	sensor_write(sd, 0x02, 0x08, explow);
	sensor_write(sd, 0x02, 0x09, expmid);

	vfe_dev_dbg("sensor_s_exp: exp_val = %d exptime=%d\n", exp_val , exptime);

	info->exp = exp_val;
	return 0;
}


static int sensor_g_gain(struct v4l2_subdev *sd, __s32 *value)
{
	struct sensor_info *info = to_state(sd);

	*value = info->gain;
	vfe_dev_dbg("sensor_get_gain = %d\n", info->gain);
	return 0;
}

static int sensor_s_gain(struct v4l2_subdev *sd, int gain_val)
{		
	struct sensor_info *info = to_state(sd);
	int tblGain=gain_val*1000;
	int gainRegVal = 0;
	int i;

	if(tblGain<g_ImxX22SenGainTbl[0].db) {
		tblGain = g_ImxX22SenGainTbl[0].db;  
	}

	if(tblGain>g_ImxX22SenGainTbl[IMXX22_SGAIN_TBLSIZE-1].db) {
		tblGain = g_ImxX22SenGainTbl[IMXX22_SGAIN_TBLSIZE-1].db;
	}

	for (i=0; i<IMXX22_SGAIN_TBLSIZE-1; i++) {
		if (tblGain>=g_ImxX22SenGainTbl[i].db && tblGain<g_ImxX22SenGainTbl[i+1].db) {
			gainRegVal = g_ImxX22SenGainTbl[i].reg;
			break;
		}
	}

	sensor_write(sd, 0x02, 0x1E, gainRegVal);
	vfe_dev_dbg("sensor_s_gain: gain_val = %d  reg_val = %d\n", gain_val , gainRegVal);
	
	info->gain = gain_val;
	return 0;
}

static int sensor_s_exp_gain(struct v4l2_subdev *sd, struct sensor_exp_gain *exp_gain)
{
	int exp_val, gain_val, shutter, frame_length;  
	struct sensor_info *info = to_state(sd);

	exp_val = exp_gain->exp_val;
	gain_val = exp_gain->gain_val;

	if(gain_val<1*16){
		gain_val=16;
	}
	
	if(gain_val>32*16-1){
		gain_val=32*16-1;
	}

	if(exp_val>0xfffff){
		exp_val=0xfffff;
	}

	shutter = (exp_val+8)/16;
	if(shutter  > 1125){
		frame_length = 1125;
	}

	sensor_s_exp(sd,exp_val);
	sensor_s_gain(sd,gain_val);

	//  printk("imx322 sensor_set_gain exp= %d, %d Done!\n", gain_val,exp_val);

	info->exp = exp_val;
	info->gain = gain_val;
	return 0;
}


static int sensor_s_sw_stby(struct v4l2_subdev *sd, int on_off)
{
	int ret = 0;
	return ret;
}

/*static int sensor_set_power_sleep(struct v4l2_subdev *sd, int on_off)
{
	int ret = 0;
	

	return ret;
}

static int sensor_set_pll_enable(struct v4l2_subdev *sd, int on_off)
{
	int ret = 0;
	
	return ret;
}*/

/*
 * Stuff that knows about the sensor.
 */
 
static int sensor_power(struct v4l2_subdev *sd, int on)
{
	struct i2c_client *client = v4l2_get_subdevdata(sd);
	int ret;

	//insure that clk_disable() and clk_enable() are called in pair 
	//when calling CSI_SUBDEV_STBY_ON/OFF and CSI_SUBDEV_PWR_ON/OFF
	ret = 0;
	switch(on)
	{
		case CSI_SUBDEV_STBY_ON:
			vfe_dev_dbg("CSI_SUBDEV_STBY_ON!\n");
			vfe_dev_dbg("Called: glb_spiBus=%p\n",glb_spiBus);
			
			//make sure that no device can access i2c bus during sensor initial or power down
			//when using i2c_lock_adpater function, the following codes must not access i2c bus before calling i2c_unlock_adapter
			i2c_lock_adapter(client->adapter);
			//standby on io
			vfe_gpio_write(sd,PWDN,CSI_STBY_ON);

			vfe_set_mclk(sd,OFF);

			//remember to unlock i2c adapter, so the device can access the i2c bus again
			i2c_unlock_adapter(client->adapter);  
			break;

		case CSI_SUBDEV_STBY_OFF:
			vfe_dev_dbg("CSI_SUBDEV_STBY_OFF!\n");
			//make sure that no device can access i2c bus during sensor initial or power down
			//when using i2c_lock_adpater function, the following codes must not access i2c bus before calling i2c_unlock_adapter
			i2c_lock_adapter(client->adapter);    
			//active mclk before stadby out
			vfe_set_mclk_freq(sd,MCLK);
			vfe_set_mclk(sd,ON);
			usleep_range(10000,12000);
			//standby off io
			vfe_gpio_write(sd,PWDN,CSI_STBY_OFF);
			usleep_range(10000,12000);
			//remember to unlock i2c adapter, so the device can access the i2c bus again
			i2c_unlock_adapter(client->adapter);        
			//software standby
			ret = sensor_s_sw_stby(sd, CSI_STBY_OFF);
			if(ret < 0)
				vfe_dev_err("soft stby off falied!\n");

			usleep_range(10000,12000);
			vfe_dev_print("enable oe!\n");
			break;

		case CSI_SUBDEV_PWR_ON:
			vfe_dev_dbg("CSI_SUBDEV_PWR_ON!\n");
			i2c_lock_adapter(client->adapter);

			//SPI CS initailization
			vfe_gpio_set_status(sd,SPI_CS,1);//set the gpio to output
			
			//power on reset
			vfe_gpio_set_status(sd,PWDN,1);//set the gpio to output
			vfe_gpio_set_status(sd,RESET,1);//set the gpio to output

			//SPI CS default level set as high
			vfe_gpio_write(sd,SPI_CS,CSI_SPI_CS_HIGH);


			vfe_gpio_write(sd,RESET,CSI_RST_ON);  
			vfe_gpio_write(sd,PWDN,CSI_STBY_ON);
			usleep_range(1000,1200);

			//power supply
			//vfe_gpio_write(sd,POWER_EN,CSI_PWR_ON);
			vfe_set_pmu_channel(sd,IOVDD,ON);
			usleep_range(1000,1200);
			vfe_set_pmu_channel(sd,DVDD,ON);
			vfe_set_pmu_channel(sd,AFVDD,ON);


			vfe_gpio_write(sd,PWDN,CSI_STBY_OFF); 
			vfe_gpio_write(sd,RESET,CSI_RST_OFF);
			usleep_range(3000,3200);

			//active mclk before power on
			vfe_set_mclk_freq(sd,MCLK);
			vfe_set_mclk(sd,ON);
			usleep_range(10000,12000);
			//remember to unlock i2c adapter, so the device can access the i2c bus again
			i2c_unlock_adapter(client->adapter); 


			#if 0
			ret = sensor_write_array(sd, sensor_default_regs, ARRAY_SIZE(sensor_default_regs));  
			if(ret < 0) {
				vfe_dev_err("write sensor_default_regs error\n");
				return ret;
			}
			#endif
			vfe_dev_dbg("Sensor Initialization Success\n");
			break;

		case CSI_SUBDEV_PWR_OFF:
			vfe_dev_dbg("CSI_SUBDEV_PWR_OFF!\n");
			//make sure that no device can access i2c bus during sensor initial or power down
			//when using i2c_lock_adpater function, the following codes must not access i2c bus before calling i2c_unlock_adapter
			i2c_lock_adapter(client->adapter);

			vfe_gpio_set_status(sd,PWDN,1);//set the gpio to output
			vfe_gpio_set_status(sd,RESET,1);//set the gpio to output
			vfe_gpio_write(sd,RESET,CSI_RST_ON);  
			vfe_gpio_write(sd,PWDN,CSI_STBY_ON);

			//inactive mclk before power off
			vfe_set_mclk(sd,OFF);

			//power supply off
			//vfe_gpio_write(sd,POWER_EN,CSI_PWR_OFF);
			//vfe_set_pmu_channel(sd,AFVDD,OFF);
			vfe_set_pmu_channel(sd,DVDD,OFF);
			vfe_set_pmu_channel(sd,AVDD,OFF);
			vfe_set_pmu_channel(sd,IOVDD,OFF);  

			//set the io to hi-z
			vfe_gpio_set_status(sd,RESET,0);//set the gpio to input
			vfe_gpio_set_status(sd,PWDN,0);//set the gpio to input
			//remember to unlock i2c adapter, so the device can access the i2c bus again
			i2c_unlock_adapter(client->adapter);  
			break;
		default:
			return -EINVAL;
	}   

	return 0;
}
 
static int sensor_reset(struct v4l2_subdev *sd, u32 val)
{
	switch(val)
	{
		case 0:
			vfe_gpio_write(sd,RESET,CSI_RST_OFF);
			usleep_range(10000,12000);
			break;

		case 1:
			vfe_gpio_write(sd,RESET,CSI_RST_ON);
			usleep_range(10000,12000);
			break;

		default:
			return -EINVAL;
	}

	return 0;
}

static int sensor_detect(struct v4l2_subdev *sd)
{
	printk(KERN_DEBUG"*********find IMX122 raw data camera sensor now.\n");
	return 0;
}

static int sensor_init(struct v4l2_subdev *sd, u32 val)
{
	int ret;
	struct sensor_info *info = to_state(sd);

	vfe_dev_dbg("sensor_init\n");

	/*Make sure it is a target sensor*/
	ret = sensor_detect(sd);
	if (ret) {
		vfe_dev_err("chip found is not an target chip.\n");
		return ret;
	}

	vfe_get_standby_mode(sd,&info->stby_mode);

	if((info->stby_mode == HW_STBY || info->stby_mode == SW_STBY) && info->init_first_flag == 0) {
		vfe_dev_print("stby_mode and init_first_flag = 0\n");
		return 0;
	} 

	info->focus_status = 0;
	info->low_speed = 0;
	info->width = HD1080_WIDTH;
	info->height = HD1080_HEIGHT;
	info->hflip = 0;
	info->vflip = 0;
	info->gain = 0;

	info->tpf.numerator = 1;            
	info->tpf.denominator = 30;    /* 30fps */    

	ret = sensor_write_array(sd, sensor_default_regs, ARRAY_SIZE(sensor_default_regs));  
	if(ret < 0) {
		vfe_dev_err("write sensor_default_regs error\n");
		return ret;
	}

	if(info->stby_mode == 0)
		info->init_first_flag = 0;

	info->preview_first_flag = 1;

	return 0;
}

static long sensor_ioctl(struct v4l2_subdev *sd, unsigned int cmd, void *arg)
{
	int ret=0;
	struct sensor_info *info = to_state(sd);

	vfe_dev_dbg(" cmd=[%d]\n",cmd);
	vfe_dev_dbg(" arg=[%0x]\n",arg);

	switch(cmd) {
		case GET_CURRENT_WIN_CFG:
			if(info->current_wins != NULL)
			{
				memcpy( arg,
						info->current_wins,
						sizeof(struct sensor_win_size) );
				ret=0;
			}
			else
			{
				vfe_dev_err("empty wins!\n");
				ret=-1;
			}
			break;
		case SET_FPS:
			ret=0;
			break;
		case ISP_SET_EXP_GAIN:
			sensor_s_exp_gain(sd, (struct sensor_exp_gain *)arg);
			break;
		default:
			return -EINVAL;
	}
	return ret;
}


/*
 * Store information about the video data format. 
 */
static struct sensor_format_struct {
	__u8 *desc;
	//__u32 pixelformat;
	enum v4l2_mbus_pixelcode mbus_code;
	struct ImxX22Reg *regs;
	int regs_size;
	int bpp;   /* Bytes per pixel */
}sensor_formats[] = {
	{
		.desc		= "Raw RGB Bayer",
		.mbus_code	= V4L2_MBUS_FMT_SBGGR12_1X12,
		.regs 		= sensor_fmt_raw,
		.regs_size = ARRAY_SIZE(sensor_fmt_raw),
		.bpp		= 1
	},
	{
		.desc		= "Raw RGB Bayer",
		.mbus_code	= V4L2_MBUS_FMT_SGBRG12_1X12,
		.regs 		= sensor_fmt_raw,
		.regs_size = ARRAY_SIZE(sensor_fmt_raw),
		.bpp		= 1
	},
	{
		.desc		= "Raw RGB Bayer",
		.mbus_code	= V4L2_MBUS_FMT_SGRBG12_1X12,
		.regs 		= sensor_fmt_raw,
		.regs_size = ARRAY_SIZE(sensor_fmt_raw),
		.bpp		= 1
	},
	{
		.desc		= "Raw RGB Bayer",
		.mbus_code	= V4L2_MBUS_FMT_SRGGB12_1X12,
		.regs 		= sensor_fmt_raw,
		.regs_size = ARRAY_SIZE(sensor_fmt_raw),
		.bpp		= 1
	}
};
#define N_FMTS ARRAY_SIZE(sensor_formats)

  

/*
 * Then there is the issue of window sizes.  Try to capture the info here.
 */


static struct sensor_win_size sensor_win_sizes[] = {
    /* 1080P */
    {
      .width      = HD1080_WIDTH,
      .height     = HD1080_HEIGHT,
      .hoffset	  = 107,
      .voffset	  = 16,
      .hts        = 2008,
      .vts        = 1125,
      .pclk       = 74250*1000,
      .fps_fixed  = 1,
      .bin_factor = 1,
      .intg_min   = 1,
      .intg_max   = 1102<<4,//1122<<4,
      .gain_min   = 1<<4,
      .gain_max   = 9<<4,
      .regs       = sensor_1080p_regs,//
      .regs_size  = ARRAY_SIZE(sensor_1080p_regs),//
      .set_size		= NULL,
    },
    /* 720p */
    {
      .width      = HD720_WIDTH,
      .height     = HD720_HEIGHT,
      .hoffset    = 0,
      .voffset    = 0,
      .hts        = 1792,//1288,
      .vts        = 744,
      .pclk       = 80*1000*1000,
      .fps_fixed  = 1,
      .bin_factor = 1,
      .intg_min   = 1,
      .intg_max   = 744<<4,
      .gain_min   = 1<<4,
      .gain_max   = 10<<4,
      .regs			  = sensor_720p_regs,//
      .regs_size	= ARRAY_SIZE(sensor_720p_regs),//
      .set_size		= NULL,
    },
  	/* VGA */
    {
      .width	  = VGA_WIDTH,
      .height 	  = VGA_HEIGHT,
      .hoffset	  = 0,
      .voffset	  = 0,
      .hts        = 1344,//limited by sensor
      .vts        = 496,
      .pclk       = 80*1000*1000,
      .fps_fixed  = 1,
      .bin_factor = 1,
      .intg_min   = 1,
      .intg_max   = 496<<4,
      .gain_min   = 1<<4,
      .gain_max   = 10<<4,
      .regs       = sensor_vga_regs,
      .regs_size  = ARRAY_SIZE(sensor_vga_regs),
      .set_size   = NULL,
    },
};

#define N_WIN_SIZES (ARRAY_SIZE(sensor_win_sizes))

static int sensor_enum_fmt(struct v4l2_subdev *sd, unsigned index, enum v4l2_mbus_pixelcode *code)
{
	if (index >= N_FMTS)
		return -EINVAL;

	*code = sensor_formats[index].mbus_code;
	return 0;
}

static int sensor_enum_size(struct v4l2_subdev *sd, struct v4l2_frmsizeenum *fsize)
{
	if(fsize->index > N_WIN_SIZES-1)
		return -EINVAL;

	fsize->type = V4L2_FRMSIZE_TYPE_DISCRETE;
	fsize->discrete.width = sensor_win_sizes[fsize->index].width;
	fsize->discrete.height = sensor_win_sizes[fsize->index].height;

	return 0;
}

static int sensor_try_fmt_internal(struct v4l2_subdev *sd,
									struct v4l2_mbus_framefmt *fmt,
									struct sensor_format_struct **ret_fmt,
									struct sensor_win_size **ret_wsize)
{
	int index;
	struct sensor_win_size *wsize;
	struct sensor_info *info = to_state(sd);

	for (index = 0; index < N_FMTS; index++){
		if (sensor_formats[index].mbus_code == fmt->code){
			break;
		}
	}

	vfe_dev_print("index=%d mbus_code=%d fmt->code=%d\n",index, sensor_formats[index].mbus_code, fmt->code);

	if (index >= N_FMTS) {
		return -EINVAL;
	}

	if (ret_fmt != NULL){
		*ret_fmt = sensor_formats + index;
	}

	/*
	* Fields: the sensor devices claim to be progressive.
	*/
	fmt->field = V4L2_FIELD_NONE;

	/*
	* Round requested image size down to the nearest
	* we support, but not below the smallest.
	*/
	for (wsize = sensor_win_sizes; wsize < sensor_win_sizes + N_WIN_SIZES;wsize++){
		if (fmt->width >= wsize->width && fmt->height >= wsize->height){
			break;
		}
	}

	if (wsize >= sensor_win_sizes + N_WIN_SIZES){
		wsize--;   /* Take the smallest one */
	}

	if (ret_wsize != NULL){
		*ret_wsize = wsize;
	}

	/*
	* Note the size we'll actually handle.
	*/
	fmt->width = wsize->width;
	fmt->height = wsize->height;
	info->current_wins = wsize;	
	return 0;
}

static int sensor_try_fmt(struct v4l2_subdev *sd, struct v4l2_mbus_framefmt *fmt)
{
	return sensor_try_fmt_internal(sd, fmt, NULL, NULL);
}

static int sensor_g_mbus_config(struct v4l2_subdev *sd, struct v4l2_mbus_config *cfg)
{
	vfe_dev_print("sensor_g_mbus_config Called...\n");
	cfg->type = V4L2_MBUS_PARALLEL;
	cfg->flags = V4L2_MBUS_MASTER | VREF_POL | HREF_POL | CLK_POL ;
	return 0;
}


/*
 * Set a format.
 */
static int sensor_s_fmt(struct v4l2_subdev *sd, struct v4l2_mbus_framefmt *fmt)
{
	int ret;
	struct sensor_format_struct *sensor_fmt;
	struct sensor_win_size *wsize;
	struct sensor_info *info = to_state(sd);

	vfe_dev_dbg("sensor_s_fmt\n");

	//  sensor_write_array(sd, sensor_oe_disable_regs, ARRAY_SIZE(sensor_oe_disable_regs));
	ret = sensor_try_fmt_internal(sd, fmt, &sensor_fmt, &wsize);
	if (ret){
		return ret;
	}

	if(info->capture_mode == V4L2_MODE_VIDEO)
	{
		//video
	}
	else if(info->capture_mode == V4L2_MODE_IMAGE)
	{
		//image 
	}
	//LOG_ERR_RET(sensor_write_array(sd, sensor_fmt->regs, sensor_fmt->regs_size))

	ret = 0;
	if (wsize->regs)
	{
		// usleep_range(5000,6000);
		//LOG_ERR_RET(sensor_write_array(sd, wsize->regs, wsize->regs_size))
	}

	if (wsize->set_size){
		LOG_ERR_RET(wsize->set_size(sd))
	}

	info->fmt = sensor_fmt;
	info->width = wsize->width;
	info->height = wsize->height;

	vfe_dev_print("s_fmt set width = %d, height = %d\n",info->width,info->height);
	if(info->capture_mode == V4L2_MODE_VIDEO)
	{
		//video
	} 
	else 
	{
		//capture image
	}
	return 0;
}

/*
 * Implement G/S_PARM.  There is a "high quality" mode we could try
 * to do someday; for now, we just do the frame rate tweak.
 */
static int sensor_g_parm(struct v4l2_subdev *sd, struct v4l2_streamparm *parms)
{
	struct v4l2_captureparm *cp = &parms->parm.capture;
	struct sensor_info *info = to_state(sd);

	if (parms->type != V4L2_BUF_TYPE_VIDEO_CAPTURE){
		return -EINVAL;
	}

	memset(cp, 0, sizeof(struct v4l2_captureparm));
	cp->capability = V4L2_CAP_TIMEPERFRAME;
	cp->capturemode = info->capture_mode;

	return 0;
}

static int sensor_s_parm(struct v4l2_subdev *sd, struct v4l2_streamparm *parms)
{
	struct v4l2_captureparm *cp = &parms->parm.capture;
	//struct v4l2_fract *tpf = &cp->timeperframe;
	struct sensor_info *info = to_state(sd);
	//unsigned char div;

	vfe_dev_dbg("sensor_s_parm\n");

	if (parms->type != V4L2_BUF_TYPE_VIDEO_CAPTURE){
		return -EINVAL;
	}

	if (info->tpf.numerator == 0){
		return -EINVAL;
	}

	info->capture_mode = cp->capturemode;

	return 0;
}


static int sensor_queryctrl(struct v4l2_subdev *sd,
    struct v4l2_queryctrl *qc)
{
	/* Fill in min, max, step and default value for these controls. */
	/* see include/linux/videodev2.h for details */

	switch (qc->id) {
		case V4L2_CID_GAIN:
			return v4l2_ctrl_query_fill(qc, 1*16, 16*16, 1, 16);
		case V4L2_CID_EXPOSURE:
			return v4l2_ctrl_query_fill(qc, 1, 65536*16, 1, 1);
		case V4L2_CID_FRAME_RATE:
			return v4l2_ctrl_query_fill(qc, 15, 120, 1, 30);
		case V4L2_CID_VFLIP:
			vfe_dev_print("unsupported sensor_queryctrl command: V4L2_CID_VFLIP\n");
			return 0;
		case V4L2_CID_HFLIP:
			vfe_dev_print("unsupported sensor_queryctrl command: V4L2_CID_HFLIP\n");
			return 0;
	}
	return -EINVAL;
}

static int sensor_g_ctrl(struct v4l2_subdev *sd, struct v4l2_control *ctrl)
{
	switch (ctrl->id) {
		case V4L2_CID_GAIN:
			return sensor_g_gain(sd, &ctrl->value);
		case V4L2_CID_EXPOSURE:
			return sensor_g_exp(sd, &ctrl->value);
	}
	return -EINVAL;
}

static int sensor_s_ctrl(struct v4l2_subdev *sd, struct v4l2_control *ctrl)
{
	struct v4l2_queryctrl qc;
	int ret=0;

	qc.id = ctrl->id;
	ret = sensor_queryctrl(sd, &qc);
	if (ret < 0) {
		return ret;
	}
	
	switch (ctrl->id) {
		case V4L2_CID_GAIN:
			ret = sensor_s_gain(sd, ctrl->value);
			break;
		case V4L2_CID_EXPOSURE:
			ret = sensor_s_exp(sd, ctrl->value);
			break;
		case V4L2_CID_FRAME_RATE:
			vfe_dev_print("unsupported sensor_s_ctrl command: V4L2_CID_FRAME_RATE\n");
			ret = 0;
			break;
		case V4L2_CID_VFLIP:
			vfe_dev_print("unsupported sensor_s_ctrl command: V4L2_CID_VFLIP\n");
			ret = 0;
			break;
		case V4L2_CID_HFLIP:
			vfe_dev_print("unsupported sensor_s_ctrl command: V4L2_CID_HFLIP\n");
			ret = 0;
			break;
		default:
			ret = -EINVAL;
	}
	return ret;
}


static int sensor_g_chip_ident(struct v4l2_subdev *sd, struct v4l2_dbg_chip_ident *chip)
{
	struct i2c_client *client = v4l2_get_subdevdata(sd);
	return v4l2_chip_ident_i2c_client(client, chip, V4L2_IDENT_SENSOR, 0);
}


/* ----------------------------------------------------------------------- */

static const struct v4l2_subdev_core_ops sensor_core_ops = {
	.g_chip_ident = sensor_g_chip_ident,
	.g_ctrl = sensor_g_ctrl,
	.s_ctrl = sensor_s_ctrl,
	.queryctrl = sensor_queryctrl,
	.reset = sensor_reset,
	.init = sensor_init,
	.s_power = sensor_power,
	.ioctl = sensor_ioctl,
};

static const struct v4l2_subdev_video_ops sensor_video_ops = {
	.enum_mbus_fmt = sensor_enum_fmt,
	.enum_framesizes = sensor_enum_size,
	.try_mbus_fmt = sensor_try_fmt,
	.s_mbus_fmt = sensor_s_fmt,
	.s_parm = sensor_s_parm,
	.g_parm = sensor_g_parm,
	.g_mbus_config = sensor_g_mbus_config,
};

static const struct v4l2_subdev_ops sensor_ops = {
	.core = &sensor_core_ops,
	.video = &sensor_video_ops,
};

/* ----------------------------------------------------------------------- */
static int __devinit sensor_spi_probe(struct spi_device *spi)
{
	int res;
	u8 tmpBuf[3]={0x02,0x00,0x00};

	spi->mode = SPI_LSB_FIRST;//SPI_MODE_3;
	spi->bits_per_word = 8;
	spi->max_speed_hz = 8000000;
	
	spi_setup(spi);

	vfe_dev_dbg("Before: glb_spiBus=%p\n",glb_spiBus);
	
	glb_spiBus = spi;

	/**< JoelKim: Send summy packet for make sure spi setting */ 
	spi_write(glb_spiBus,tmpBuf,sizeof(tmpBuf));
	
	vfe_dev_dbg("Allcated: glb_spiBus=%p\n",glb_spiBus);
	
	return 0;
}

static int __devexit sensor_spi_remove(struct spi_device *spi)
{
	glb_spiBus = NULL;
	return 0;
}


static struct spi_driver sensor_spi_driver = {
	.driver = {
		.name	= "spi2_cam",
		.owner	= THIS_MODULE,
	},
	.probe	= sensor_spi_probe,
	.remove = __devexit_p(sensor_spi_remove),
};





static int sensor_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct v4l2_subdev *sd;
	struct sensor_info *info;

	info = kzalloc(sizeof(struct sensor_info), GFP_KERNEL);
	if (info == NULL){
		return -ENOMEM;
	}
	
	sd = &info->sd;
	glb_sd = sd;
	v4l2_i2c_subdev_init(sd, client, &sensor_ops);

	info->fmt = &sensor_formats[0];
	info->af_first_flag = 1;
	info->init_first_flag = 1;

	return 0;
}


static int sensor_remove(struct i2c_client *client)
{
	struct v4l2_subdev *sd = i2c_get_clientdata(client);

	v4l2_device_unregister_subdev(sd);
	kfree(to_state(sd));
	return 0;
}

static const struct i2c_device_id sensor_id[] = {
	{ "imx122", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, sensor_id);


static struct i2c_driver sensor_driver = {
	.driver = {
		.owner = THIS_MODULE,
		.name = "imx122",
	},
	.probe = sensor_probe,
	.remove = sensor_remove,
	.id_table = sensor_id,
};


static __init int init_sensor(void)
{
	int ret = 0;
	ret = spi_register_driver(&sensor_spi_driver);
	if (ret < 0) {
		vfe_dev_err("failed to register spi driver: %d", ret);
		goto out;
	}
	ret = i2c_add_driver(&sensor_driver);

out:
	return ret;
}

static __exit void exit_sensor(void)
{
	spi_unregister_driver(&sensor_spi_driver);
	i2c_del_driver(&sensor_driver);
}

module_init(init_sensor);
module_exit(exit_sensor);


#!/bin/bash
#
# To call this script, make sure make_ext4fs is somewhere in PATH

set -e

function usage() {
cat<<EOT
Usage:
make_ext4fs.sh <ROOTFS_DIR> <TARGET_DIR> <TARGET_IMAGE> <SYS_PARTION> <BOARD FORMFACTOR> <MARGIN> <ALIGN>
		- ROOTFS_DIR: directory path for source rootfs
		- TARGET_DIR: directory path for saving output image
		- TARGET_IMAGE: target ext4 image name what you want to create
		- SYS_PARTION: file path(full path) of sys_partition.fex file
		- BOARD FORMFACTOR: file path(full path) of sys_partition.fex file
		- PACK ALIGN(MB)
		- MARGIN(MB): Free space for users
			The sys_partition.fex has nand flash partition information for filesystem
EOT
}

ROOTFS_DIR=""
TARGET_IMAGE=""
SYS_PARTION=""
FORMFACTOR=""
TARGET_DIR=""
PACK_ALIGN=1
PACK_FREESPACE=0

while getopts h:r:t:p:f:a:m:o: OPTION
do
	case $OPTION in
	h) show_help
	exit 0
	;;
	r) ROOTFS_DIR=$OPTARG
	;;
	t) TARGET_IMAGE=$OPTARG
	;;
	p) SYS_PARTION=$OPTARG
	;;
	f) FORMFACTOR=$OPTARG
	;;
	a) PACK_ALIGN=$OPTARG
	;;
	m) PACK_FREESPACE=$OPTARG
	;;
	o) TARGET_DIR=$OPTARG
	;;
	*) usage
	exit 0
	;;
esac
done

if [ -z "$ROOTFS_DIR" -o -z "$TARGET_IMAGE" -o -z "$SYS_PARTION"  ]; then
	printf "Invalid Parameter\n"
	usage
	exit 1
fi

if [ ! -d $ROOTFS_DIR ]; then
	echo "Can not find directory [$ROOTFS_DIR]!"
	exit 2
fi

if [ ! -e $SYS_PARTION ]; then
	echo "Can not find configuration file [$SYS_PARTION]!"
	exit 2
fi




find_line_number(){
	grep -n 'name' $SYS_PARTION | sed -n '/\bname\b/{/=/{/\brootfs\b/p}}' | awk -F':' '{print $1}'
}

find_partition_size(){
	awk NR==$SIZEINFO_LINENUM $SYS_PARTION | awk -F'=' '{print $2}'
}

get_mbytes(){
	expr $SIZE_BYTES \* $SECTER_PER_BYTES
}

# name = rootfs 로 된 라인 번호를 검출
# sys_partition.fex는 반드시 name = rootfs로 되어 있는 라인이 한줄만 존재해야 한다.
# sys_partition.fex 수정할때 유의 할 것.
SIZEINFO_LINENUM=$(find_line_number)
if [ -z "$SIZEINFO_LINENUM" ]; then
	echo "Cannot find size info."
	if [ -z "$TARGET_DIR" ]; then
		echo "rootfs.ext4 is empty" > rootfs.ext4
	else
		echo "rootfs.ext4 is empty" > ${TARGET_DIR}/rootfs.ext4
	fi
	echo "Success in generating rootfs with empty file"
	exit 0
fi
SIZEINFO_LINENUM=$(($SIZEINFO_LINENUM+1))
SIZE_BYTES=$(find_partition_size)
SECTER_PER_BYTES=$'512\r'
SIZE_BYTES=$((${SIZE_BYTES//$'\r'} * ${SECTER_PER_BYTES//$'\r'}))
SIZE_MB=$((${SIZE_BYTES//$'\r'} / 1024 / 1024))


echo $SIZEINFO_LINENUM
echo $SIZE_BYTES
echo $SIZE_MB"M"


#SIZE_ACTUAL=`du -sm $ROOTFS_DIR | awk '{print $1}'`
#echo $SIZE_ACTUAL

#if [ $SIZE_MB -le $SIZE_ACTUAL ]; then
#	echo "ERROR: Partition size smaller than actual size"
#	exit 1
#fi

#IMG_SIZE=$((  $(( $(( $((SIZE_MB+PACK_FREESPACE)) + $((PACK_ALIGN-1)) ))  /  $((PACK_ALIGN)) )) * $((PACK_ALIGN)) ))
IMG_SIZE=$(( ((($SIZE_MB+ ($PACK_ALIGN-1))/$PACK_ALIGN) *$PACK_ALIGN) ))

echo $IMG_SIZE
echo "blocks: $SIZE_MB"M" -> $IMG_SIZE"M""
echo "TARGET_IMAGE="$TARGET_IMAGE
$ROOTFS_DIR/../host/usr/bin/make_ext4fs -l $IMG_SIZE"M" $TARGET_IMAGE $ROOTFS_DIR
fsck.ext4 -y $TARGET_IMAGE > /dev/null

#if [ -z "$TARGET_DIR" ]; then
#	#
#else
#	$ROOTFS_DIR/../host/usr/bin/make_ext4fs -l $IMG_SIZE"M" $TARGET_DIR/$TARGET_IMAGE $ROOTFS_DIR
#	fsck.ext4 -y $TARGET_DIR/$TARGET_IMAGE> /dev/null
#fi
echo "Success in generating rootfs"



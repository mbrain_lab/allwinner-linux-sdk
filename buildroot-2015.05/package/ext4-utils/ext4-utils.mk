################################################################################
#
# ext4-utils
#
################################################################################

EXT4_UTILS_VERSION = 1.0.0
EXT4_UTILS_SOURCE = ext4-utils
EXT4_UTILS_SITE = $(TOPDIR)/3rd_party/mbrain/packages/$(EXT4_UTILS_SOURCE)
EXT4_UTILS_LICENSE = ext4-utils license
EXT4_UTILS_LICENSE_FILES = NOTICE
EXT4_UTILS_SITE_METHOD = local
EXT4_UTILS_STAGING = YES

EXT4_UTILS_DEPENDENCIES = zlib
EXT4_UTILS_DEPENDENCIES+= host-ext4-utils

define EXT4_UTILS_BUILD_CMDS
	$(MAKE) CC="$(TARGET_CC)" LD="$(TARGET_LD)" -C $(@D)
endef

define HOST_EXT4_UTILS_BUILD_CMDS
	$(MAKE) -C $(@D)
endef

define EXT4_UTILS_INSTALL_TARGET_CMDS
	$(MAKE) CC="$(TARGET_CC)" LD="$(TARGET_LD)" -C $(@D) DESTDIR=$(TARGET_DIR) install
endef

define HOST_EXT4_UTILS_INSTALL_CMDS
	$(MAKE) -C $(@D) DESTDIR=$(HOST_DIR) install
endef

$(eval $(generic-package))
$(eval $(host-generic-package))

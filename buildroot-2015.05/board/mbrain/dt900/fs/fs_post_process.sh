#!/bin/bash
#
# To call this script, make sure make_ext4fs is somewhere in PATH

set -e

function usage() {
cat<<EOT
Usage:
fs_post_process.sh <ROOTFS_DIR>
		- ROOTFS_DIR: directory path for source rootfs
EOT
}

ROOTFS_DIR=""

EXEC_FILE="$0"
BASE_NAME=`basename "$EXEC_FILE"`
if [ "$EXEC_FILE" = "./$BASE_NAME" ] || [ "$EXEC_FILE" = "$BASE_NAME" ]; then
	FULL_PATH=`pwd`
else
	FULL_PATH=`echo "$EXEC_FILE" | sed 's/'"${BASE_NAME}"'$//'`
	cd "$FULL_PATH" > /dev/null 2>&1
	FULL_PATH=`pwd`
fi
WORK_DIR=${FULL_PATH}
echo "WORK_DIR=${WORK_DIR}"

while getopts h:r: OPTION
do
	case $OPTION in
	h) show_help
	exit 0
	;;
	r) ROOTFS_DIR=$OPTARG
	;;
	*) usage
	exit 0
	;;
esac
done

if [ -z "$ROOTFS_DIR" ]; then
	printf "Invalid Parameter\n"
	usage
	exit 1
fi


if [ ! -d "$ROOTFS_DIR" ]; then
	echo "Can not find directory [$ROOTFS_DIR]!"
	exit 2
fi

if [ ! -d "$WORK_DIR" ]; then
	printf "Cannot find out work directory\n"
	usage
	exit 1
fi

if [ -e ${WORK_DIR}/init ]; then
	install -m 0755  ${WORK_DIR}/init ${ROOTFS_DIR}/init
	cp -a ${WORK_DIR}/etc/* ${ROOTFS_DIR}/etc/
else
	if [ ! -e ${WORK_DIR}/init ]; then
		install -m 0755 ${ROOTFS_DIR}/../../fs/cpio/init ${ROOTFS_DIR}/init
	else
		echo "Original"
	fi
fi

# ISP tuning files copy
if [ -d ${WORK_DIR}/../isp ]; then
	cp -af ${WORK_DIR}/../isp ${ROOTFS_DIR}/etc/
fi


echo "Success in post processing for custom rootfs"


